<?php

namespace App;

use App\Item;
use App\User;
use App\AccountType;
use App\ClientLedger;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public $table = 'accounts';

    protected $fillable = [
        'account_name',
    	'type_id',
    	'code',
        'parent_id',
        'user_id',
        'entity_type'
    ];

     public function accounttype()
    {
    	return $this->belongsTo(AccountType::class, 'type_id', 'id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

     public function clientledger()
    {
        return $this->hasMany(ClientLedger::class);
    }

     public function item()
    {
        return $this->hasMany(Item::class);
    }

}
