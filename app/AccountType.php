<?php

namespace App;

use App\Account;
use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{
	public $table = 'account_types';
	
    public $fillable = [
    	'name'
    ];

    public function account()
    {
    	return $this->hasMany(Account::class, 'type_id', 'id');
    }
}
