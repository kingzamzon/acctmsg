<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditTrail extends Model
{
     protected $fillable = [
     	'model',
        'user_id',
        'business_id',
        'action',
        'comment'
    ];

}
