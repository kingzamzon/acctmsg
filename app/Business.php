<?php

namespace App;

use App\Item;
use App\User;
use App\Transaction;
use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'company_logo',
        'address',
        'phone',
        'email',
        'website',
        'category',
        'founders',
        'members',
        'description',
        'business_code'
    ];

    public static function generateBusinessCode()
    {
        return strtoupper(str_random(10));
    }

    public function setEmailAttribute($email)
    {
        $this->attributes['email'] = strtolower($email);
    }

    public function businessCode()
    {
        return strtoupper(str_random(5));
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }

    public function item()
    {
        return $this->hasMany(Item::class);
    }
}
