<?php

namespace App;

use App\Account;
use App\BusinessAccounts;
use Illuminate\Database\Eloquent\Model;

class ClientLedger extends Model
{
    public $table = 'client_ledger';

    protected $fillable = [
        'business_id',
        'account_id',
        'balance'
    ];

    public function business()
    {
    	return $this->belongsTo(Business::class);
    }

     public function account()
    {
    	return $this->belongsTo(Account::class);
    }
}
