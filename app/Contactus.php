<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactus extends Model
{
	public $table = 'contact_us';

    protected $fillable = [
    	'name','email','url','message'
    ];
}
