<?php

namespace App;

use App\Business;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'business_id',
        'business_name',
        'first_name',
        'last_name',
        'email',
        'address',
        'city',
        'tel',
        'state'
    ];


    public function business()
    {
        return $this->belongsTo(Business::class);
    }

}
