<?php

namespace App\Http\Controllers;

use App\Business;
use App\Accounts;
use Illuminate\Http\Request;

class AccountsController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:30|unique:account_types',
            'categories' => 'required|string|max:30'
        ];

        $this->validate($request, $rules);

        $data = $request->all();

        $newAccount = Accounts::create($data);
        
        return redirect()->back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Accounts  $accounts
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = Accounts::find($id);

        $deleteaccount = $account->delete();

        return redirect()->back();

    }
}
