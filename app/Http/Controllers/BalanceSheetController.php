<?php

namespace App\Http\Controllers;

use App\ClientLedger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use PDF;

use App\Http\Traits\BusinessTrait;
use App\Http\Traits\IncomeReportTrait;

class BalanceSheetController extends Controller
{
    use BusinessTrait, IncomeReportTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($businessCode, Request $request)
    {
        $businesses = $this->userBusinessList();

        $businessInfo = $this->businessInfo($businessCode);
       
        return view('dashboard.reports.balance_sheet', compact('businesses', 'businessInfo'));
    }

    public function getBalancesheet($businessCode, Request $request)
    {
        $businessInfo = DB::table('businesses')
                        ->where('businesses.business_code', '=', $businessCode)
                        ->first();

        $z = $this->invoiceSheet($businessInfo, $request->startdate, $request->enddate);
       
        return response()->json($z);
    }

          /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePDF($businessCode, $start_date, $end_date)
    {
        $businessInfo = DB::table('businesses')
                        ->where('businesses.business_code', '=', $businessCode)
                        ->first();

        $sheets = $this->invoiceSheet($businessInfo, $start_date, $end_date);

        $year = date("Y",strtotime($start_date));
        // Account Data
        $current_assets_data = [];
        $fixed_assets_data = [];
        $current_liabilities_data = [];
        $fixed_liabilities_data = [];
        $equity_data = [];

        // Account Data Balance
        $current_assets_data_balance = [];
        $fixed_assets_data_balance = [];
        $current_liabilities_data_balance = [];
        $fixed_liabilities_data_balance = [];
        $equity_data_balance = [];

        // Income statement
        $revenue_data = [];
        $revenue_data_balance = [];
        $expense_data = [];
        $expense_data_balance = [];

        foreach ($sheets as $key => $account) {
            if($account->account_type == "Current Assets"){
                array_push($current_assets_data, $account);
                array_push($current_assets_data_balance, $account->balance);
            }
        }

        foreach ($sheets as $key => $account) {
            if($account->account_type == "Fixed Assets"){
                array_push($fixed_assets_data, $account);
                array_push($fixed_assets_data_balance, $account->balance);
            }
        }

        foreach ($sheets as $key => $account) {
            if($account->account_type == "Current Liabilities"){
                array_push($current_liabilities_data, $account);
                array_push($current_liabilities_data_balance, $account->balance);
            }
        }

        foreach ($sheets as $key => $account) {
            if($account->account_type == "Fixed Liabilities"){
                array_push($fixed_liabilities_data, $account);
                array_push($fixed_liabilities_data_balance, $account->balance);
            }
        }

        foreach ($sheets as $key => $account) {
            if($account->account_type == "Equity"){
                array_push($equity_data_balance, $account);
                array_push($equity_data_balance_balance, $account->balance);
            }
        }

        // Revenue wahala
        foreach ($sheets as $key => $account) {
            if($account->account_type == "Revenue"){
                array_push($revenue_data, $account);
                array_push($revenue_data_balance, $account->balance);
            }
        }
        
        foreach ($sheets as $key => $account) {
            if($account->account_type == "Expense"){
                array_push($expense_data, $account);
                array_push($expense_data_balance, $account->balance);
            }
        }

        $revenue_sum = array_sum($revenue_data_balance);
        $expense_sum = array_sum($expense_data_balance);

        $retained_earning = $revenue_sum - $expense_sum;
        // End of Revenue wahala

        $current_assets_sum = array_sum($current_assets_data_balance);
        $fixed_assets_sum = array_sum($fixed_assets_data_balance);
        $current_liabilities_sum = array_sum($current_liabilities_data_balance);
        $fixed_liabilities_sum = array_sum($fixed_liabilities_data_balance);
        $equity_sum = array_sum($equity_data_balance);

        $equity_total = $equity_sum + $retained_earning;

        // Total of accounts
        $assets_total = $current_assets_sum + $fixed_assets_sum;
        $liablities_equity_total = $current_liabilities_sum + $fixed_liabilities_sum + $equity_total;
        $liablities_total = $current_liabilities_sum + $fixed_liabilities_sum;

        // Total of accounts
        $assets_total != 0 ? $debt_ratio = ($liablities_total / $assets_total) : $debt_ratio = 0.0;
        $current_liabilities_sum != 0 ? $current_ratio = ($current_assets_sum / $current_liabilities_sum) : $current_ratio = 0.0;
        $current_liabilities_sum != 0 ? $working_capital = ($current_assets_sum - $current_liabilities_sum) : $working_capital = 0.0;
        $equity_total != 0 ? $assests_equity_ratio = ($assets_total / $equity_total) : $assests_equity_ratio = 0.0;
        $equity_total != 0 ? $debt_equity_ratio = ($liablities_total / $equity_total) : $debt_equity_ratio = 0.0;

        $data = ['businessInfo'=>$businessInfo, 
                'sheets'=>$sheets,
                'year'=>$year,
                'current_assets_data'=>$current_assets_data,
                'fixed_assets_data'=>$fixed_assets_data,
                'current_liabilities_data'=>$current_liabilities_data,
                'fixed_liabilities_data'=>$fixed_liabilities_data,
                'equity_data'=>$equity_data,
                'current_assets_sum'=>$current_assets_sum,
                'fixed_assets_sum'=>$fixed_assets_sum,
                'current_liabilities_sum'=>$current_liabilities_sum,
                'fixed_liabilities_sum'=>$fixed_liabilities_sum,
                'equity_sum'=>$equity_sum,
                'retained_earning'=>$retained_earning,
                'equity_total'=>$equity_total,
                'assets_total'=>$assets_total,
                'liablities_equity_total'=>$liablities_equity_total,
                'liablities_total'=>$liablities_total,
                'debt_ratio'=>$debt_ratio,
                'current_ratio'=>$current_ratio,
                'working_capital'=>$working_capital,
                'assests_equity_ratio'=>$assests_equity_ratio,
                'debt_equity_ratio'=>$debt_equity_ratio,
            ];

        // return response()->json($data);
        $pdf = PDF::loadView('dashboard.pdf.balancePDF', $data);
  
        return $pdf->download('balanceSheet');
        // return view("dashboard.pdf.balance_dataPDF", compact('data'));
    }


}
