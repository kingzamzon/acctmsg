<?php

namespace App\Http\Controllers;

use App\Account;
use App\Business;
use App\UserBusiness;
use App\SubscriptionPlan;
use App\Subscribers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Traits\BusinessTrait;

class BusinessController extends Controller
{
    use BusinessTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get list of businesses for auth user
        $businesses = $this->userBusinessList();
        
        // get all the subsciption so far for auth user
        $subscribes = DB::table('subscribers')
                    ->where('subscribers.user_id', '=', auth()->user()->id)
                    ->get();
        
        // get all subscription plans  
        $plans = SubscriptionPlan::orderBy('id', 'desc')->get();

        $accounts = Account::orderBy('id', 'desc')->get();
        
        //check subscription expiration
        $subscription = Subscribers::where('user_id',auth()->user()->id)->orderBy('id','desc')->first();
            if($subscription) {
                $datetime1 = date_create($subscription->created_at);
                $datetime2 = date_create(Carbon::now());
                $interval = date_diff($datetime1, $datetime2);
                $daysused =  $interval->format('%a');
            }else {
                $daysused =  0;
            }
            
        return view('dashboard.business_list', compact('businesses','subscribes','accounts','plans','daysused'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'description' => 'required|string',
            'company_logo' => 'image',
            'address' => 'required|string',
            'phone' => 'required|unique:businesses',
            'email' => 'email|unique:businesses',
            'category' => 'required|string',
            'founders' => 'required|string',
            'members' => 'required|string',
            'description' => 'required'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['user_id'] = auth()->user()->id;
        if($request->has('website')){
            $data['website'] = $request->website;
        }else {
            $data['website'] = 'null';
        }
        if($request->has('company_logo')){
            $data['company_logo'] = $request->company_logo->store('');
        }else {
            $data['company_logo'] = 'no-avatar.jpg';
        }
        $data['business_code'] = Business::generateBusinessCode();

        $newBusiness = Business::create($data);
        $this->storeUserBusiness($newBusiness->id);

        return redirect()->back();
    }

/**
     * Store a newly created business in usersbusiness table.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function storeUserBusiness($id)
    {
        $data['business_id'] = $id; 
        $data['user_id'] = auth()->user()->id;
        $data['role_id'] = 1;
        $newJournal = UserBusiness::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function show(Business $business)
    {
        return response()->json($business);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function edit(Business $business)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Business $business)
    {
       $business->name = $request->name;
       if($request->has('company_logo')){
        $business->company_logo = $request->company_logo->store('');
        }
       $business->address = $request->address;
       $business->phone = $request->phone;
       $business->email = $request->email;
       $business->website = $request->website;
       $business->save();

       return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $business = Business::find($id);
        if($business->company_logo != 'no-avatar.jpg'){
            Storage::delete($business->company_logo);
        }
        $userbusiness = UserBusiness::where('business_id','=',$id)->delete();
        $business = $business->delete();

        return redirect()->back();
    }
}
