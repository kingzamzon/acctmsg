<?php

namespace App\Http\Controllers;

use App\ClientLedger;
use App\Business;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use PDF;

use App\Http\Traits\BusinessTrait;
use App\Http\Traits\IncomeReportTrait;

class CashFlowController extends Controller
{
    use BusinessTrait, IncomeReportTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($businessCode)
    {
        $businesses = $this->userBusinessList();

        $businessInfo = $this->businessInfo($businessCode);
        return view('dashboard.reports.cash_flow', compact('businesses','businessInfo'));
    }

    public function generatePDF($businessCode, $start_date, $end_date)
    {
        $businessInfo = DB::table('businesses')
                        ->where('businesses.business_code', '=', $businessCode)
                        ->first();

        $sheets = $this->invoiceSheet($businessInfo, $start_date, $end_date);

        $year = date("Y",strtotime($end_date));

        // Income statement
        $revenue_data = [];
        $revenue_data_balance = [];
        $expense_data = [];
        $expense_data_balance = [];
        $investing_data = [];
        $investing_data_balance = [];
        $investing_paid_data = [];
        $investing_paid_data_balance = [];
        $financing_data = [];
        $financing_data_balance = [];
        $financing_paid_data = [];
        $financing_paid_data_balance = [];

        // Revenue wahala
        foreach ($sheets as $key => $account) {
            if($account->account_type == "Revenue"){
                array_push($revenue_data, $account);
                array_push($revenue_data_balance, $account->balance);
            }
        }
        
        foreach ($sheets as $key => $account) {
            if($account->account_type == "Expense"){
                array_push($expense_data, $account);
                array_push($expense_data_balance, $account->balance);
            }
        }

        foreach ($sheets as $key => $account) {
            if($account->account_type == "Non-Current Assets"){
                array_push($investing_data, $account);
                array_push($investing_data_balance, $account->balance);
            }
        }
        
        foreach ($sheets as $key => $account) {
            if($account->account_type == "Non-Current Assets"){
                array_push($investing_paid_data, $account);
                array_push($investing_paid_data_balance, $account->balance);
            }
        }

        foreach ($sheets as $key => $account) {
            if($account->account_type == "Non-Current Liabilities"){
                array_push($financing_data, $account);
                array_push($financing_data_balance, $account->balance);
            }
        }
        
        foreach ($sheets as $key => $account) {
            if($account->account_type == "Non-Current Liabilities"){
                array_push($financing_paid_data, $account);
                array_push($financing_paid_data_balance, $account->balance);
            }
        }

        $revenue_sum = array_sum($revenue_data_balance);
        $expense_sum = array_sum($expense_data_balance);
        $investing_sum = array_sum($investing_data_balance);
        $investing_paid_sum = array_sum($investing_paid_data_balance);
        $financing_sum = array_sum($financing_data_balance);
        $financing_paid_sum = array_sum($financing_paid_data_balance);

        $cash_sum = $revenue_sum + $expense_sum;
        $investing_sum = $investing_sum + $investing_paid_sum;
        $financing_sum = $financing_sum + $financing_paid_sum;
        // End of Revenue wahala

        $data = ['businessInfo'=>$businessInfo, 
                'year'=>$year,
                'revenue_data'=>$revenue_data,
                'expense_data'=>$expense_data,
                'investing_data'=>$investing_data,
                'investing_paid_data'=>$investing_paid_data,
                'financing_data'=>$financing_data,
                'financing_paid_data'=>$financing_paid_data,
                'cash_sum'=>$cash_sum,
                'investing_sum'=>$investing_sum,
                'financing_sum'=>$financing_sum,
            ];

        // return response()->json($data);
        $pdf = PDF::loadView('dashboard.pdf.cashflowPDF', $data);
  
        return $pdf->download('cashflowStatement');
        // return view("dashboard.pdf.cashflow_dataPDF", compact('data'));
    }

}
