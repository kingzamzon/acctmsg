<?php

namespace App\Http\Controllers;

use App\ClientLedger;
use App\Account;
use App\AccountType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Http\Traits\BusinessTrait;

class ChartOfAccountsController extends Controller
{
    use BusinessTrait;
       /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index($businessCode)
    {
        $businesses = $this->userBusinessList();

        $businessInfo = $this->businessInfo($businessCode);
        
        $chartofaccounts = Account::orderBy('account_name','asc')->get();
        $entityLedger = DB::table('client_ledger')
            ->where('client_ledger.business_id', '=', $businessInfo->id)
            // ->where('chartofaccount->name', 'Assets')
            ->get();
        $accountTypes = AccountType::all();
        $success = [];

        return view('dashboard.chart_of_accounts', compact('businesses','businessInfo','chartofaccounts','entityLedger','accountTypes', 'success'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($businessCode, Request $request)
    {
        $business = DB::table('businesses')
                        ->where('businesses.business_code', '=', $businessCode)
                        ->first();
        $rules = [
            'account_name' => 'required',
            'type_id' => 'required',
            'code' => 'integer|required|unique:accounts',
            'entity_type' => 'required|max:8'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['parent_id'] = 3;
        $data['user_id'] = auth()->user()->id;

        $transaction = Account::create($data);
        $success = "New Chart of Accounts Created";
        return redirect()->back()->with(['data' => $success]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show($businessCode, $account)
    {
        $businesses = $this->userBusinessList();

        $businessInfo = $this->businessInfo($businessCode);

        $account = Account::find($account);

        $transactions = DB::table('transactions')
                    ->leftJoin('journals', 'transactions.id', '=', 'journals.transaction_id')
                    ->where('transactions.business_id', '=', $businessInfo->id)
                    ->where('journals.account_id', '=', $account->id)
                    ->orderBy('transactions.id','desc')
                    ->get();

        return view('dashboard.account-view', compact('businesses','businessInfo','account','transactions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy($businessCode, $id)
    {
        $account = Account::find($id);

        $deleteaccount = $account->delete();

        $success = "Chart of Accounts Deleted";
        return redirect()->back()->with(['data' => $success]);
    }
}
