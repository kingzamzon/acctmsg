<?php

namespace App\Http\Controllers;

use App\Contactus;
use App\Mail\ContactusMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class ContactusController extends Controller
{

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function index()
    {
        return view('auth.contactus');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return 12;
       $rules = [
            'name' => 'required|string',
            'email' => 'required|email',
            'url' => 'string',
            'message' => 'required'
       ];

       $this->validate($request, $rules);

       $data = $request->all();
       $contact = Contactus::create($data);

    //    retry(5, function() use ($contact){
    //         Mail::to($contact)->send(new ContactusMail($contact));
    //     }, 100);
        $success = 'We will get back to you shortly';
       return redirect()->back()->with(['data' => $success]);
    }

}
