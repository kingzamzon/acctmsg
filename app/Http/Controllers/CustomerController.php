<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Traits\BusinessTrait;

class CustomerController extends Controller
{
    use BusinessTrait;
       /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($businessCode)
    {
        $businesses = $this->userBusinessList();

        $businessInfo = $this->businessInfo($businessCode);

        $customers = DB::table('customers')
                    ->where('customers.business_id', '=', $businessInfo->id)
                    ->select('customers.*')
                    ->get(); 
           
        return view('dashboard.invoice.invoice-customers', compact('businesses', 'businessInfo', 'customers'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($businessCode, Request $request)
    {
        $business = DB::table('businesses')
                        ->where('businesses.business_code', '=', $businessCode)
                        ->first();

        $rules = [
            'business_name' => 'required|string',
            'fname' => 'required|string',
            'lname' => 'required|string',
            'email' => 'required|email',
            'customer_address' => 'required|string',
            'customer_city' => 'required|string|max:30',
            'customer_tel' => 'required|max:30',
            'customer_state' => 'required|string|max:30'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['business_id'] = $business->id;
        $data['first_name'] = $request->fname;
        $data['last_name'] = $request->lname;
        $data['email'] = $request->email;
        $data['address'] = $request->customer_address;
        $data['city'] = $request->customer_city;
        $data['tel'] = $request->customer_tel;
        $data['state'] = $request->customer_state;

        $item = Customer::create($data);

        $success = "New Customer Created";
        return redirect()->back()->with(['data' => $success]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($businessCode, Customer $customer)
    {
        return response()->json($customer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update($businessCode, Request $request, Customer $customer)
    {
        $customer =  Customer::find($request->id);
        $customer->business_name = $request->business_name;
        $customer->first_name = $request->fname;
        $customer->last_name = $request->lname;
        $customer->email = $request->email;
        $customer->address = $request->customer_address;
        $customer->city = $request->customer_city;
        $customer->tel = $request->customer_tel;
        $customer->state = $request->customer_state;
        $customer->save();
        $success = "Customer Updated";
        return redirect()->back()->with(['data' => $success]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($businessCode, $id)
    {
        $customer = Customer::find($id);

        $deletecustomer = $customer->delete();

        $success = "Customer Deleted";
        return redirect()->back()->with(['data' => $success]);
    }
}
