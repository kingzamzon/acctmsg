<?php

namespace App\Http\Controllers;

use App\Business;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($businessCode)
    {
        // Route::current()->parameter('id');
        $businesses = DB::table('businesses')
                    ->leftJoin('user_businesses', 'businesses.id', '=', 'user_businesses.business_id')
                    ->where('user_businesses.user_id', '=', auth()->user()->id)
                    ->select('businesses.*')
                    ->orderBy('businesses.id','asc')
                    ->get();
        $businessInfo = DB::table('businesses')
                        ->where('businesses.business_code', '=', $businessCode)
                        ->first();
        $customers = DB::table('customers')
                        ->where('customers.business_id', '=', $businessInfo->id)
                        ->select('customers.*')
                        ->get();
        $items =  DB::table('items')
                        ->where('items.business_id', '=', $businessInfo->id)
                        ->leftJoin('accounts', 'items.account_id', '=', 'accounts.id')
                        ->select('items.*','accounts.account_name')
                        ->get();
        $invoices =  DB::table('invoices')
                        ->where('invoices.business_id', '=', $businessInfo->id)
                        ->select('invoices.*')
                        ->get();
        $transactions =  DB::table('transactions')
                        ->where('transactions.business_id', '=', $businessInfo->id)
                        ->select('transactions.*')
                        ->get();
        $cash_ledger =  DB::table('client_ledger')
                        ->where('client_ledger.business_id', '=', $businessInfo->id)
                        ->where('client_ledger.account_id', '=', 1)
                        ->select('client_ledger.*')
                        ->get();
        $all_cash_balances = array_pluck($cash_ledger, 'balance');
        $last_cash = last($all_cash_balances);

        $sales_revenue_ledger =  DB::table('client_ledger')
                        ->where('client_ledger.business_id', '=', $businessInfo->id)
                        ->where('client_ledger.account_id', '=', 14)
                        ->select('client_ledger.*')
                        ->get();

        $service_revenue_ledger =  DB::table('client_ledger')
                        ->where('client_ledger.business_id', '=', $businessInfo->id)
                        ->where('client_ledger.account_id', '=', 15)
                        ->select('client_ledger.*')
                        ->get();
        // return count($sales_revenue_ledger);
        if(count($sales_revenue_ledger) > 0)
        {
            $all_revenue_balances = array_pluck($sales_revenue_ledger, 'balance');
            $last_revenue = last($all_revenue_balances);
        }
        else {
            $all_revenue_balances = array_pluck($service_revenue_ledger, 'balance');
            $last_revenue = last($all_revenue_balances);  
        }
        return view('dashboard.dashboard', compact('businesses', 'businessInfo','customers','items',
        'invoices','transactions','last_cash','last_revenue'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
