<?php

namespace App\Http\Controllers;

use App\Accounts;
use App\SubscriptionPlan;
use App\Business;
use App\BusinessAccounts;
use App\Journal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect()->route('business.index');
    }
}
