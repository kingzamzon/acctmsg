<?php

namespace App\Http\Controllers;

use App\Business;
use App\IncomeStatement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

use App\Exports\IncomeStatementExport;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Traits\BusinessTrait;
use App\Http\Traits\IncomeReportTrait;
// use Excel;

class IncomeStatementController extends Controller
{
    use BusinessTrait, IncomeReportTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($businessCode)
    {
        $businesses = $this->userBusinessList();

        $businessInfo = $this->businessInfo($businessCode);
        
        return view('dashboard.reports.income_statement', compact('businesses','businessInfo'));
    }

      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePDF($businessCode, $start_date, $end_date)
    {
        $businessInfo = DB::table('businesses')
                        ->where('businesses.business_code', '=', $businessCode)
                        ->first();

        $sheets = $this->invoiceSheet($businessInfo, $start_date, $end_date);


        $year = date("Y",strtotime($start_date));
        $revenue_data = [];
        $revenue_data_balance = [];
        $expense_data = [];
        $expense_data_balance = [];

        foreach ($sheets as $key => $account) {
            if($account->account_type == "Revenue"){
                array_push($revenue_data, $account);
                array_push($revenue_data_balance, $account->balance);
            }
        }
        
        foreach ($sheets as $key => $account) {
            if($account->account_type == "Expense"){
                array_push($expense_data, $account);
                array_push($expense_data_balance, $account->balance);
            }
        }

        $revenue_sum = array_sum($revenue_data_balance);
        $expense_sum = array_sum($expense_data_balance);

        $net_icome_before_taxes = $revenue_sum - $expense_sum;
        $tax_expense = 0;
        $net_income = $net_icome_before_taxes - $tax_expense;

        $data = ['businessInfo'=>$businessInfo, 
                'sheets'=>$sheets, 
                'year'=>$year,
                'revenue_data'=>$revenue_data,
                'expense_data'=>$expense_data,
                'revenue_sum'=>$revenue_sum,
                'expense_sum'=>$expense_sum,
                'net_icome_before_taxes'=>$net_icome_before_taxes,
                'tax_expense'=>$tax_expense,
                'net_income'=>$net_income,
            ];

        // return response()->json($data);


        $pdf = PDF::loadView('dashboard.pdf.incomePDF', $data);
  
        return $pdf->download('invoiceStatement');
    }

    public function export()
    {
        return Excel::download(new IncomeStatementExport, 'bbbbb.xlsx');
    }

}
