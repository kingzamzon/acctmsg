<?php

namespace App\Http\Controllers;

use App\Item;
use App\Account;
use App\Journal;
use App\Transaction;
use App\ClientLedger;

use App\Invoice;
use App\InvoiceTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Mail\InvoiceMail1;
use Illuminate\Support\Facades\Mail;
use PDF;

use App\Http\Traits\LedgerTrait;
use App\Http\Traits\JournalTrait;
use App\Http\Traits\BusinessTrait;

class InvoiceController extends Controller
{
    use LedgerTrait, JournalTrait, BusinessTrait;
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($businessCode)
    {
        $businesses = $this->userBusinessList();

        $businessInfo = $this->businessInfo($businessCode);
         
        $invoices = Invoice::where('business_id', $businessInfo->id)->orderBy("id","desc")->get();

        return view('dashboard.invoice.invoice', compact('businesses', 'businessInfo','invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($businessCode)
    {
        $businesses = $this->userBusinessList();

        $businessInfo = $this->businessInfo($businessCode);

        $customers = DB::table('customers')
                    ->where('customers.business_id', '=', $businessInfo->id)
                    ->select('customers.*')
                    ->get();
        $items =  DB::table('items')
                    ->where('items.business_id', '=', $businessInfo->id)
                    ->leftJoin('accounts', 'items.account_id', '=', 'accounts.id')
                    ->select('items.*','accounts.account_name')
                    ->get();

        return view('dashboard.invoice.invoice-create', compact('businesses','businessInfo', 'customers','items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($businessCode, Request $request)
    {
        // return $request;
        $business = DB::table('businesses')
        ->where('businesses.business_code', '=', $businessCode)
        ->first();

        $rules = [
        'customer_id' => 'required',
        'invoice_date' => 'required'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['user_id'] = auth()->user()->id;
        $data['business_id'] = $business->id;
        $data['invoice_no'] = Invoice::generateInvoiceCode();
        if($request->discount == ''){
            $data['discount'] = '0';
        }else {
            $data['discount'] = $request->discount;
        }
        if($request->vat == ''){
            $data['vat'] = '0';
        }else {
            $data['vat'] = $request->vat;
        }

        $invoice = Invoice::create($data);
      
        if($invoice){
            for($i=0; $i < count($request->item); $i++){
                self::invoiceTransaction($invoice->id, $request->item[$i], $request->item_description[$i], 
                $request->quantity[$i], $request->unit_cost[$i],$business);
            }
          }

        // retry(5, function() use ($invoice){
        //     Mail::to($invoice->customer->email)->send(new InvoiceMail1($invoice));
        // }, 100);
        $success = "New Invoice Created";
        
        $invoice_view_url = route('invoice.show', ['businessCode' => $businessCode, 'invoice' => $invoice->id]);
        
        return response()->json([$success,$invoice_view_url]);
    }

    public function invoiceTransaction($invoice_id, $item_id, $description, $quantity, $unit_cost, $business)
    {
            $data['invoice_id'] = $invoice_id;
            $data['item_id'] = $item_id;
            $data['description'] = $description;
            $data['quantity'] = $quantity;
            $data['unit_cost'] = $unit_cost;
            $invoiceTransaction = InvoiceTransaction::create($data);
            self::logJournalEntry($business, $item_id,  $quantity, $unit_cost, $description);
    }

    public function logJournalEntry($business, $item_id, $quantity, $unit_cost, $description)
    {
        $amount = $quantity * $unit_cost;
        $item_account_id = Item::where('id','=',$item_id)->pluck('account_id')[0];  // Credit
        $account_receiveable_id = Account::where('account_name','=','Accounts receivable')->pluck('id')[0];  // Debit

        // $data = [];
        $data['user_id'] = auth()->user()->id;
        $data['description'] = $description;
        $data['business_id'] = $business->id;

        $transaction = Transaction::create($data);

        $this->debitTransactionJournal($transaction->id, $account_receiveable_id, $amount);
        $this->creditTransactionJournal($transaction->id, $item_account_id, $amount);

        $this->debitLedger($business->id, $account_receiveable_id, $amount);
        $this->creditLedger($business->id, $item_account_id, $amount);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show($businessCode, $id)
    {
        $businesses = $this->userBusinessList();

        $businessInfo = $this->businessInfo($businessCode);

        $invoice = Invoice::where(['id'=>$id, 'user_id'=>auth()->user()->id])->with('invoicetransaction')->with('customer')->first();

        if($invoice) {
            return view('dashboard.invoice.invoice-view', compact('businesses', 'businessInfo','invoice'));
        }else {
            return view('dashboard.404_page');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }

          /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadInvoiceasPDF($businessCode, $id)
    {
        $businessInfo = DB::table('businesses')
                        ->where('businesses.business_code', '=', $businessCode)
                        ->first();
        $invoice = Invoice::where('id', $id)->with('invoicetransaction')->with('customer')->first();

        $subtotal = [];

        foreach ($invoice->invoicetransaction as $key => $value) {
           array_push($subtotal, $value->quantity * $value->unit_cost);
        }
        $subtotal = array_sum($subtotal);
        $discount = $this->calculateDiscount($invoice->discount, $subtotal);
        // total sum for subtotal and discount 
        $total_sum = $subtotal - $discount;

        $vat = $this->calculateVat($invoice->vat, $total_sum);
        // return $vat;
        
        $data = ['title' => 'Welcome to HDTuto.com','businessInfo'=>$businessInfo, 
                'invoice'=>$invoice, 'subtotal'=>$subtotal, 'discount'=>$discount, 'vat'=>$vat];

        $pdf = PDF::loadView('dashboard.pdf.invoicePDF', $data);
  
        return $pdf->download('invoice');
    }

    public function calculateDiscount($discount, $subtotal)
    {
        if($discount > 0){
            $discount = ($discount * $subtotal) / 100;
        }else {
            $discount = 0.0;
        }
        return $discount;
    }

    public function calculateVat($vat, $total_sum)
    {
        if($vat > 0){
            $vat = ($vat * $total_sum) / 100;
        }else {
            $vat = 0.0;
        }
        return $vat;
    }
}
