<?php

namespace App\Http\Controllers;

use App\Item;
use App\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Traits\BusinessTrait;

class ItemController extends Controller
{
    use BusinessTrait;
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($businessCode)
    {
        $businesses = $this->userBusinessList();

        $businessInfo = $this->businessInfo($businessCode);;

        $accounts = Account::all();

        $items =  DB::table('items')
                        ->where('items.business_id', '=', $businessInfo->id)
                        ->leftJoin('accounts', 'items.account_id', '=', 'accounts.id')
                        ->select('items.*','accounts.account_name')
                        ->get(); 
            
        return view('dashboard.invoice.invoice-items', compact('businesses', 'businessInfo', 'accounts', 'items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($businessCode, Request $request)
    {
        $business = DB::table('businesses')
                        ->where('businesses.business_code', '=', $businessCode)
                        ->first();

        $rules = [
            'name' => 'required|string',
            'type' => 'required|string',
            'unit' => 'required|string',
            'price' => 'required|max:20',
            'account' => 'required|integer',
            'description' => 'required|string|max:30'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['user_id'] = auth()->user()->id;
        $data['account_id'] = $request->account;
        $data['business_id'] = $business->id;

        $item = Item::create($data);

        $success = "New Item Created";
        return redirect()->back()->with(['data' => $success]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show($businessCode, Item $item)
    {
        return response()->json($item);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function showPrice($businessCode, Request $request)
    {
       $item = Item::find($request->id);
       return response()->json(['data'=>$item->price]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update($businessCode, Request $request, Item $item)
    {
        $item =  Item::find($request->id);
        $item->name = $request->name;
        $item->type = $request->type;
        $item->unit = $request->unit;
        $item->price = $request->price;
        $item->account_id = $request->account;
        $item->description = $request->description;
        $item->save();
        $success = "Item Updated";
        return redirect()->back()->with(['data' => $success]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($businessCode, $id)
    {
        $item = Item::find($id);

        $deleteitem = $item->delete();

        $success = "Item Deleted";
        return redirect()->back()->with(['data' => $success]);
    }
}
