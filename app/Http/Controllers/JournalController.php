<?php

namespace App\Http\Controllers;

use App\Account;
use App\Journal;
use App\Business;
use App\Transaction;
use App\ClientLedger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Traits\LedgerTrait;
use App\Http\Traits\JournalTrait;
use App\Http\Traits\BusinessTrait;

class JournalController extends Controller
{
    use LedgerTrait, JournalTrait, BusinessTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($businessCode)
    {
        $businesses = $this->userBusinessList();

        $businessInfo = $this->businessInfo($businessCode);

        $accounts = Account::orderBy('account_name', 'asc')->get();
        $daccounts = Account::orderBy('account_name', 'asc')->where('entity_type', 'Debit')->get();
        $caccounts = Account::orderBy('account_name', 'asc')->where('entity_type', 'Credit')->get();

        $transactions = Transaction::where('business_id','=',$businessInfo->id)->get();
        $journals =  DB::table('transactions')
                        ->where('transactions.business_id', '=', $businessInfo->id)
                        ->leftJoin('journals', 'transactions.id', '=', 'journals.transaction_id')
                        ->leftJoin('accounts', 'journals.account_id', '=', 'accounts.id')
                        ->leftJoin('account_types', 'accounts.type_id', '=', 'account_types.id')
                        ->select('transactions.*','journals.amount','accounts.account_name','accounts.code','account_types.name AS account_type','journals.entity_type')
                        ->orderBy('transactions.id','desc')
                        ->get(); 

        return view('dashboard.account_journal', compact('businesses','businessInfo','accounts', 'caccounts', 'daccounts', 'transactions','journals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($businessCode, Request $request)
    {
        $business = DB::table('businesses')
                        ->where('businesses.business_code', '=', $businessCode)
                        ->first();
        $rules = [
            'account_debit' => 'required|max:10',
            'account_credit' => 'required|max:10',
            'description' => 'required|string',
            'amount' => 'required|integer'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['user_id'] = auth()->user()->id;
        $data['business_id'] = $business->id;

        $transaction = Transaction::create($data);

        
        $debitTransaction =$this->debitTransactionJournal($transaction->id, $request->account_debit, $request->amount);
        $creditTransaction = $this->creditTransactionJournal($transaction->id, $request->account_credit, $request->amount);
        
        // return $debitTransaction;
        if($creditTransaction) {
            $debitLedger = $this->debitLedger($business->id, $request->account_debit, $request->amount);
            if($debitLedger) {
                $creditLedger = $this->creditLedger($business->id, $request->account_credit, $request->amount);
                // return $creditLedger;
                if($creditLedger){
                    return redirect()->back();
                }
            }

        }
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function show(Journal $journal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function edit(Journal $journal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Journal $journal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function destroy($businessCode, $id)
    {
        $journal = Journal::find($id);

        $deletejournal = $journal->delete();

        return redirect()->back();
    }
}
