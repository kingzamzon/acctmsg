<?php

namespace App\Http\Controllers;

use App\User;
use App\Business;
use App\SubscriptionPlan;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class PagesController extends Controller
{
     use AuthenticatesUsers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function index()
    {
        $businesses = Business::orderBy('id', 'asc')->get()->count();
        $users = User::orderBy('id', 'asc')->get()->count();
        return view('auth.welcome', compact('businesses','users'));

        // $plans = SubscriptionPlan::orderBy('id', 'desc')->get();
        // $categories = Business::distinct('category')->pluck('category');
        // // return $businesses;
        // return view('welcome', compact('plans','categories','businesses','users'));
    }

    public function oldindex()
    {
        $plans = SubscriptionPlan::orderBy('id', 'desc')->get();
        $businesses = Business::orderBy('id', 'asc')->get()->count();
        $users = User::orderBy('id', 'asc')->get()->count();
        $categories = Business::distinct('category')->pluck('category');
        // return $businesses;
        return view('welcome', compact('plans','categories','businesses','users'));
    }

    public function services()
    {
        return view('auth.services');
    }

    public function pricing()
    {
        return view('auth.pricing');
    }

    public function error_404()
    {
        return view('dashboard.404_page');
    }

    
}
