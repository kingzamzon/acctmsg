<?php

namespace App\Http\Controllers;

use App\Subscribers;
use App\SubscriptionPlan;
use App\PaystackTransaction;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Paystack;

class PaymentController extends Controller
{

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();
        $paystack_subscription_plan = $paymentDetails['data']['metadata']['plan_id'];

        $originalSubAmount = SubscriptionPlan::where('id', $paystack_subscription_plan)->first()->amount;
        $originalSubAmount = intval($originalSubAmount) * 100;
        // dd($paymentDetails['data']);
        // return response()->json($paymentDetails['data']);
        if(($paymentDetails['data']['status'] == 'success') && ($paymentDetails['data']['amount'] == $originalSubAmount))
        // if($paymentDetails['data']['status'] == 'success')
        {
            // return dd('we are here');
            $data['user_id'] = $paymentDetails['data']['metadata']['user_id'];
            $data['amount'] = $paymentDetails['data']['amount'];
            $data['subscription_plan'] = $paymentDetails['data']['metadata']['plan_id'];
        	$business_code = $paymentDetails['data']['metadata']['business_code'];
            $data['transaction_ref'] = $paymentDetails['data']['reference'];
            $data['transaction_status'] = $paymentDetails['data']['status'];

            $subscribe = Subscribers::create($data);

            PaystackTransaction::create($data);

            $success = 'Subscription Successful';
            return redirect($paymentDetails['data']['metadata']['prev_url'])->with(['data' => $success]);
            // return redirect('/'.$business_code.'/dashboard')->with(['data' => $success]);
            
        }else {
            // return dd('something went wrong');
            $success = 'Something went wrong';
            return redirect('/'.$business_code.'/dashboard')->with(['data' => $success, 'status' => 'error']);
        }
    //    return dd($paymentDetails->data['status']);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }
}