<?php

namespace App\Http\Controllers;

use App\UserBusiness;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Traits\BusinessTrait;

class UserBusinessController extends Controller
{
    use BusinessTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesses = $this->userBusinessList();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserBusiness  $userBusiness
     * @return \Illuminate\Http\Response
     */
    public function show(UserBusiness $userBusiness)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserBusiness  $userBusiness
     * @return \Illuminate\Http\Response
     */
    public function edit(UserBusiness $userBusiness)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserBusiness  $userBusiness
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserBusiness $userBusiness)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserBusiness  $userBusiness
     * @return \Illuminate\Http\Response
     */
    public function destroy($businessCode, $id)
    {
        $user = UserBusiness::find($id)->delete();

        return redirect()->back();
    }
}
