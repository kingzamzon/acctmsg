<?php

namespace App\Http\Controllers;

use App\Business;
use App\User;
use App\UserBusiness;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use App\Http\Traits\BusinessTrait;

class UsersController extends Controller
{
    use BusinessTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($businessCode)
    {
        $businesses = $this->userBusinessList();

        $businessInfo = $this->businessInfo($businessCode);

        $businessusers = DB::table('user_businesses')
                        ->leftJoin('users', 'user_businesses.user_id', '=', 'users.id')
                        ->where('user_businesses.business_id', '=', $businessInfo->id)
                        ->select('user_businesses.id as userBusinessId','user_businesses.*',
                         'users.*')
                        ->get();
        $userrole = DB::table('user_businesses')
                        ->where('user_businesses.user_id', '=', auth()->user()->id)
                        ->where('user_businesses.business_id', '=', $businessInfo->id)
                        ->pluck('role_id')
                        ->first();
                 
        return view('dashboard.users', compact('businesses', 'businessusers', 'userrole', 'businessInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($businessCode, Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['password'] = Hash::make($request->password);
        $data['profile_photo'] = 'no-avatar.jpg';
        $data['scratch_card'] = '';
        $user = User::create($data);

         $businessInfo = DB::table('businesses')
                        ->where('businesses.business_code', '=', $businessCode)
                        ->first();

            $userB['business_id'] = $businessInfo->id; 
            $userB['user_id'] = $user->id;
            $userB['role_id'] = $request->role;
            $this->userBusiness($userB);

        return redirect()->back();
    }

    public function userBusiness($request)
    {       
        $newB = UserBusiness::create($request);
    }

    public function UsersList(Request $request)
    {
        $user = User::where('email','=',$request->email)->get();
        if($user->isNotEmpty()) {
            return $user->first();
        }else {
            return 'No Match found';
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($businessCode, $id)
    {
        $businesses = $this->userBusinessList();

        $businessInfo = $this->businessInfo($businessCode);

      return  view('dashboard.user_profile', compact('businesses', 'businessInfo'));
    }

    public function update($businessCode, Request $request, $id)
    {
        $rules = [
            'profile_photo' => 'image'
        ];
        if($request->has('profile_photo')) {
            if($id == auth()->user()->id){
                $user = User::find($id);
                $user->profile_photo =  $request->profile_photo->store('');
                $user->save();
                $success = "Photo Successfully Uploaded";
                return redirect()->back()->with(['data' => $success]);
            }
        }else  {
            $success = "Please select a photo";
            return redirect()->back()->with(['data' => $success]);
        }
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($businessCode, $id)
    {
        $user = User::find($id);

        $deleteUser = $user->delete();

        return redirect()->back();
    }
}
