<?php

namespace App\Http\Middleware;

use Closure;
use App\Business;

class CheckBusiness
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd($request->route('id'));
        $bussness_id = $request->route('businessCode');
        $business = Business::where(['business_code'=>$bussness_id, 'user_id'=>auth()->user()->id])->first();
        if($business == null){
            // return view(''); with error message
            return redirect()->route('404');
        }
        return $next($request);
    }
}
