<?php

namespace App\Http\Middleware;

use Closure;
use App\Subscribers;
use App\UserBusiness;
use App\Business;
use Carbon\Carbon;

class CheckSubscription
{
    public $attributes;
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //get the business authenticated user is added to
        $business = Business::where(['business_code'=>$request->route('businessCode')])->pluck('id');
        //check the role of the authenticated user
        $checkUserRole = UserBusiness::where(['user_id'=>auth()->user()->id, 'business_id'=>$business])
                        
                        ->pluck('role_id');
        //check if subscription already exist
        $subscription = Subscribers::where('user_id',auth()->user()->id)->orderBy('id','desc')->first();
        
        if($subscription) {
            $datetime1 = date_create($subscription->created_at);
            $datetime2 = date_create(Carbon::now());
            $interval = date_diff($datetime1, $datetime2);
            $daysused =  $interval->format('%a');
            $request->attributes->add(['days' => 'oo']);
            if ($checkUserRole[0] == 1 && $daysused > 30) {
                
                // return dd($subscription->created_at, $ok, $checkUserRole[0]);
                return redirect('home');
            }
        }
        

        return $next($request);
    }
}
