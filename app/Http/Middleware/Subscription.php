<?php

namespace App\Http\Middleware;

use Closure;
use App\Business;
use App\Subscribers;
use App\SubscriptionPlan;

class Subscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //get the user id of the business creator
        $business_created_by = Business::where(['business_code'=>$request->route('businessCode')])->pluck('user_id');
        
        //check if subscription already exist
        $subscription = Subscribers::where('user_id',$business_created_by)->orderBy('id','desc')->first();

        // get all subscription plans  
        $plans = SubscriptionPlan::orderBy('id', 'desc')->get();

        if($subscription->subscription_plan == 2){
            $success = "Not available on Starter Package";
            return redirect()->back()->with(['data' => $success, 'status' => 'error', 'plans' => $plans]);
        }

        return $next($request);
    }
}
