<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\DB;

trait BusinessTrait {

    public function userBusinessList()
    {
        $businesses = DB::table('businesses')
                    ->leftJoin('user_businesses', 'businesses.id', '=', 'user_businesses.business_id')
                    ->where('user_businesses.user_id', '=', auth()->user()->id)
                    ->select('businesses.*')
                    ->orderBy('businesses.id','asc')
                    ->get();
        return $businesses;
    }

    public function businessInfo($businessCode)
    {
        $businessInfo = DB::table('businesses')
                        ->where('businesses.business_code', '=', $businessCode)
                        ->first();
        return $businessInfo;
    }

}

