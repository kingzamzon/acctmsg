<?php

namespace App\Http\Traits;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

trait IncomeReportTrait {


    /**
     * @return table 
     * account_name | account_type | client_ledger.*
     */
    public function invoiceSheet($business, $from, $to) {

      if ($from == $to) {
        // Get all entries based on a single date
        $table =  DB::select("SELECT accounts.account_name, account_types.name as account_type,T1.*  FROM client_ledger T1
          left JOIN accounts
          ON T1.account_id = accounts.id
          left JOIN account_types
          ON account_types.id = accounts.type_id
          WHERE T1.created_at = (
          
          SELECT MAX(created_at) FROM client_ledger T2
          
          WHERE T1.account_id = T2.account_id
          
          )
          AND T1.business_id = $business->id
          AND T1.created_at LIKE '$from%'
          ");
      }else {
          // Convert date to laravel date format
          $from1 = new Carbon($from);
          $to2 = new Carbon($to);
          $from = $from1->toDateTimeString();
          $to = $to2->toDateTimeString();

          $table =  DB::select("SELECT accounts.account_name, account_types.name as account_type,T1.*  FROM client_ledger T1
            left JOIN accounts
            ON T1.account_id = accounts.id
            left JOIN account_types
            ON account_types.id = accounts.type_id
            WHERE T1.created_at = (
            
            SELECT MAX(created_at) FROM client_ledger T2
            
            WHERE T1.account_id = T2.account_id
            
            )
            AND T1.business_id = $business->id
            AND T1.created_at BETWEEN '$from' AND '$to'
            ");
      }

        return $table;
    }

}