<?php
namespace App\Http\Traits;

use App\Journal;
use Illuminate\Support\Facades\DB;

trait JournalTrait {
    
    public function debitTransactionJournal($transaction_id, $account_id, $amount)
    {
            
            $data['transaction_id'] = $transaction_id;
            $data['account_id'] = $account_id;
            $data['entity_type'] = 'D';
            $data['amount'] = $amount;
            return Journal::create($data);
    }

     public function creditTransactionJournal($transaction_id, $account_id, $amount)
    {
            $data['transaction_id'] = $transaction_id;
            $data['account_id'] = $account_id;
            $data['entity_type'] = 'C';
            $data['amount'] = $amount;
            return Journal::create($data);
    }
}