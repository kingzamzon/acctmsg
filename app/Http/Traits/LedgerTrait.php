<?php 
namespace App\Http\Traits;

use App\Account;
use App\ClientLedger;
use Illuminate\Support\Facades\DB;

trait LedgerTrait {
    public function brandsAll() {
        // Get all the brands from the Brands Table.
      

        return 1;
    }
        /**
     * Log Credited Account and Debited Account
     * @param $id Id of the new created journal
     * @param $accountid business_id
     * @return void
     *
     */
    public function debitLedger($business_id, $debit, $amount )
    {
        $account = Account::where('id','=',$debit)->first();
        $account_id = Account::where('id','=',$debit)->pluck('id');
        $todayDate = date("Y-m-d h:i:s");
        $debitAcct =  DB::table('client_ledger')
                        ->where('client_ledger.business_id', '=', $business_id)
                        ->where('client_ledger.account_id', '=', $account_id)
                        ->orderBy('id','desc')
                        ->first();
        // $debitAcct = ClientLedger::orderBy('id', 'desc')->get();
        
        //check if account is assests 
        if($account->type_id == 5 ||$account->type_id == 4 || $account->type_id == 6)
        {
            if($debitAcct){
                
                if($todayDate == $debitAcct->updated_at){
                    $newBalance = $debitAcct->balance + $amount;
                    return DB::table('client_ledger')
                    ->where('client_ledger.business_id', '=', $business_id)
                    ->where('client_ledger.account_id', '=', $debit)
                    ->update(['balance'=>$newBalance]);
                }else {
                    $newBalance = $debitAcct->balance + $amount;
                    $data['business_id'] = $business_id;
                    $data['account_id'] = $debit;
                    $data['balance'] = $newBalance;
                    return ClientLedger::create($data);
                }
             }
            else {
                $data['business_id'] = $business_id;
                $data['account_id'] = $debit;
                $data['balance'] = $amount;
                return ClientLedger::create($data);
            }

        }
        else 
        {
           if($debitAcct) {
             if($todayDate == $debitAcct->updated_at){
                    $newBalance = $debitAcct->balance - $amount;
                    return DB::table('client_ledger')
                            ->where('client_ledger.business_id', '=', $business_id)
                            ->where('client_ledger.account_id', '=', $debit)
                            ->update(['balance'=>$newBalance]);
                 }
            else {
                    $newBalance = $debitAcct->balance - $amount;
                    $data['business_id'] = $business_id;
                    $data['account_id'] = $debit;
                    $data['balance'] = $newBalance;
                    return ClientLedger::create($data);
            }
           }
            else {
                $data['business_id'] = $business_id;
                $data['account_id'] = $debit;
                $data['balance'] = $amount;
                return ClientLedger::create($data);
            } 
        }  
    }

            /**
     * Log Credited Account to ledger
     * @param $id Id of the new created journal
     * @param $accountid business_id
     * @return void
     *
     */
    public function creditLedger($business_id,$credit, $amount )
    {
        $account = Account::where('id','=',$credit)->first();
        $account_id = Account::where('id','=',$credit)->pluck('id');
        $todayDate = date("Y-m-d h:i:s");
        $creditAcct = DB::table('client_ledger')
                            ->where('business_id', '=', $business_id)
                            ->where('account_id', '=', $account_id)
                            ->orderBy('id','desc')
                            ->first();
        //check if account is assests 
        if($account->type_id == 5 || $account->type_id == 4 || $account->type_id == 6)
        {
            if($creditAcct) {
                if($todayDate == $creditAcct->updated_at){
                    $newBalance = $creditAcct->balance - $amount;
                    return DB::table('client_ledger')
                            ->where('client_ledger.business_id', '=', $business_id)
                            ->where('client_ledger.account_id', '=', $credit)
                            ->update(['balance'=>$newBalance]);
                 }
                else {
                    $newBalance = $creditAcct->balance - $amount;
                    $data['business_id'] = $business_id;
                    $data['account_id'] = $credit;
                    $data['balance'] = $newBalance;
                    return ClientLedger::create($data);
                }
            } 
            else {
                $data['business_id'] = $business_id;
                $data['account_id'] = $credit;
                $data['balance'] = $amount;
                return ClientLedger::create($data);
            }

        }
        else 
        {
            if($creditAcct) {
                if($todayDate == $creditAcct->updated_at){
                    $newBalance = $creditAcct->balance + $amount;
                    return DB::table('client_ledger')
                            ->where('client_ledger.business_id', '=', $business_id)
                            ->where('client_ledger.account_id', '=', $credit)
                            ->update(['balance'=>$newBalance]);
                 }
                else {
                    $newBalance = $creditAcct->balance + $amount;
                    $data['business_id'] = $business_id;
                    $data['account_id'] = $credit;
                    $data['balance'] = $newBalance;
                    return ClientLedger::create($data);
                }
            }
            else {
                $data['business_id'] = $business_id;
                $data['account_id'] = $credit;
                $data['balance'] = $amount;
                return ClientLedger::create($data);
            } 
        }

    }

}


