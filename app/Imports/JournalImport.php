<?php

namespace App\Imports;

use App\Journal;
use Maatwebsite\Excel\Concerns\ToModel;

class JournalImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Journal([
            'name' => $row[0],
            'type' => $row[1],
        ]);
    }
}
