<?php

namespace App;

use App\User;
use App\Customer;
use App\Business;
use App\BusinessAccounts;
use App\InvoiceTransaction;
use Illuminate\Database\Eloquent\Model;
// use Carbon;

class Invoice extends Model
{
    public $table = 'invoices';

    protected $fillable = [
        'business_id',
        'user_id',
        'customer_id',
        'invoice_no',
        'invoice_date',
        'due_date',
        'description',
        'discount',
        'vat'
    ];

    public static function generateInvoiceCode()
    {
        return strtoupper('INV-'.str_random(10));
    }

    public function getInvoiceDateAttribute($value)
    {
        return $this->attributes['invoice_date'] = date('d-F-Y', strtotime($value));
    }

    public function businessaccount()
    {
    	return $this->belongsTo(BusinessAccounts::class);
    }

    public function business()
    {
    	return $this->belongsTo(Business::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

     public function customer()
    {
    	return $this->belongsTo(Customer::class);
    }

    public function invoicetransaction()
    {
    	return $this->hasMany(InvoiceTransaction::class);
    }
}
