<?php

namespace App;

use App\Item;
use App\Invoice;
use Illuminate\Database\Eloquent\Model;

class InvoiceTransaction extends Model
{
    public $table = 'invoice_transactions';

    protected $fillable = [
        'invoice_id',
        'item_id',
        'description',
        'quantity',
        'unit_cost'
    ];

    public function invoice()
    {
    	return $this->belongsTo(Invoice::class);
    }

     public function item()
    {
    	return $this->belongsTo(Item::class);
    }

}
