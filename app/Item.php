<?php

namespace App;

use App\User;
use App\Account;
use App\Business;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'type',
        'unit',
        'price',
        'business_id',
        'account_id',
        'description'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function business()
    {
        return $this->belongsTo(Business::class);
    }
    
    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
