<?php

namespace App;


use App\Account;
use App\Transaction;
use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    public $table = 'journals';

    protected $fillable = [
    	'transaction_id',
    	'account_id',
    	'entity_type',
    	'amount'
    ];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

     public function account()
    {
        return $this->belongsTo(Account::class);
    }

}
