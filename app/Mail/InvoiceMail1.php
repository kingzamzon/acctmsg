<?php

namespace App\Mail;

use App\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceMail1 extends Mailable
{
    use Queueable, SerializesModels;

    public $invoice;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Invoice Statement From '.$this->invoice->business->name.'(#'.$this->invoice->invoice_no.')')
                    ->from($this->invoice->business->email)
                    ->with([
                        'url' => route('mailable', 
                        ['businesscode' => $this->invoice->business->business_code, 'id' => $this->invoice->id]),
                    ])
                    ->markdown('emails.invoice1');
    }
}
