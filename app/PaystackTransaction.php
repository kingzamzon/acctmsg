<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaystackTransaction extends Model
{
    protected $fillable = [
    	'user_id',
    	'amount',
    	'subscription_plan',
        'transaction_ref',
        'transaction_status'
    ];

    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function subscriptionplan()
    {
        return $this->belongsTo(SubscriptionPlan::class,'subscription_plan', 'id');
    }
}
