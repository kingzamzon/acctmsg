<?php

namespace App;

use App\User;
use App\SubscriptionPlan;
use Illuminate\Database\Eloquent\Model;

class Subscribers extends Model
{
	public $table = 'subscribers';
	 
	protected $fillable = [
		'user_id',
		'subscription_plan'
	];
    
     public function user()
    {
        return $this->hasMany(User::class);
    }

    public function subscriptionplan()
    {
        return $this->belongsTo(SubscriptionPlan::class,'subscription_plan', 'id');
    }
}
