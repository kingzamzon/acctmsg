<?php

namespace App;

use App\Subscribers;
use App\PaystackTransaction;
use Illuminate\Database\Eloquent\Model;

class SubscriptionPlan extends Model
{
    public $table = 'subscription_plans';

    protected $fillable = [
        'plan',
        'days',
        'description'
    ];

    public function subscribers()
    {
        return $this->hasMany(Subscribers::class,'subscription_plan', 'id');
    }

    public function paystacktransaction()
    {
        return $this->hasMany(PaystackTransaction::class,'subscription_plan', 'id');
    }
}
