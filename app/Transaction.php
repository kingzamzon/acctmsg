<?php

namespace App;

use App\User;
use App\Journal;
use App\Business;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
    	'user_id',
    	'business_id',
    	'description'
    ];

    public function journal()
    {
    	return $this->hasMany(Journal::class);
    }

     public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
