<?php

namespace App;

use App\Item;
use App\Journal;
use App\Account;
use App\Business;
use App\Subscribers;
use App\UserBusiness;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable 
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','profile_photo','scratch_card'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = strtolower($name);
    }

    public function getNameAttribute($name)
    {
        return ucwords($name);
    }

     public function userbusiness()
    {
        return $this->hasMany(UserBusiness::class);
    }

     public function business()
    {
        return $this->hasMany(Business::class);
    }

     public function subscribers()
    {
        return $this->belongsTo(Subscribers::class);
    }

     public function journal()
    {
        return $this->hasMany(Journal::class);
    }

    public function item()
    {
    	return $this->hasMany(Item::class);
    }

    public function account()
    {
    	return $this->hasMany(Account::class);
    }
}
