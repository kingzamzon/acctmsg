<?php

namespace App;

use App\User;
use App\Business;
use Illuminate\Database\Eloquent\Model;

class UserBusiness extends Model
{
	public $table = 'user_businesses';

    protected $fillable = [
        'business_id',
        'user_id',
        'role_id'
    ];

     public function business()
    {
    	return $this->belongsTo(Business::class);
    }

     public function user()
    {
    	return $this->belongsTo(User::class);
    }

}
