<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\Account;
use App\SubscriptionPlan;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

// $factory->define(Account::class, function (Faker $faker) {
//     return [
//         // randomElement([]);
//         'type_id' => $faker->randomelement([
//                 1, 2, 3, 4, 5, 6, 7
//                 ]),
//         'code' =>  $faker->unique()->randomelement([
//                 11123,23232,33222,43323,53132,61432,74423,81243,91343
//                 ]),
        
//         'account_name' =>  $faker->unique()->randomelement([
//                 'Account Payable', 
//                 'Account Receiveable',
//                 'Account Wage Accurude',
//                 'Prepaid Revenue', 
//                 'Sales',
//                 'Inventory',
//                 'Equipment', 
//                 'Shares Capacity',
//                 'Rent',
//                 ]),
//         'parent_id' => $faker->randomelement([
//                 1, 2, 3, 4, 5, 6, 7, 8, 9
//                 ]),
//     ];
// });

$factory->define(SubscriptionPlan::class, function (Faker $faker) {
    static $plan;

    return [
        'plan' => $plan = $faker->unique()->randomElement(['FREMIUM', 'PREMIUM']),
        'description' => $plan == 'FREMIUM' ? '30 Days Trials after which access is limited to only invoice feature' : 'Monthly payment to have unlimited access to all features.',
        'days' => 30
    ];
});
