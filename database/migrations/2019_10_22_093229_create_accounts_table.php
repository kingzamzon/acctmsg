<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('account_name');
            $table->unsignedBigInteger('type_id')->foreign();
            $table->integer('code');
            $table->string('parent_id')->nullable();
            $table->unsignedBigInteger('user_id')->foreign()->nullable();
            $table->enum('entity_type', ['Credit', 'Debit']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('type_id')->references('id')->on('account_types');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
