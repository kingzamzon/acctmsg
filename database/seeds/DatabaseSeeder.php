<?php

use App\AccountType;
use App\Account;
use App\SubscriptionPlan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        // AccountType::truncate();
        // Account::truncate();
        SubscriptionPlan::truncate();
        

        // AccountType::flushEventListeners();
        // Account::flushEventListeners();
        SubscriptionPlan::flushEventListeners();

        // factory(AccountType::class, 7)->create();
        // factory(Account::class, 9)->create();
        factory(SubscriptionPlan::class, 2)->create();
        // $this->call(UsersTableSeeder::class);
    }
}
