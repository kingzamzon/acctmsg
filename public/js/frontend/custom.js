// JavaScript Document
$(function(){
	
/* Main banner Gradient */
	var granimInstance = new Granim({
		element: '#banner-canvas',
		direction: 'top-bottom',
		opacity: [.7, .5, .7],
		isPausedWhenNotInView: true,
		states : {
			"default-state": {
				gradients: [					
					['#e65c9d', '#000000', '#5d4083'],
					['#33a451', '#000000', '#000000'],
					['#ffbd2f', '#000000', '#2c283a'],													
				],
				transitionSpeed: 3000
			}
		},
	});
	
/* Services Gradient */
	var granimInstance = new Granim({
		element: '#canvas-basic',
		direction: 'top-bottom',
		opacity: [1, 1],
		isPausedWhenNotInView: true,
		states : {
			"default-state": {
				gradients: [					
					['#ffc736', '#ff8108'],
					['#ff8108', '#ffc736']										
				],
				transitionSpeed: 3000
			}
		},
	});
	
/* Mobile navigation */
	$('.mb-nav-icon').click(function(){  		
		$(this).toggleClass('active');
	});			
	
	function mb_nav() {
       	$menuLeft = $('#nav');
		$nav_list = $('.nav-icon');
		
		$nav_list.click(function() {
			$(this).toggleClass('active');
			$('.pushmenu-push').toggleClass('pushmenu-push-toright');
			$menuLeft.toggleClass('pushmenu-open');
			$('html').toggleClass('no-scroll');
		}); 
    }	
	mb_nav();	
	
/* Mobile nav click remove class */
	var clickThis = '.main-nav li a';
	var link = '.nav-icon';
	
	$(clickThis).on('click',function(){
		//alert($(this).parent().parent().parent().parent().hasClass());
		if($(this).parents().eq(3).attr('class') === 'nav-part horizontal-nav' ){			
		}else {			
			$(link)[0].click();
		}
		$('body').find('.pushmenu-open').removeClass("pushmenu-open");	
		$('body').find('.pushmenu-push').removeClass("pushmenu-push-toright");
		$('html').removeClass('no-scroll');		
	});
		
	
	/* Aroow button click page scroll bottom */
	$('#hpart #nav .main-nav li a').bind('click',function(event){
		var $anchor = $(this);

		$('html, body').stop().animate({
			scrollTop: $($anchor.attr('href')).offset().top
		}, 500);
		
		event.preventDefault();
	});
	
	
	/* Fix navigation animation effect */
	
	// navbar scrolling background
	var winWidth = $(window).width();
	//if( winWidth >= 770 ){
		$(window).on("scroll",function () {
			var hpartHeight = $('#hpart').height();
			var bodyScroll = $(window).scrollTop(),
				navbar = $(".nav-part");
	
			if(bodyScroll > hpartHeight){			
				navbar.addClass('horizontal-nav').fadeIn('slow');
				if( winWidth <= 630 ){					
					navbar.parent('#hpart').addClass('mbnav-active');
				}				
			}else{
				navbar.removeClass('horizontal-nav');
				if( winWidth <= 630 ){
					navbar.parent('#hpart').removeClass('mbnav-active');
				}
			}
		});
		
	if( winWidth <= 630 ){	
		$('.nav-part .main-nav li a').on('click',function(){				
			if($(this).closest('div.nav-part').hasClass('horizontal-nav')){ 								
				$('.mb-nav-icon, .nav-icon').removeClass('active');				
			}							
		});		
		$('.nav-part .main-nav li a.venobox_custom').on('click',function(){
			alert('hi');
		});
	}
	//}
    
	
	
/* Back to top button */
	$(window).load(function(){
		$(window).scroll(function () {
			if ($(this).scrollTop() > 300) {
					$('#back-top').fadeIn();
				} else {
					$('#back-top').fadeOut();
				}
			});
		});
		jQuery('.backtotop').click(function(){
		jQuery('html, body').animate({scrollTop:0}, 'slow');	
	});
});


/* Appointment Popup */
// $('.venobox_custom').venobox({
// 	framewidth: '50%',        // default: ''	
// 	border: '10px',             // default: '0'
// 	bgcolor: '#33a451',         // default: '#fff'
// 	titleattr: 'data-title',    // default: 'title'
// 	numeratio: true,            // default: false
// 	infinigall: true            // default: false
// });


// Contact Form 
		$(".contactform").validate({
	   submitHandler: function(form) {
		   var name = $("input#name").val();
		   var email = $("input#email").val();
		   var url = $("input#url").val();
		   var message = $("input#message").val();
		   
		   var dataString = 'name='+ name + '&email=' + email + '&url=' + url+'&message='+message;
		  $.ajax({
		  type: "POST",
		  url: "email.php",
		  data: dataString,
		  success: function() {
			  $('#contactmsg').remove();
			  $('.contactform').prepend("<div id='contactmsg' class='successmsg'>Form submitted successfully!</div>");
			   $('#contactmsg').delay(1500).fadeOut(500);
			  $('#submit_id').attr('disabled','disabled');
			  }
		 	});   
	   return false;
	  	}
		});

