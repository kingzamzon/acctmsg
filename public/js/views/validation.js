$(function (){
  // $.validator.setDefaults( {
  //   submitHandler: function () {
  //     alert( 'submitted!' );
  //   }
  // });
  $('#addJournalForm').validate({
    rules: {
      amount: {
        required: true,
        minlength: 2
      },
      description: {
        required: true,
        minlength: 10
      }
    },
    messages: {
      amount: {
        required: 'Please enter a amount',
        minlength: 'Your amount must consist of at least 2 characters'
      },
      description: {
        required: 'Please provide a description',
        minlength: 'Your description must be at least 10 characters long'
      }
    },
    errorElement: 'em',
    errorPlacement: function ( error, element ) {
      error.addClass( 'invalid-feedback' );
      if ( element.prop( 'type' ) === 'checkbox' ) {
        error.insertAfter( element.parent( 'label' ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).addClass( 'is-invalid' ).removeClass( 'is-valid' );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).addClass( 'is-valid' ).removeClass( 'is-invalid' );
    }
  });

  $('#addBusinessForm').validate({
    rules: {
      name: {
        required: true,
        minlength: 2
      },
      address: {
        required: true,
        minlength: 10
      },
      phone: {
        required: true,
        minlength: 9
      },
      email: {
        required: true,
        email: true
      },
      founders: {
        required: true,
        minlength: 10
      },
      members: {
        required: true,
        maxlength: 10
      },
      description: {
        required: true,
        minlength: 10
      }
    },
    messages: {
      name: {
        required: 'Please enter a business name',
        minlength: 'Your business name must consist of at least 2 characters'
      },
      address: {
        required: 'Please provide a business address',
        minlength: 'Your business address must be at least 10 characters long'
      },
      phone: {
        required: 'Please provide a business phone',
        minlength: 'Your business phone must be at least 10 characters long'
      },
      founders: {
        required: 'Please provide a names of founders',
        minlength: 'Your founders must be at least 10 characters long'
      },
      members: {
        required: 'Please provide a total members',
        maxlength: 'Your members minimum is 10 characters long'
      },
      description: {
        required: 'Please provide a description',
        minlength: 'Your description must be at least 10 characters long'
      },
      email: 'Please enter a valid email address'
    },
    errorElement: 'em',
    errorPlacement: function ( error, element ) {
      error.addClass( 'invalid-feedback' );
      if ( element.prop( 'type' ) === 'checkbox' ) {
        error.insertAfter( element.parent( 'label' ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).addClass( 'is-invalid' ).removeClass( 'is-valid' );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).addClass( 'is-valid' ).removeClass( 'is-invalid' );
    }
  });

});
