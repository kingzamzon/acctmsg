## TODO:
Move Cash in debit account below
Use the column to place if it operation, financing and investing- that column that is useless
sum up all reveni=u, cash flow
display all sum all investment
check downloaded documents for more info.

## Run composer install 
	cause of `composer require maatwebsite/excel`
## Edited the AuthenticatesUsers in Laravel framework

Cash flow from operating activities (CFO) indicates the amount of money a company brings in from its ongoing, regular business activities, such as manufacturing and selling goods or providing a service to customers. Operating activities include generating revenue, paying expenses, and funding working capital. It is calculated by taking a company’s (1) net income, (2) adjusting for non-cash items, and (3) accounting for changes in working capital.This value, which measures a business's profitability, is derived directly from the net income shown in the company's income statement for the corresponding period. The cash flow statement must then reconcile net income to net cash flows by adding back non-cash expenses such as depreciation and amortization.A positive change in assets from one period to the next is recorded as a cash outflow, while a positive change in liabilities is recorded as a cash inflow. Inventories, accounts receivable, tax assets, accrued revenue, and deferred revenue are common examples of assets for which a change in value will be reflected in cash flow from operating activities.

Accounts payable, tax liabilities, and accrued expenses are common examples of liabilities for which a change in value is reflected in cash flow from operations

Additions to property, plant, equipment, capitalized software expense, cash paid in mergers and acquisitions, purchase of marketable securities, and proceeds from the sale of assets are all examples of entries that should be included in the cash flow from investing activities section.
The cash flow from investing section shows the cash used to purchase fixed and long-term assets, such as plant, property, and equipment (PPE), as well as any proceeds from the sale of these assets.

Proceeds from the issuance of stock, proceeds from the issuance of debt, dividends paid, cash paid to repurchase common stock, and cash paid to retire debt are all entries that should be included in the cash flow from financing activities section.The cash flow from financing section shows the source of a company's financing and capital as well as its servicing and payments on the loans. For example, proceeds from the issuance of stocks and bonds, dividend payments, and interest payments will be included under financing activities. The financing activity in the cash flow statement focuses on how a firm raises capital and pays it back to investors through capital markets.
The largest line items in the cash flow from financing activities statement are dividends paid, repurchase of common stock, and proceeds from the issuance of debt.such as retiring or paying off long-term debt or making a dividend payment to shareholders.  e.g
Receiving cash from issuing stock or spending cash to repurchase shares
Receiving cash from issuing debt or paying down debt
Paying cash dividends to shareholders
Proceeds received from employees exercising stock options
Receiving cash from issuing hybrid securities, such as convertible debt

A company's cash flow from financing activities refers to the cash inflows and outflows resulting from the issuance of debt, the issuance of equity, dividend payments, and the repurchase of existing stock.

What do Investing Activities Not Include?

Now that you have a solid understanding of what’s included, let’s look at what’s not included.

Not included items are:

    Interest payments or dividends
    Debt, equity, or other forms of financing
    Deprecation of capital assets (even though the purchase of these assets is part of investing)
    All income and expenses related to normal business operations

Task:
Add Owner's Equity to List of accounts with Equity 
is owner equity belong to current assets?
For victor use database for contact and display it in admin template

when you credit and debit an account (cash) you will have zero

conclusion:
revenue - cash intake
expense - cash outtake

Investing activities: Investing activities include purchases of long-term assets (such as property, plant, and equipment), acquisitions of other businesses, and investments in marketable securities (stocks and bonds).
 Inflows:

    Proceeds from disposal of property, plant, and equipment
    Cash receipts from the disposal of debt instruments of other entities
    Receipts from sale-of-equity instruments of other entities

Outflows:

    Payments for acquisition of property, plant, and equipment
    Payments for purchase of debt instruments of other entities
    Payments for purchase of equity instruments of other entities
    Sales/maturities of investments
    Purchasing and selling long-term assets and other investments

 Types of Cash Flow

Overall, the cash flow statement provides an account of the cash used in operations, including working capital, financing, and investing. There are three sections–labeled activities–on the cash flow statement.
Cash Flow from Operating

Operating activities include any spending or sources of cash that's involved in a company's day-to-day business activities. Any cash spent or generated from the company's products or services is listed in this section, including:

    Cash received from the sale of goods and services
    Interest payments
    Salary and wages paid
    Payments to suppliers for inventory or goods needed for production
    Income tax payments

Cash Flow from Financing

Cash generated or spent on financing activities shows the net cash flows involved in funding the company's operations. Financing activities include:

    Dividend payments
    Stock repurchases
    Bond offerings–generating cash

Common items included in the cash flow from Financing activities are as follows –

    Cash dividend paid (cash outflow)
    Increases in short-term borrowings (cash inflows)
    The decrease in short-term borrowings (cash outflow)
    Long-term borrowings (cash inflows)
    Repayment of long-term borrowings  (cash outflow)
    Share sales (cash inflows)
    Share repurchases (cash outflow)


 Cash Flow from Investing

Cash flows from investing activities provides an account of cash used in the purchase of non-current assets–or long-term assets– that will deliver value in the future. 

Investing activity is an important aspect of growth and capital. A change to property, plant, and equipment (PPE), a large line item on the balance sheet, is considered an investing activity. When investors and analysts want to know how much a company spends on PPE, they can look for the sources and uses of funds in the investing section of the cash flow statement.

Capital expenditures (CapEx), also found in this section, is a popular measure of capital investment used in the valuation of stocks. An increase in capital expenditures means the company is investing in future operations. However, capital expenditures are a reduction in cash flow. Typically, companies with a significant amount of capital expenditures are in a state of growth.

Below are a few examples of cash flows from investing activities along with whether the items generate negative or positive cash flow.

    Purchase of fixed assets–cash flow negative
    Purchase of investments such as stocks or securities–cash flow negative
    Lending money–cash flow negative
    Sale of fixed assets–cash flow positive
    Sale of investment securities–cash flow positive
    Collection of loans and insurance proceeds–cash flow positive

Limitation: could not reduce raw material due to the fact that raw material is not in credit account

Short cut for categories
```
Income - I

Current Asset - CA

Current Liablity - CL

Expenses - Ex

Non Current Asset NCA

Non Current Liablity NCL

Owner Equity EQ
```

commented out foreach plan in welcome.blade.php and the code that follows due to 60 days trials 

