@extends('layouts.app')

@section('page-title')
    Contact Us
@endsection

@section('content')
    @include('layouts.header')
    <!-- Start Contact -->
    <section class="section pt-5 mt-4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 p-0">
                    <div class="card map border-0">
                        <div class="card-body p-0">
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->

        <div class="container mt-100 mt-60">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0 order-2 order-md-1">
                    <div class="card custom-form rounded shadow border-0">
                        <div class="card-body">
                            @if(Session::get('data'))
                            <div id="message">
                                Thank you for contacting us. We will get back to you shortly.
                            </div>
                            @endif
                            <form method="post" action="{{ route('contactus.store') }}" name="contact-form" id="contact-form">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group position-relative">
                                            <label>Your Name <span class="text-danger">*</span></label>
                                            <i data-feather="user" class="fea icon-sm icons"></i>
                                            <input name="name" id="name" type="text" class="form-control pl-5" placeholder="First Name :">
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-lg-6">
                                        <div class="form-group position-relative">
                                            <label>Your Email <span class="text-danger">*</span></label>
                                            <i data-feather="mail" class="fea icon-sm icons"></i>
                                            <input name="email" id="email" type="email" class="form-control pl-5" placeholder="Your email :">
                                        </div> 
                                    </div><!--end col-->
                                    <div class="col-lg-12">
                                        <div class="form-group position-relative">
                                            <label>Your URL <span class="text-danger">*</span></label>
                                            <i data-feather="mail" class="fea icon-sm icons"></i>
                                            <input name="url" id="email" type="url" class="form-control pl-5" placeholder="Your url :">
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-lg-12">
                                        <div class="form-group position-relative">
                                            <label>Comments</label>
                                            <i data-feather="message-circle" class="fea icon-sm icons"></i>
                                            <textarea name="message" id="comments" rows="4" class="form-control pl-5" placeholder="Your Message :"></textarea>
                                        </div>
                                    </div>
                                </div><!--end row-->
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <input type="submit" id="submit" name="send" class="submitBnt btn btn-primary btn-block" value="Send Message">
                                        <div id="simple-msg"></div>
                                    </div><!--end col-->
                                </div><!--end row-->
                            </form><!--end form--> 
                        </div>
                    </div><!--end custom-form-->
                </div><!--end col-->

                <div class="col-lg-7 col-md-6 order-1 order-md-2">
                    <div class="title-heading ml-lg-4">
                        <h4 class="mb-4">Contact Details</h4>
                        <p class="text-muted">Start working with <span class="text-primary font-weight-bold">Startupclerk</span> that can provide everything you need to generate awareness, drive traffic, connect.</p>
                        <div class="media contact-detail align-items-center mt-3">
                            <div class="icon">
                                <i data-feather="mail" class="fea icon-m-md text-dark mr-3"></i>
                            </div>
                            <div class="media-body content">
                                <h4 class="title font-weight-bold mb-0">Email</h4>
                                <a href="mailto:hello@startupclerk.com" class="text-primary">hello@startupclerk.com</a>
                            </div>
                        </div>
                        
                        <div class="media contact-detail align-items-center mt-3">
                            <div class="icon">
                                <i data-feather="phone" class="fea icon-m-md text-dark mr-3"></i>
                            </div>
                            <div class="media-body content">
                                <h4 class="title font-weight-bold mb-0">Phone</h4>
                                <a href="tel:+234-9065042956" class="text-primary">+234 9065042956</a>
                            </div>
                        </div>

                        <div class="media contact-detail align-items-center mt-3">
                            <div class="icon">
                                <i data-feather="phone" class="fea icon-m-md text-dark mr-3"></i>
                            </div>
                            <div class="media-body content">
                                <h4 class="title font-weight-bold mb-0">Mobile</h4>
                                <a href="tel:+234-8066071971" class="text-primary">+234 8066071971</a>
                            </div>
                        </div>
                        
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- End contact -->
@endsection


