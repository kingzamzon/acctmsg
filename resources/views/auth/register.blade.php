@extends('layouts.app')

@section('page-title')
    Register
@endsection

@section('content')
<div class="back-to-home rounded d-none d-sm-block">
    <a href="{{ route('homepage') }}" class="btn btn-icon btn-soft-primary"><i data-feather="home" class="icons"></i></a>
</div>

<!-- Hero Start -->
<section class="cover-user bg-white">
    <div class="container-fluid px-0">
        <div class="row no-gutters position-relative">
            <div class="col-lg-4 cover-my-30 order-2" style="padding-top: 100px; padding-bottom: 150px;">
                <div class="cover-user-img d-flex align-items-center">
                    <div class="row">
                        <div class="col-12">
                            <div class="card login_page border-0" style="z-index: 1">
                                <div class="card-body p-0 pt-5 mt-5">
                                    <h4 class="card-title text-center">
                                        <a href="{{ route('homepage') }}" >
                                            <img src="{{ asset('land/images/logo-dark.png') }}" class="l-dark" height="50" alt=""> 
                                        </a><br>
                                        Signup
                                    </h4>
                                    <div class="container">
                                        <form class="login-form mt-4" method="POST" action="{{ route('register') }}">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group position-relative">                                               
                                                        <label>Fullname <span class="text-danger">*</span></label>
                                                        <i data-feather="user" class="fea icon-sm icons"></i>
                                                        <input type="text" class="form-control pl-5 @error('name') is-invalid @enderror" placeholder="Full Name" name="name" value="{{ old('name') }}" required="">
                                                        @error('name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div><!--end col-->

                                                <div class="col-md-12">
                                                    <div class="form-group position-relative">
                                                        <label>Your Email <span class="text-danger">*</span></label>
                                                        <i data-feather="mail" class="fea icon-sm icons"></i>
                                                        <input type="email" class="form-control pl-5 @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required="">
                                                        @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                    </div>
                                                </div><!--end col-->

                                                <div class="col-md-12">
                                                    <div class="form-group position-relative">
                                                        <label>Password <span class="text-danger">*</span></label>
                                                        <i data-feather="key" class="fea icon-sm icons"></i>
                                                        <input type="password" class="form-control pl-5 @error('password') is-invalid @enderror" name="password" placeholder="Password" required="">
                                                        @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div><!--end col-->

                                                <div class="col-md-12">
                                                    <div class="form-group position-relative">
                                                        <label>Confirm Password <span class="text-danger">*</span></label>
                                                        <i data-feather="key" class="fea icon-sm icons"></i>
                                                        <input type="password" class="form-control pl-5" name="password_confirmation" placeholder="Confirm Password" required="">
                                                    </div>
                                                </div><!--end col-->

                                                <div class="col-md-12">
                                                    <div class="form-group position-relative">
                                                        <label>Liscense Key</label>
                                                        <i data-feather="key" class="fea icon-sm icons"></i>
                                                        <input type="text" class="form-control pl-5 @error('scratch_card') is-invalid @enderror" name="scratch_card" placeholder="Liscense Key">
                                                        @error('scratch_card')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div><!--end col-->

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                            <label class="custom-control-label" for="customCheck1">I Accept <a href="#" class="text-primary">Terms And Condition</a></label>
                                                        </div>
                                                    </div>
                                                </div><!--end col-->

                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-primary btn-block">Register</button>
                                                </div><!--end col-->

                                                <div class="mx-auto">
                                                    <p class="mb-0 mt-3"><small class="text-dark mr-2">Already have an account ?</small> <a href="{{ route('login') }}" class="text-dark font-weight-bold">Sign in</a></p>
                                                </div>
                                            </div><!--end row-->
                                        </form>  

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--end row-->
                </div> <!-- end about detail -->
            </div> <!-- end col -->    

            <div class="col-lg-8 offset-lg-4 padding-less img order-1 d-none d-sm-block " style="background-image:url('land/images/user/02.jpg')" data-jarallax='{"speed": 0.5}'></div><!-- end col -->    
        </div><!--end row-->
    </div><!--end container fluid-->
</section><!--end section-->
<!-- Hero End -->
@endsection

