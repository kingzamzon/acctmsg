@extends('layouts.app')

@section('page-title')
    Services
@endsection

@section('content')
    @include('layouts.header')
     <!-- Hero Start -->
     <section class="bg-half bg-light d-table w-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="page-next-level">
                        <h4 class="title"> Services </h4>
                        <div class="page-next">
                            <nav aria-label="breadcrumb" class="d-inline-block">
                                <ul class="breadcrumb bg-white rounded shadow mb-0">
                                    <li class="breadcrumb-item active" aria-current="page">Services</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div> <!--end container-->
    </section><!--end section-->
    <!-- Hero End -->

    <!-- Shape Start -->
    <div class="position-relative">
        <div class="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!--Shape End-->

     <!-- Feature Start -->
     <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-12">
                    <div class="features mt-5">
                        <div class="image position-relative d-inline-block">
                            <img src="{{ asset('land/images/icon/pen.svg') }}" class="avatar avatar-small" alt="">
                        </div>

                        <div class="content mt-4">
                            <h4 class="title-2">Accounting Journal</h4>
                            <p class="text-muted mb-0">Record daily financial activities in your business.</p>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-md-4 col-12 mt-5">
                    <div class="features">
                        <div class="image position-relative d-inline-block">
                            <img src="{{ asset('land/images/icon/video.svg') }}" class="avatar avatar-small" alt="">
                        </div>

                        <div class="content mt-4">
                            <h4 class="title-2">Income Statement</h4>
                            <p class="text-muted mb-0">Reports all income and expenses in your business, helps to inform of profits or losses made in your business.</p>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-md-4 col-12 mt-5">
                    <div class="features">
                        <div class="image position-relative d-inline-block">
                            <img src="{{ asset('land/images/icon/Intellectual.svg') }}" class="avatar avatar-small" alt="">
                        </div>

                        <div class="content mt-4">
                            <h4 class="title-2">Balance Sheet</h4>
                            <p class="text-muted mb-0">Provides a statement of your assets, liabilities and capital of your business.</p>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-md-4 col-12 mt-5">
                    <div class="features">
                        <div class="image position-relative d-inline-block">
                            <img src="{{ asset('land/images/icon/user.svg') }}" class="avatar avatar-small" alt="">
                        </div>

                        <div class="content mt-4">
                            <h4 class="title-2">Cashflow Statement</h4>
                            <p class="text-muted mb-0">Reports how chances in balance sheet accounts and income affect cash and cash equivalent.</p>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-md-4 col-12 mt-5">
                    <div class="features">
                        <div class="image position-relative d-inline-block">
                            <img src="{{ asset('land/images/icon/calendar.svg') }}" class="avatar avatar-small" alt="">
                        </div>

                        <div class="content mt-4">
                            <h4 class="title-2">Invoice</h4>
                            <p class="text-muted mb-0">Generates invoice for your business transactions with clients.</p>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->

    </section><!--end section-->
    <!-- Feature Start -->

@endsection


