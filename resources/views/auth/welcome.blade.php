@extends('layouts.app')

@section('page-title')
    Welcome
@endsection

@section('content')
<!-- Navbar STart -->
@include('layouts.header')
<!-- Hero Start -->
    <section class="bg-half-260 pb-lg-0 pb-md-4 bg-primary d-table w-100">
        <div class="bg-overlay bg-black" style="opacity: 0.8;"></div>
        <div class="container">
            <div class="row position-relative" style="z-index: 1;">
                <div class="col-md-7 col-12 mt-lg-5">
                    <div class="title-heading">
                        <h1 class="heading text-white title-dark mb-4">You don't need an<br> accountant! You need Startupclerk!</h1>
                        <p class="para-desc text-white-50">Easy bookkeeping for startups.</p>
                        <div class="watch-video mt-4 pt-2">
                            <a href="{{ route('register') }}" class="btn btn-primary mb-2">Get Started</a>
                            <a href="#" class="video-play-icon watch text-white title-dark mb-2 ml-2">
                                <i class="fas fa-play play-icon-circle text-center d-inline-block mr-2 rounded-circle"></i> WATCH VIDEO</a>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-md-5 col-12 mt-4 pt-2 mt-sm-0 pt-sm-0">
                    <div class="shape-before">
                        <div class="carousel-cell"><img src="{{ asset('land/images/landing/2.jpg') }}" class="img-fluid rounded-md" alt=""></div>
                        <img src="{{ asset('land/images/shapes/shape1.png') }}" class="img-fluid shape-img" alt="">
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <div class="position-relative">
        <div class="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 250" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M720 125L2160 0H2880V250H0V125H720Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!-- Hero End -->

    <!-- Start -->
    <section class="section">
        <div class="container">
            <div class="row justify-ontent-center">
                <div class="col-12">
                    <div class="section-title text-center mb-4 pb-2">
                        <h4 class="title mb-4">You just got a life saver!</h4>
                        <p class="text-muted para-desc mb-0 mx-auto">
                            Saving 1000 businesses from crashing one at a time.</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row">
                <div class="col-lg-4 col-md-6 mt-4 pt-2">
                    <div class="media key-feature align-items-center p-3 rounded-md shadow">
                        <div class="icon text-center rounded-circle mr-3">
                            <i data-feather="book" class="fas fa-book text-primary"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="title mb-0">Accounting Journal</h4>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-lg-4 col-md-6 mt-4 pt-2">
                    <div class="media key-feature align-items-center p-3 rounded-md shadow">
                        <div class="icon text-center rounded-circle mr-3">
                            <i data-feather="file-invoice-dollar" class="fas fa-file-invoice-dollar text-primary"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="title mb-0">Income Statement</h4>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-lg-4 col-md-6 mt-4 pt-2">
                    <div class="media key-feature align-items-center p-3 rounded-md shadow">
                        <div class="icon text-center rounded-circle mr-3">
                            <i data-feather="balance-scale" class="fas fa-balance-scale text-primary"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="title mb-0">Balance Sheet</h4>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-lg-4 col-md-6 mt-4 pt-2">
                    <div class="media key-feature align-items-center p-3 rounded-md shadow">
                        <div class="icon text-center rounded-circle mr-3">
                            <i data-feather="chart-line" class="fas fa-chart-line text-primary"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="title mb-0">Cashflow Statement</h4>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-lg-4 col-md-6 mt-4 pt-2">
                    <div class="media key-feature align-items-center p-3 rounded-md shadow">
                        <div class="icon text-center rounded-circle mr-3">
                            <i data-feather="file-invoice" class="fas fa-file-invoice text-primary"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="title mb-0">Invoice</h4>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-12 mt-4 pt-2 text-center">
                    <a href="{{ route('services') }}" class="btn btn-primary">See More <i class="fas fa-arrow-right"></i></a>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--ed container-->

        <div class="container mt-100 mt-60">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title text-center text-lg-left">
                        <h4 class="title mb-4">
                            Saving <span class="text-primary">1000</span> businesses from crashing one at a time.
                        </h4>
                        <p class="text-muted mb-0 mx-auto para-desc"><span class="text-primary font-weight-bold">Startupclerk</span> is encapsulated with a blend of exceptional simplicity and mind-blowing efficiency 
                        		along with interesting features and tools that enable you do reporting, spooling of your financial records to excel and/or printing.</p>
                    </div>
                </div><!--end col-->

                <div class="col-lg-6">
                    <div class="row" id="counter">
                        <div class="col-md-6 mt-4 mt-lg-0 pt-2 pt-lg-0">
                            <div class="content text-center">
                                <h1 class="mb-0"><span class="counter-value">{{ $users }}</span>+</h1>
                                <ul class="list-unstyled mb-0 h5">
                                    <li class="list-inline-item"><i class="fas fa-heart text-danger"></i></li>
                                    <li class="list-inline-item"><i class="fas fa-heart text-danger"></i></li>
                                    <li class="list-inline-item"><i class="fas fa-heart text-danger"></i></li>
                                    <li class="list-inline-item"><i class="fas fa-heart text-danger"></i></li>
                                    <li class="list-inline-item"><i class="fas fa-heart text-danger"></i></li>
                                </ul>
                                <h6>Trusted Users</h6>
                            </div>
                        </div>

                        <div class="col-md-6 mt-4 mt-lg-0 pt-2 pt-lg-0">
                            <div class="content text-center">
                                <h1 class="mb-0"><span class="counter-value">{{ $businesses }}</span>+</h1>
                                <ul class="list-unstyled mb-0 h5">
                                    <li class="list-inline-item"><i class="fas fa-heart text-danger"></i></li>
                                    <li class="list-inline-item"><i class="fas fa-heart text-danger"></i></li>
                                    <li class="list-inline-item"><i class="fas fa-heart text-danger"></i></li>
                                    <li class="list-inline-item"><i class="fas fa-heart text-danger"></i></li>
                                    <li class="list-inline-item"><i class="fas fa-heart text-danger"></i></li>
                                </ul>
                                <h6>Registered Businesses</h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- End -->

    <!-- Start -->
    <section class="section pt-0">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="video-solution-cta position-relative" style="z-index: 1;">
                        <div class="position-relative">
                            <img src="{{ asset('land/images/email/1.png') }}" class="img-fluid" alt="">
                            <div class="play-icon">
                                <a href="#" class="play-btn video-play-icon">
                                    <i class="fas fa-play text-primary rounded-circle bg-white shadow-lg"></i>
                                </a>
                            </div>
                        </div>
                        <div class="content mt-md-4 pt-md-2">
                            <div class="row justify-content-center">
                                <div class="col-lg-10 text-center">
                                    <div class="row align-items-center">
                                        <div class="col-md-6 mt-4 pt-2">
                                            <div class="section-title text-md-left">
                                                <h6 class="text-white-50">Every business should at least have an income statement and a balance sheet statement.</h6>
                                                <h4 class="title text-white mb-0 title-dark">Do you keep the books?</h4>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-12 mt-4 pt-md-2">
                                            <div class="section-title text-md-right">
                                                <p class="text-white-50 para-desc">With <span class="text-light title-dark">Startupclerk</span> it's <span class="text-light title-dark">So Easy!</span>.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row -->
            <div class="feature-posts-placeholder bg-primary"></div>
        </div><!--end container-->
    </section><!--end section-->
    <!-- End -->

    <!-- Start -->
    <section class="section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-6">
                    <img src="{{ asset('land/images/course/online/ab01.jpg') }}" class="img-fluid rounded-md shadow-lg" alt="">
                </div><!--end col-->

                <div class="col-lg-7 col-md-6 mt-4 pt-2 mt-sm-0 pt-sm-0">
                    <div class="section-title text-md-left text-center">
                        <h4 class="title mb-4">Poor bookkeeping constitute the <br /> top 4 causes of startup crashes.</h4>
                        <p class="text-muted mb-0 para-desc">Start working with <span class="text-primary font-weight-bold">Startupclerk</span> to be informed on your business financial position and cash flow.</p>
                        
                        <div class="media align-items-center text-left mt-4 pt-2">
                            <div class="text-primary h4 mb-0 mr-3 p-3 rounded-md shadow">
                                <i class="uil uil-capture"></i>
                            </div>
                            <div class="media-body">
                                <a href="javascript:void(0)" class="text-dark h6">Evade financial crisis</a>
                            </div>
                        </div>

                        <div class="media align-items-center text-left mt-4">
                            <div class="text-primary h4 mb-0 mr-3 p-3 rounded-md shadow">
                                <i class="uil uil-file"></i>
                            </div>
                            <div class="media-body">
                                <a href="javascript:void(0)" class="text-dark h6">Prevent accumulation of tax debts</a>
                            </div>
                        </div>

                        <div class="media align-items-center text-left mt-4">
                            <div class="text-primary h4 mb-0 mr-3 p-3 rounded-md shadow">
                                <i class="uil uil-credit-card-search"></i>
                            </div>
                            <div class="media-body">
                                <a href="javascript:void(0)" class="text-dark h6">Save your business from iminent crash</a>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->

        <div class="container mt-100 mt-60">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-6 order-1 order-md-2">
                    <div class="text-right">
                        <img src="{{ asset('land/images/course/online/ab03.jpg') }}" class="img-fluid rounded-md shadow-lg" alt="">
                    </div>
                </div><!--end col-->

                <div class="col-lg-7 col-md-6 order-2 order-md-1 mt-4 pt-2 mt-sm-0 pt-sm-0">
                    <div class="section-title text-md-left text-center">
                        <h4 class="title mb-4">You don't need a fortune <br> to keep the books</h4>
                        <p class="text-muted mb-0 para-desc">
                            You can use <span class="text-primary font-weight-bold">StartupClerk</span> for free to do financial bookkeeping for your business. It ships with features such as Accounting Journal, Balance Sheet, Income Statement, Cash Flow Statement and Invoice. 
                        </p>
                        
                        <div class="row justify-content-center">
                            <div class="col-lg-12 mt-4">
                                <div id="single-owl" class="owl-carousel owl-theme">
                                    <div class="media customer-testi m-2">
                                        <img src="{{ asset('images/balance_sheet.png') }}" class="avatar avatar-small mr-3 rounded shadow" alt="">
                                        <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                            <p class="text-muted mt-2">Provides a statement of your assets, liabilities and capital of your business. </p>
                                            <h6 class="text-primary">- Balance Sheet</h6>
                                        </div>
                                    </div>
                                    
                                    <div class="media customer-testi m-2">
                                        <img src="{{ asset('images/cash_flow.png') }}" class="avatar avatar-small mr-3 rounded shadow" alt="">
                                        <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                            <p class="text-muted mt-2">Reports how chances in balance sheet accounts and income affect cash and cash equivalent.</p>
                                            <h6 class="text-primary">- Cashflow Statement </h6>
                                        </div>
                                    </div>
        
                                    <div class="media customer-testi m-2">
                                        <img src="{{ asset('images/invoice.png') }}" class="avatar avatar-small mr-3 rounded shadow" alt="">
                                        <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                            <p class="text-muted mt-2">Generate invoice for your business transactions with clients</p>
                                            <h6 class="text-primary">- Invoice</h6>
                                        </div>
                                    </div>
        
                                    <div class="media customer-testi m-2">
                                        <img src="{{ asset('images/journal1.png') }}" class="avatar avatar-small mr-3 rounded shadow" alt="">
                                        <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                            <p class="text-muted mt-2">Record daily financial activities in your business.</p>
                                            <h6 class="text-primary">- Accounting Journal</h6>
                                        </div>
                                    </div>
        
                                    <div class="media customer-testi m-2">
                                        <img src="{{ asset('images/income_statement.png') }}" class="avatar avatar-small mr-3 rounded shadow" alt="">
                                        <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                            <p class="text-muted mt-2">Reports all income and expenses in your business, helps to inform of profits or losses made in your business</p>
                                            <h6 class="text-primary">- Income Statement</h6>
                                        </div>
                                    </div>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->

        <div class="container mt-100 mt-60">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="section-title">
                        <p class="text-muted para-desc mx-auto mb-0">Have you recorded financial transactions in your business <span class="text-primary font-weight-bold">Today</span>?</p>
                    
                        <div class="mt-4">
                            <a href="{{ route('pricing') }}" class="btn btn-primary mt-2 mr-2">Get Started Now</a>
                            <a href="{{ route('register') }}" class="btn btn-outline-primary mt-2">Free Trial</a>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- End -->
@endsection

@section('scripts')
     <!-- SLIDER -->
     <script src="{{ asset('land/js/owl.carousel.min.js') }}"></script>
     <script src="{{ asset('land/js/owl.init.js') }}"></script>
     <!-- Magnific Popup -->
     <script src="{{ asset('land/js/jquery.magnific-popup.min.js') }}"></script>
     <script src="{{ asset('land/js/magnific.init.js') }}"></script>
     <!-- counter -->
     <script src="{{ asset('land/js/counter.init.js') }}"></script>
@endsection