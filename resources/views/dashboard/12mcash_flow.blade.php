@extends('dashboard.template')

@section('main')
<main class="main">

  <!-- Breadcrumb -->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item"><a href="#">Admin</a></li>
    <li class="breadcrumb-item active">Cash Flow</li>
  </ol>

  <div class="container-fluid">
    <div class="card">
      <div class="card-header">
        Cash Flow Statement
        <strong>#90-98792</strong>
        <a href="#" class="btn btn-sm btn-info float-right" onclick="javascript:window.print();"><i class="fa fa-print"></i> Print</a>
      </div>
         <!--/.col-->
         <div class="float-right mt-3 mr-2">
            <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i> Download</button>
            <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
              <label class="btn btn-outline-secondary active">
                <input type="radio" name="options" id="option2" autocomplete="off" checked="">12 Month
              </label>
              <label class="btn btn-outline-secondary">
                <input type="radio" name="options" id="option3" autocomplete="off"> Year
              </label>
            </div>
          </div>
          <!--/.col-->
     
      <div class="card-body">
        <div class="row">
          <div class="col-sm-12 text-center bg-secondary">
            <strong><h4>Company Name</h4></strong>
          </div>
          <div class="col-sm-12 text-center bg-info">
            <h4 class="bold">Cash Flow Statement</h4>
        </div>
        </div>
        <!--/.row-->
          <section>
                <div class="row float-right">
                <div class="col-5-sm">For the year Ending:</div>
                <div class="col"> 12/09/2019 </div>
                </div>
                <br>
                <div class="row float-right">
                <div class="col-5-sm">Cash at Beginning of Year:</div>
                <div class="col"> N78,908 </div>
                </div>
          </section>
             
                 <table class="table table-responsive-sm">
                  <thead>
                    <tr class="bg-success">
                        <th scope="col">Operations</th>
                        <th scope="col">Jan'16</th>
                        <th scope="col">Feb'16</th>
                        <th scope="col">Mar'16</th>
                        <th scope="col">Apr'16</th>
                        <th scope="col">May'16</th>
                        <th scope="col">Jun'16</th>
                        <th scope="col">Jul'16</th>
                        <th scope="col">Aug'16</th>
                        <th scope="col">Sept'16</th>
                        <th scope="col">Oct'16</th>
                        <th scope="col">Nov'16</th>
                        <th scope="col">Dec'16</th>
                      </tr>
                      <tr class="table-success">
                          <th scope="col">Cash recipts from</th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                  </thead>
                  <tbody>
                      <!-- @foreach ($entityLedger->where('accttype','=','Assets') as $item) -->
                      <tr>
                          <td>{{$item->name}}sdsd</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                          <td>{{$item->total_balance}}sds</td>
                        </tr>
                      <!-- @endforeach -->
                      <tr>
                        <th class="pl-5">Net Cash Flow From Operations</th>
                        <td class="bg-light">&#8358;{!!$entityLedger->where('accttype','=','Assets')->sum('total_balance' )!!}</td>
                      </tr>
                  </tbody>
                </table>


                    <table class="table table-responsive-sm">
                  <thead>
                    <tr class="bg-success">
                        <th scope="col">Investing Activities</th>
                        <th scope="col">{{date('Y')}}</th>
                      </tr>
                      <tr class="table-success">
                          <th scope="col">Cash recipts from</th>
                          <th scope="col"></th>
                        </tr>
                  </thead>
                  <tbody>
                      <!-- @foreach ($entityLedger->where('accttype','=','Assets') as $item) -->
                      <tr>
                          <td>{{$item->name}}sdsd</td>
                          <td>{{$item->total_balance}}sds</td>
                        </tr>
                      <!-- @endforeach -->
                      <tr>
                        <th class="pl-5">Net Cash Flow from Investing Activities</th>
                        <td class="bg-light">&#8358;{!!$entityLedger->where('accttype','=','Assets')->sum('total_balance' )!!}</td>
                      </tr>
                  </tbody>
                </table>

                    <table class="table table-responsive-sm">
                  <thead>
                    <tr class="bg-success">
                        <th scope="col">Financing Activities</th>
                        <th scope="col">{{date('Y')}}</th>
                      </tr>
                      <tr class="table-success">
                          <th scope="col">Cash recipts from</th>
                          <th scope="col"></th>
                        </tr>
                  </thead>
                  <tbody>
                      <!-- @foreach ($entityLedger->where('accttype','=','Assets') as $item) -->
                      <tr>
                          <td>{{$item->name}}sdsd</td>
                          <td>{{$item->total_balance}}sds</td>
                        </tr>
                      <!-- @endforeach -->
                      <tr>
                        <th class="pl-5">Net Cash Flow from Financing Activites</th>
                        <td class="bg-light">&#8358;{!!$entityLedger->where('accttype','=','Assets')->sum('total_balance' )!!}</td>
                      </tr>

                       <tr class="bg-light">
                        <th>Net Increase in Cash</th>
                        <td >&#8358;{!!$entityLedger->where('accttype','=','Assets')->sum('total_balance' )!!}</td>
                      </tr>

                       <tr>
                        <th class="pl-5">Cash at End of Year</th>
                        <td class="bg-light">&#8358;{!!$entityLedger->where('accttype','=','Assets')->sum('total_balance' )!!}</td>
                      </tr>
                  </tbody>
                </table>
            
    

        </div>
        <!-- cardbody -->

      </div>
    </div>
  <!-- /.conainer-fluid -->
</main>
@endsection
