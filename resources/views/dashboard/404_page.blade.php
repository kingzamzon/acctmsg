<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Account Management System">
  <meta name="author" content="StartUpClerk">
  <meta name="keyword" content="">
  <link rel="shortcut icon" href="{{ asset('img/logoi.png') }}">

  <title>StartUpClerk | 404</title>

  <link href="{{ asset('vendors/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/css/simple-line-icons.min.css') }}" rel="stylesheet">

  <!-- Main styles for this application -->
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">


</head>

<body class="app flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="clearfix">
          <h1 class="float-left display-3 mr-4">404</h1>
          <h4 class="pt-3"><a href="{{ route('home') }}">StartUpClerk</a></h4>
          <p class="text-muted">
            Oops! You're lost. <br>
            The page you are looking for was not found.</p>
        </div>
          
      </div>
    </div>
  </div>



</body>
</html>