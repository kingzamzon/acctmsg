@extends('dashboard.template')


@section('page-title')
  {{$account->account_name}}
@endsection

@section('view-style')
  <link href="{{ asset('vendors/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
  <style>
    .no-display{
      display: none;
    }
    @media only screen and (max-width: 600px) {
      .no-display {
        display: inline;
      }
    }
  </style>
@endsection

@section('main')
<main class="main">

  <!-- Breadcrumb -->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item"><a href="{{ route('chartofaccounts.index', ['businessCode' => $businessInfo->business_code]) }}">Chart of Accounts</a></li>
    <li class="breadcrumb-item active">{{$account->account_name}}</li>

  </ol>

  <div class="container-fluid">
    <div class="card">
      <div class="card-header">
        {{$account->account_name}}
        <strong>#{{$transactions->count()}}</strong>
        <a href="#" class="btn btn-sm btn-info float-right" onclick="javascript:window.print();"><i class="fa fa-print"></i> Print</a>
      </div>
      <div class="card-body">
        <div class="mb-3">
          <div><b>{{$account->account_name}}</b></div>
          <div> <b>Code:</b> {{$account->code}} </div>
          <div> <b>Type:</b> {{$account->accounttype->name}} </div>
        </div>
        <table class="table table-striped table-bordered dt-responsive nowrap datatable " style="width:100%;border-collapse: collapse !important">
          <thead>
            <tr>
              <th>#</th>
              <th>Action</th>
              <th>Amount</th>
              <th>Description</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
              @if($transactions->count() > 0)
                @foreach($transactions as $transaction)
                  <tr>
                    <td> <b> <i class="icon-list no-display"></i> {{$loop->iteration}} </b> </td>
                    <td> @if($transaction->entity_type == 'C'){{ 'Credit' }} @else {{ 'Debit' }} @endif</td>
                    <td>{{$transaction->amount}}</td>
                    <td>{{$transaction->description}}</td>
                    <td>{{date('d-F-Y', strtotime($transaction->created_at))}}</td>
                  </tr>
              @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- /.conainer-fluid -->
</main>

@endsection

@section('view-scripts')
  <script src="{{ asset('vendors/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendors/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('js/views/datatables.js') }}"></script>
  <script src="{{ asset('js/views/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('js/views/responsive.bootstrap4.min.js') }}"></script>
@endsection