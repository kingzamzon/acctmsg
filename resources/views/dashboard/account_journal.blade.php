@extends('dashboard.template')

@section('page-title')
  Accounting Journal
@endsection

@section('main')
<main class="main">

  <!-- Breadcrumb -->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">Account Journal</li>
    @if(Session::get('data'))
      <li class="breadcrumb-item"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#upgradeModal"> Upgrade</button></li>
    @endif
  </ol>

  <div class="container-fluid">
    <div class="card">
      <div class="card-header">
        Account Journal
        <strong>#
        {{$journals->count()}}
        </strong>
        <a href="#" class="btn btn-sm btn-info float-right" onclick="javascript:window.print();"><i class="fa fa-print"></i> Print</a>
        <a href="#" class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="#addModal">
            <i class="fa fa-plus"></i> Add</a>
      </div>
      <div class="card-body">
          <strong><h4 class="text-center">Account Journal</h4></strong>
          <div class="row mb-4">
            <div class="col-sm-4">
              <div><h4>{{title_case($businessInfo->name)}}</h4></div>
            </div>
            <!--/.col-->
          </div>
          <!--/.row-->

        <div class="table-responsive-sm">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Account Code</th>
                <th>Account</th>
                <th>Description</th>
                <th>Debit (&#8358;) </th>
                <th>Credit (&#8358;) </th>
              </tr>
            </thead>
            <tbody>
              @if($journals->count() > 0)
                @foreach($journals as $journal)
              <tr>
                <td>{{$journal->id}}</td>
                <td>{{$journal->account_name}}</td>
                <td>{{$journal->description}}</td>
                <td>@if($journal->entity_type == 'D') {{$journal->amount}} @endif</td>
                <td>@if($journal->entity_type == 'C') {{$journal->amount}} @endif</td>
              </tr>
              @endforeach
            @endif
            </tbody>
          </table>
        </div>
        <!--/.row-->
      </div>
    </div>

    @include('dashboard.modal')
  </div>
  <!-- /.conainer-fluid -->
</main>
@endsection
