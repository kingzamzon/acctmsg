@extends('dashboard.template')

@section('page-title')
  Business List
@endsection

@section('main')
<main class="main">

  <!-- Breadcrumb -->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item"><a href="#">Admin</a></li>
    <li class="breadcrumb-item active">Business</li>
  </ol>

  <div class="container-fluid">
    <div class="card">
      <div class="card-header">
        Business
        
        <strong>#{{$businesses->count()}}</strong>
        {{-- If user has ever subscribe check if subscription has expired else display use for free button --}}
        @if($subscribes->count() > 0)
              @if ($daysused > 30)
              <a href="#" class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="#subscribeModal">
                <i class="fa fa-credit-card-alt"></i> Get Started </a> 
                <span class="float-right mr-3 text-danger font-weight-bold">Subscription Expired</span>  
              @else
                <a href="#" class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="#addModal">
                  <i class="fa fa-plus"></i> Add</a>
              @endif
        @else 
        <a href="#" class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="#subscribeModal">
            <i class="fa fa-credit-card-alt"></i> Get Started </a>
        @endif
      </div>

      <div class="card-body">
          <strong><h4 class="text-center">Business</h4></strong>
          {{-- If subscription has expired display this text --}}
          @if ($daysused > 30)
          <p class="text-center bg-danger"> Your subscription has expired, you will only have access to 
            the Invoice feature until you 
            renew your subscription. Click on the Get Started  button to renew. </p>
          @endif

          <!--/.row-->
          @if($businesses->count() > 0)
            <div class="table-responsive-sm">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Business Name</th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($businesses as $business)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td><img src="img/avatars/{{$business->company_logo}}" width="35px"
                      class="img-avatar" alt="{{$business->name}}"></td>
                    <td>{{$business->name}}</td>
                    <td>
                      <a href="{{$business->business_code}}/dashboard" class="btn btn-success"> Start Bookkeeping</a>
                    </td>
                    <td class="col-sm-push-9">
                      @if($business->user_id == auth()->user()->id)
                      <button class="edit btn btn-dark text-white" onclick="showEditModal('{{ $business->id }}')">
                        <i class="icon-pencil" ></i> Edit</a>
                      </button>
                      @endif
                    </td>                
                    <td>
                        @if($business->user_id == auth()->user()->id)
                      <form method="POST" 
                        action="{{ action('BusinessController@destroy', ['id' => $business->id])
                        }}" onsubmit="return confirm('Do you really want to delete Business?');">
                            @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="submit">
                            <i class="icon-trash"></i>Delete</button>
                      </form>
                      @endif
                    </td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
            </div>
          @else
              <h4>New Here?</h4>
             @if($subscribes->count() > 0)
                <p>
                  <h5>Get Started By Adding Your Business. </h5>
                  @include('dashboard.forms.add_business')
                </p>
              @else 
              <p>
                <h5>Get Started By Clicking the Get Started  button</h5>
                <a href="#" class="btn btn-sm btn-info" data-toggle="modal" data-target="#subscribeModal">
                  <i class="fa fa-credit-card-alt"></i> Get Started </a>
              </p>
              @endif
         
          @endif
        <!--/.row-->
      </div>
    </div>
  
    @if($subscribes->count() > 0)
      @include('dashboard.modal')
    @endif
      <!--edit  Modal -->
    @if($businesses->count() > 0)
      @include('dashboard.modals.business_edit')
    @endif
    <script type="text/javascript">
        $(document).ready( function() {
            $.getJSON( "{{asset('static/industries.json')}}", function( data ) {
            // $.getJSON( "https://raw.githubusercontent.com/dariusk/corpora/master/data/corporations/industries.json", function( data ) {
            }).done(function(data) {
                // console.log(data.industries);
                $.each(data.industries, function( index, value ) {
                $('#category').append(`<option value="${value}">${value}</option>`)
                })
                $.each(data.countries, function( index, value ) {
                $('#country').append(`<option value="${value.name}">${value.name}</option>`)
                })
                $.each(data.states, function( index, value ) {
                $('#state').append(`<option value="${value}">${value}</option>`)
                })
            })
        })
    </script>
  </div>
  <!-- /.conainer-fluid -->
</main>
 <!--subcscription  Modal -->
  @include('dashboard.modals.subscribe')



@endsection

