@extends('dashboard.template')

@section('page-title')
  Chart of Accounts
@endsection

@section('main')
<main class="main">

  <!-- Breadcrumb -->
  <ol class="breadcrumb no-print">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">Chart of Accounts</li>
    @if(Session::get('data'))
      <li class="breadcrumb-item"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#upgradeModal"> Upgrade</button></li>
    @endif
  </ol>

  <div class="container-fluid">
    <div class="card">
      <div class="card-header no-print">
        Chart of Accounts
        <strong>#{{$chartofaccounts->count()}}</strong>
        <a href="#" class="btn btn-sm btn-info float-right" onclick="javascript:window.print();"><i class="fa fa-print"></i> Print</a>
        <a href="#" class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="#addModal">
            <i class="fa fa-plus"></i> Add</a>
      </div>
      <div class="card-body">
          <strong><h4 class="text-center">Chart of Accounts</h4></strong>
          <div class="row mb-4">
            <div class="col-sm-4">
              <div>{{$businessInfo->name}}</div>
              <div>Email: {{$businessInfo->email}}</div>
              <div>Phone: {{$businessInfo->phone}}</div>
            </div>
            <!--/.col-->
          </div>
          <!--/.row-->
        <div class="table-responsive-sm">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Actions</th>
                <th>Account Code</th>
                <th>Account Name</th>
                <th>Type</th>
                <th>Entity Type</th>
              </tr>
            </thead>
            <tbody>
              @if($chartofaccounts->count() > 0)
                @foreach($chartofaccounts as $account)
                <tr class="clickable" data-href="" style="cursor: pointer;">
                  <td>
                    <a class="btn btn-success" href="{{ route('chartofaccounts.show', ['businessCode' => $businessInfo->business_code, 'account'=> $account->id]) }}">
                        <i class="fa fa-search-plus "></i>
                      </a>

                      @if($account->user_id == Auth::user()->id)
                      <form method="POST" action="chartofaccounts/{{$account->id}}" style="display: inline-block;">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <button class="btn btn-danger">
                              <i class="fa fa-trash-o "></i>
                            </button>
                      </form>
                      @endif
                  </td>
                  <td> {{$account->code}}</td>
                    <td>{{$account->account_name}}</td>
                    <td>{{$account->accounttype->name}}</td>
                    <td>{{$account->entity_type}}</td>
                  </tr>
              @endforeach
            @endif
            </tbody>
          </table>
        </div>
        <!--/.row-->
      </div>
    </div>

    @include('dashboard.modal')


  </div>
  <!-- /.conainer-fluid -->
</main>
<script>
  $(function() {
    $('.clickable').on('click', function(){
      href = $(this).data('href');
      window.location.replace(href);
    })
  })
</script>
@endsection

