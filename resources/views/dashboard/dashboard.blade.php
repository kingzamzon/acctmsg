@extends('dashboard.template')

@section('page-title')
  Dashboard
@endsection

@section('main')
<main class="main">

  <!-- Breadcrumb -->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">Dashboard</li>
    @if(Session::get('data'))
      <li class="breadcrumb-item"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#upgradeModal"> Upgrade</button></li>
    @endif
  </ol>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-5">
                <h3 class="card-title clearfix mb-0">Dashboard</h3>
              </div>
              <div class="col-sm-7 mb-3">
                {{-- <button type="button" class="btn btn-outline-primary float-right ml-3"><i class="icon-cloud-download"></i> &nbsp; Download</button> --}}
                {{-- <fieldset class="form-group float-right">
                  <div class="input-group float-right" style="width:240px;">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input name="daterange" class="form-control date-picker" type="text">
                  </div>
                </fieldset> --}}
              </div>
            </div>
            <hr class="m-0">
            <div class="row">
              <div class="col-sm-12 col-lg-4">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="callout callout-info">
                      <small class="text-muted">Customers</small>
                      <br>
                      <strong class="h4">
                        {{$customers->count()}}
                      </strong>
                      <div class="chart-wrapper">
                        <canvas id="sparkline-chart-1" width="100" height="30"></canvas>
                      </div>
                    </div>
                  </div>
                  <!--/.col-->
                  <div class="col-sm-6">
                    <div class="callout callout-danger">
                      <small class="text-muted">Invoices</small>
                      <br>
                      <strong class="h4">{{$invoices->count()}}</strong>
                      <div class="chart-wrapper">
                        <canvas id="sparkline-chart-2" width="100" height="30"></canvas>
                      </div>
                    </div>
                  </div>
                  <!--/.col-->
                </div>
                <!--/.row-->
                <hr class="mt-0">
                <ul class="horizontal-bars">
                  
                </ul>
              </div>
              <!--/.col-->
              <div class="col-sm-6 col-lg-4">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="callout callout-warning">
                      <small class="text-muted">Items</small>
                      <br>
                      <strong class="h4">{{$items->count()}}</strong>
                      <div class="chart-wrapper">
                        <canvas id="sparkline-chart-3" width="100" height="30"></canvas>
                      </div>
                    </div>
                  </div>
                  <!--/.col-->
                  <div class="col-sm-6">
                    <div class="callout callout-success">
                      <small class="text-muted">Transactions</small>
                      <br>
                      <strong class="h4">{{$transactions->count()}}</strong>
                      <div class="chart-wrapper">
                        <canvas id="sparkline-chart-4" width="100" height="30"></canvas>
                      </div>
                    </div>
                  </div>
                  <!--/.col-->
                </div>
                <!--/.row-->
                <hr class="mt-0">
                <ul class="horizontal-bars type-2">
                  
                </ul>
              </div>
              <!--/.col-->
              <div class="col-sm-6 col-lg-4">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="callout">
                      <small class="text-muted">Cash</small>
                      <br>
                      @if($last_revenue > 0)
                          <strong class="h4">&#8358; {{$last_cash}}</strong>
                      @else
                        <strong class="h4">&#8358; 0 </strong>
                      @endif
                      <div class="chart-wrapper">
                        <canvas id="sparkline-chart-5" width="100" height="30"></canvas>
                      </div>
                    </div>
                  </div>
                  <!--/.col-->
                  <div class="col-sm-6">
                    <div class="callout callout-primary">
                      <small class="text-muted">Revenue</small>
                      <br>
                      @if($last_revenue > 0)
                          <strong class="h4">&#8358; {{$last_revenue}}</strong>
                      @else
                        <strong class="h4">&#8358; 0 </strong>
                      @endif
                      <div class="chart-wrapper">
                        <canvas id="sparkline-chart-6" width="100" height="30"></canvas>
                      </div>
                    </div>
                  </div>
                  <!--/.col-->
                </div>
                <!--/.row-->
                <hr class="mt-0">
                <ul class="icons-list">
                 
                </ul>
              </div>
              <!--/.col-->
            </div>
            <!--/.row-->
            <br>
          </div>
        </div>
      </div>
      <!--/.col-->
    </div>
  </div>
  <!-- /.conainer-fluid -->
</main>
@endsection
