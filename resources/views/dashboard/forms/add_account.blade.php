<form method="POST" action="{{ route('chartofaccounts.store', ['businessCode' => $businessInfo->business_code]) }}">
    @csrf
    <div class="form-group row">
        <label class="col-md-3 col-form-label" for="account_name">Account Name*</label>
        <div class="col-md-9">
          <input type="text" id="account_name" name="account_name" class="form-control" placeholder="account name" value="{{ old('account_name') }}">
          <span class="help-block">e.g Sales of Bugdget</span>
        </div>
        @error('account_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>
    <div class="form-group row">
        <label class="col-md-3 col-form-label" for="type_id">Account Type*</label>
        <div class="col-md-9">
          <select class="form-control" id="type_id" name="type_id">
            @foreach($accountTypes as $accountType)
            <option value="{{$accountType->id}}">{{$accountType->name}}</option>
            @endforeach
        </select>
        </div>
      </div>
    <div class="form-group row">
      <label class="col-md-3 col-form-label" for="entity_type">Entity Type*</label>
      <div class="col-md-9">
        <select class="form-control" id="entity_type" name="entity_type">
          <option value="Debit">Debit</option>
          <option value="Credit">Credit</option>
      </select>
      </div>
    </div>
    <div class="form-group row">
        <label class="col-md-3 col-form-label" for="code">Account Code*</label>
        <div class="col-md-9">
          <input type="text" id="code" name="code" class="form-control" placeholder="account code" value="{{ old('code') }}">
          <span class="help-block">e.g 311123</span>
        </div>
        @error('code')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>
    <div class="form-group mb-0">
        <button type="submit" class="btn btn-primary" onclick="disabled">
            Submit
        </button>
    </div>
</form>