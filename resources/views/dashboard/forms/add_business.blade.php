<form method="POST" action="{{ route('business.store') }}" enctype="multipart/form-data" id="addBusinessForm">
        @csrf
    <div class="form-group">
        <label for="name">Business Name:</label>
        <input input id="name" type="text" class="form-control 
            @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" 
            placeholder="Enter Company Name" required autofocus>
    </div>
    <div class="form-group">
      <label for="companylogo">Business Logo:</label> <br>
      <input type="file" name="company_logo" id="companylogo"> 
    </div>
    <div class="form-group">
        <label for="address">Business Address:</label>
        <input input id="address" type="text" class="form-control 
            @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" 
            placeholder="Enter Company address" required autofocus>
    </div>
    <div class="form-group">
        <label for="phone">Business Phone:</label>
        <input input id="phone" type="text" class="form-control 
            @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" 
            placeholder="Enter Company phone" required autofocus>
    </div>
    <div class="form-group">
        <label for="email">Business email:</label>
        <input input id="email" type="text" class="form-control 
            @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" 
            placeholder="Enter Company email" required autofocus>
    </div>
    <div class="form-group">
        <label for="website">Business website:</label>
        <input input id="website" type="text" class="form-control" name="website" value="{{ old('website') }}" 
            placeholder="Enter Company website" autofocus>
    </div>
    <div class="row">
            <div class="form-group col-sm-8">
              <label for="country">Country</label>
              <select class="form-control" id="country" name="country">
            </select>
            </div>
            <div class="form-group col-sm-4">
              <label for="state">State</label>
            <select class="form-control" id="state" name="state">
            </select>
            </div>
          </div>
    <div class="form-group">
        <label for="category">Category:</label>
        <select class="form-control" id="category" name="category">
            </select>
    </div>
    <div class="form-group">
        <label for="founders">Business founders:(e.g Mark, Jude)</label>
        <textarea id="founders" class="form-control 
            @error('founders') is-invalid @enderror" name="founders" cols="10" rows="3" required autofocus>
            {{ old('founders') }}
            </textarea>
    </div>
    <div class="form-group">
        <label for="members">Business Size: (e.g 2)</label>
        <input id="members" type="text" class="form-control 
        @error('members') is-invalid @enderror" name="members" value="{{ old('members') }}" required autofocus>            
    </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea id="description" class="form-control
            @error('description') is-invalid @enderror" name="description"  cols="10" rows="3" required autofocus>
            {{ old('description') }}
            </textarea>  
        </div>   
        <div class="form-group mb-0">
                <button type="submit" class="btn btn-primary" onclick="disabled">
                    {{ __('Submit') }}
                </button>
        </div>
    </form>

