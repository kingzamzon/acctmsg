<form method="POST" action="journal" id="addJournalForm">
        @csrf
        <div class="form-group">
            <label for="account">Debit Account:</label>
            <select class="form-control" id="account" name="account_debit">
                <option value="118">Cash</option>
                @foreach($daccounts as $account)
                <option value="{{$account->id}}">{{$account->account_name}}</option>
                @endforeach
            </select>
        </div>
         <div class="form-group">
            <label for="amount">Amount</label>
                <input id="amount" type="text" class="form-control
                 @error('amount') is-invalid @enderror" name="amount" value="{{ old('amount') }}" required autofocus>
                @error('amount')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
        </div>
        <div class="form-group">
            <label for="account">Credit Account:</label>
            <select class="form-control" id="account" name="account_credit">
                @foreach($caccounts as $account)
                <option value="{{$account->id}}">{{$account->account_name}}</option>
                @endforeach
                <option value="118">Cash</option>
            </select>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
                <input id="description" type="text" class="form-control
                 @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autofocus>
                @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
        </div>



        <div class="form-group mb-0">
                <button type="submit" class="btn btn-primary" onclick="disabled">
                    {{ __('Submit') }}
                </button>
        </div>
    </form>

