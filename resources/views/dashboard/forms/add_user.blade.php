<form method="POST" action="users">
    @csrf
<fieldset class="form-group">
    <label>Name</label>
    <div class="input-group">
      <span class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-user-circle"></i></span>
      </span>
      <input type="text" class="form-control"  name="name">
    </div>
  </fieldset>  
<fieldset class="form-group">
    <label>Email</label>
    <div class="input-group">
      <span class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
      </span>
      <input type="text" class="form-control"  name="email">
    </div>
  </fieldset>  
<fieldset class="form-group">
    <label>Password</label>
    <div class="input-group">
      <span class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-asterisk"></i></span>
      </span>
      <input type="password" class="form-control" name="password">
    </div>
  </fieldset>  
<fieldset class="form-group">
    <label>Confirm Password</label>
    <div class="input-group">
      <span class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-asterisk"></i></span>
      </span>
      <input type="password" class="form-control" name="password_confirmation">
    </div>
  </fieldset>  
<fieldset class="form-group">
    <label>Role</label>
    <div class="input-group">
      <span class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-sitemap"></i></span>
      </span>
      <select id="select1"  class="form-control" name="role">
          <option value="0">Please select</option>
          <option value="1">Admin</option>
          <option value="2">Staff</option>
        </select>
    </div>
  </fieldset>  
  <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
  <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
</form>