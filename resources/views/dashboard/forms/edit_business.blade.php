<form method="POST" action="{{ route('business.update', ['business' => $business->id]) }}" enctype="multipart/form-data" id="addBusinessForm">
    @csrf
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" id="e_id" name="id">
    <div class="form-group">
        <label for="name">Business Name:</label>
        <input id="bname" type="text" class="form-control 
            @error('name') is-invalid @enderror" name="name" 
            placeholder="Enter Company Name" required autofocus>
    </div>
    <div class="form-group">
      <label for="companylogo">Business Logo:</label> <br>
      <input type="file" name="company_logo" id="companylogo"> 
    </div>
    <div class="form-group">
        <label for="address">Business Address:</label>
        <input id="baddress" type="text" class="form-control 
            @error('address') is-invalid @enderror" name="address" 
            placeholder="Enter Company address" required autofocus>
    </div>
    <div class="form-group">
        <label for="phone">Business Phone:</label>
        <input id="bphone" type="text" class="form-control 
            @error('phone') is-invalid @enderror" name="phone" 
            placeholder="Enter Company phone" required autofocus>
    </div>
    <div class="form-group">
        <label for="email">Business email:</label>
        <input id="bemail" type="text" class="form-control 
            @error('email') is-invalid @enderror" name="email" 
            placeholder="Enter Company email" required autofocus>
    </div>
    <div class="form-group">
        <label for="website">Business website:</label>
        <input id="bwebsite" type="text" class="form-control" name="website" 
            placeholder="Enter Company website" autofocus>
    </div>
 
    <div class="form-group mb-0">
        <button type="submit" class="btn btn-primary" onclick="disabled">
            {{ __('Update') }}
        </button>
    </div>
    </form>

