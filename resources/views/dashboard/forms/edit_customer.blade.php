<form method="POST" action="{{ route('customers.update', ['businessCode' => $businessInfo->business_code, 'customer' => $customer->id]) }}">
    @csrf
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" id="e_id" name="id">
<div class="form-group">
  <label for="business_name">Business Name</label>
  <input type="text" class="form-control" name="business_name" id="e_bname" placeholder="Enter Business Name">
</div>
<div class="row">
    <div class="form-group col-sm-6">
      <label for="fname">First Name</label>
      <input type="text" class="form-control" name="fname" id="e_fname" placeholder="First Name">
    </div>
    <div class="form-group col-sm-6">
      <label for="lname">Last Name</label>
      <input type="text" class="form-control" name="lname" id="e_lname" placeholder="Last Name">
    </div>
  </div>
<div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control" name="email" id="e_email" placeholder="Enter Email">
  </div>
<div class="form-group">
  <label for="customer_address">Address</label>
  <textarea id="e_address" name="customer_address" rows="3" 
  class="form-control" placeholder="Content.."></textarea>
</div>

<div class="row">
  <div class="form-group col-sm-6">
    <label for="customer_city">City</label>
    <input type="text" class="form-control" name="customer_city" id="e_city"  placeholder="Enter your city">
  </div>
  <div class="form-group col-sm-6">
    <label for="customer_tel">Tel</label>
    <input type="text" class="form-control" name="customer_tel" id="e_tel" placeholder="Enter Tel Number">
  </div>
</div>
<div class="form-group">
    <label for="customer_state">State</label>
    <input type="text" class="form-control" name="customer_state" id="e_state" placeholder="Enter State">
</div>
<div class="form-group mb-0">
    <button type="submit" class="btn btn-primary" onclick="disabled">
        Save Customer
    </button>
</div>
</form>