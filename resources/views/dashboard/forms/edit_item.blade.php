<form method="POST" action="{{ route('items.update', ['businessCode' => $businessInfo->business_code, 'item' => $item->id]) }}">
    @csrf
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" id="e_id" name="id">
<div class="form-group row">
    <label class="col-md-3 col-form-label">Type</label>
    <div class="col-md-9 col-form-label">
      <div class="form-check form-check-inline mr-1">
        <input class="form-check-input" type="radio" id="e_goods" value="Goods" name="type">
        <label class="form-check-label" for="goods">Goods</label>
      </div>
      <div class="form-check form-check-inline mr-1">
        <input class="form-check-input" type="radio" id="e_service" value="Service" name="type">
        <label class="form-check-label" for="service">Service</label>
      </div>
    </div>
  </div>

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="name">Name</label>
    <div class="col-md-9">
      <input type="text" id="e_name" name="name" class="form-control" placeholder="Name">
      <span class="help-block">Name of product / service</span>
    </div>
  </div>

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="unit">Unit</label>
    <div class="col-md-9">
      <input type="text" id="e_unit" name="unit" class="form-control" placeholder="Unit">
      <span class="help-block">e.g Box, cm, g, in, kg, m, lb</span>
    </div>
  </div>
<b>Sales Information </b><br>
<div class="form-group row">
    <label class="col-md-3 col-form-label" for="price">Selling Price</label>
    <div class="col-md-9">
        <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">&#8358;</span>
            </div>
            <input type="text" id="e_price" name="price" class="form-control" placeholder="Price">
          </div>
    </div>
  </div>

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="account">Account *</label>
    <div class="col-md-9">
      <select class="form-control" id="account" name="account">
        @foreach($accounts as $account)
          @if($account->account_name === 'Sales Revenue' || 
              $account->account_name === 'Service Revenue' || 
              $account->account_name === 'Account Receivable')
          <option value="{{$account->id}}">{{$account->account_name}}</option>
          @endif
        @endforeach
    </select>
    </div>
  </div>
<div class="form-group row">
    <label class="col-md-3 col-form-label" for="description">Description</label>
    <div class="col-md-9">
      <textarea id="e_description" name="description" rows="5" class="form-control" placeholder="Description.."></textarea>
    </div>
  </div>
  <div class="form-group mb-0">
      <button type="submit" class="btn btn-primary" onclick="disabled">
          Submit
      </button>
  </div>
</form>
