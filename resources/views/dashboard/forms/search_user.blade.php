<form method="POST" action="journal">
    @csrf
<fieldset class="form-group">
    <label>Email</label>
    <div class="input-group">
      <span class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
      </span>
      <input type="text" class="form-control" id="ssn" name="user-email">
      <span class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-search"></i> Search</span>
      </span>
    </div>
    <small class="text-muted">ex. adesanoye.samson@yahoo.com</small>
  </fieldset>
</form>
<table class="table table-responsive-sm">
    <thead>
      <tr>
        <th>Fullname</th>
        <th>Role</th>
        <th></th>
      </tr>
    </thead>
    <tbody table-data>

    </tbody>
  </table>

  <script type="text/javascript">
    $("[name='user-email']")
        .keyup(function() {
           $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              }); 
           
            $.ajax({
                url: "{{ url('users_list') }}",
                method: 'get', 
                data: {
                    email: $('[name="user-email"]').val()
                },
                success: function(data){
                  $('[table-data]').html('');
                    if(typeof(data) == 'string'){
                      $('[table-data]').append(`
                        <tr>
                          <td>${data}</td>
                        </tr>
                        `)
                    }else {
                       $('[table-data]').append(`
                      <tr>
                        <td>${data.name}</td>
                        <form method="POST" 
                            action="userbusiness/{{$businessuser->userBusinessId}}">
                              @csrf
                        <td> 
                          <select id="select1"  class="form-control" name="role">
                            <option value="0">select</option>
                              <option value="1">Admin</option>
                              <option value="2">Staff</option>
                            </select>
                          </td>
                        <td>
                              <input type="hidden" name="user_id" value="${data.id}"/>
                            <button class="btn btn-sm btn-primary" type="submit">
                              <i class="fa fa-user-plus"></i>Add</button>

                          </form>
                        </td>
                      </tr><br>
                        `);
                    }
                    
                  
                 

                   
                }
            })

        })
        .keyup();
  </script>