<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{url('business')}}"></a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
      <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav d-md-down-none mr-auto">
      <li class="nav-item px-3">
        <a class="nav-link" href="{{url('business')}}">My Businesses
        </a>
      </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
      @if(Request::route()->getName() != 'business.index')
      <li class="nav-item dropdown">
        <a class="nav-link nav-link"  href="{{route('users.show', ['businessCode' => $businessInfo->business_code,'user' => Auth::user()->id])}}">
          <img src="{{asset('img/avatars/'.Auth::user()->profile_photo) }}" class="img-avatar" alt="{{Auth::user()->name}}">
        </a>
      </li>
      @endif

      <li class="nav-item dropdown ">
        <a class="nav-link mr-3" data-toggle="dropdown" href="#" role="button" 
        aria-haspopup="true" aria-expanded="false" href="{{ route('logout') }}"
          onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
           <i class="icon-power"></i> 
           <span class="d-md-down-none">Logout</span> 
       </a>

       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
           @csrf
       </form>
   </li>
    </ul>
  </header>
