@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <p>List of Subcription plans</p>
                   
                    <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>Plan</th>
                                <th>Description</th>
                                <th></th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($plans as $plan)
                            <tr>
                                <td>{{$plan->id}}</td>
                                <td>{{$plan->plan}}</td>
                                <td>{{$plan->description}}</td>
                                <td>
                                    <a href="#" class="btn btn-primary"><i class="icon-pencil" ></i> Edit</a>
                                </td>
                                <td>
                                    <form method="POST" 
                                    action="{{ action('SubscriptionPlanController@destroy', ['id' => $plan->id])
                                 }}">
                                        @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-danger" type="submit">
                                         <i class="icon-trash"></i>Delete</button>
                                </form>
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                          </table>

                    <p>Create new Plan</p>
                    <form method="POST" action="{{ route('plan') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">
                                {{ __('Plan') }}
                            </label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control
                                 @error('plan') is-invalid @enderror" name="plan" value="{{ old('plan') }}" required autofocus>
                                @error('plan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">
                                {{ __('Description') }}
                            </label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control
                                 @error('description') is-invalid @enderror" name="description" required autofocus>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>



                    <p>Create new Business</p>
                    <form method="POST" action="{{ route('business.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">
                                {{ __('name') }}
                            </label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control
                                 @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">
                                {{ __('Description') }}
                            </label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control
                                 @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autofocus>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>

                    <p>List of Accounts</p>
                   
                    <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>name</th>
                                <th>categories</th>
                                <th></th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($accounts as $account)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$account->name}}</td>
                                <td>{{$account->categories}}</td>
                                <td>
                                    <a href="#" class="btn btn-primary"><i class="icon-pencil" ></i> Edit</a>
                                </td>
                                <td>
                                    <form method="POST" 
                                    action="{{ action('AccountsController@destroy', ['id' => $account->id])
                                 }}">
                                        @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-danger" type="submit">
                                         <i class="icon-trash"></i>Delete</button>
                                </form>
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                          </table>

                    <p>Create new Account</p>
                    <form method="POST" action="{{ route('accounts') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">
                                {{ __('name') }}
                            </label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control
                                 @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">
                                {{ __('Categories') }}
                            </label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control
                                 @error('categories') is-invalid @enderror" name="categories" value="{{ old('categories') }}" required autofocus>
                                @error('categories')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" onclick="disabled">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>

                    <p>List of Business Accounts</p>
                   
                    <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>Business Name</th>
                                <th>Account </th>
                                <th></th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($baccounts as $baccount)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$baccount->businessName}}</td>
                                <td>{{$baccount->accountName}}</td>
                                <td>
                                    <a href="#" class="btn btn-primary"><i class="icon-pencil" ></i> Edit</a>
                                </td>
                                <td>
                                    <form method="POST" 
                                    action="{{ action('BusinessAccountsController@destroy', ['id' => $baccount->id])
                                 }}">
                                        @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-danger" type="submit">
                                         <i class="icon-trash"></i>Delete</button>
                                </form>
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                          </table>


                    <p>Create new Business Account</p>
                    <form method="POST" action="{{ route('businessaccounts') }}">
                        @csrf
                        @foreach($accounts as $account)
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" 
                                value="{{$account->id}}" name="accounts[]" id="{{$account->name}}">
                            <label class="form-check-label" for="{{$account->name}}">
                                    {{$account->name}}
                            </label>
                        </div>
                        @endforeach             
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" onclick="disabled">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>



                    <p>Create new Journal</p>
                    <form method="POST" action="{{ route('journal') }}">
                        @csrf
                        <div class="form-group row">

                            @foreach($baccounts->where('business_id','==' ,1) as $baccount)
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" 
                                value="{{$baccount->id}}" name="account" id="{{$baccount->account_id}}"> 
                            <label class="form-check-label" for="{{$baccount->account_id}}">
                                    {{$baccount->accountName}}
                            </label> 
                        </div> <br> <br>
                        @endforeach  
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">
                                {{ __('Description') }}
                            </label>
                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control
                                 @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autofocus>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>   
                        
                        <div class="form-group row">
                            <label for="amount" class="col-md-4 col-form-label text-md-right">
                                {{ __('Amount') }}
                            </label>
                            <div class="col-md-6">
                                <input id="amount" type="text" class="form-control
                                 @error('amount') is-invalid @enderror" name="amount" value="{{ old('amount') }}" required autofocus>
                                @error('amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" onclick="disabled">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
