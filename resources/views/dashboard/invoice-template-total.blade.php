
<div class="col-lg-6 col-sm-5 ml-auto">
  <table class="table table-clear">
    <tbody>
      <tr>
        <td class="left">
          <strong>Subtotal</strong>
        </td>
        <td class="right" id="total_sum_value"></td>
      </tr>
      <tr>
        <td class="left form-inline">
            <div class="form-group">
                <label for="discount"><strong>Discount 
                  @if(Request::route()->getName() != 'invoice.create')
                    (<span id="invoice-discount">{{$invoice->discount}}</span>%)
                  @endif
                </strong></label>
              </div>
        </td>
        <td class="right" >
          @if(Request::route()->getName() == 'invoice.create')
          <input type="text" class="form-control ml-2 col-sm-3" id="discount" placeholder="10%" name="discount"/> 
          @else           
          <span id="discount-total"></span> 
          @endif
      </td>
      </tr>
      <tr>
        <td class="left form-inline">
          <div class="form-group"> 
            <label for="vat"><strong>VAT 
              @if(Request::route()->getName() != 'invoice.create')
                (<span id="invoice-vat">{{$invoice->vat}}</span>%)
              @endif
            </strong></label>    
          </div>
        </td>
        <td class="right" >
            @if(Request::route()->getName() == 'invoice.create')
            <input type="text" class="form-control ml-2 col-sm-3" id="vat" placeholder="10%" name="vat"  /> 
            @else 
              <span id="vat-total"></span> 
            @endif
        </td>
      </tr>
      <tr class="bg-light">
        <td class="left">
          <strong>Total</strong>
        </td>
        <td class="right">
          <strong id="real-total"></strong>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<!--/.col-->



