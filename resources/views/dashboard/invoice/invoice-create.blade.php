@extends('dashboard.invoice.invoice_template')

@section('invoice-breadcrumb')
<li class="breadcrumb-item active">New Invoice</li>
@endsection

@section('page-title')
  New Invoice
@endsection

@section('view-style')
  <style>
    @media only screen and (max-width: 600px) {
      .cost {
        width: 90px;
      }
      .item {
        width: 90px;
      }
    }
  </style>
@endsection


@section('invoice-inbox')
<main>
  <div class="toolbar">
    <div class="btn-group">
      <button type="button" class="btn btn-light">
        <b>
          <span class="icon-note"></span> Create
        </b>
      </button>
    </div>
    <div class="btn-group">
        <button type="button" class="btn btn-light" data-toggle="modal" data-target="#addModal">
          <span class="fa fa-plus"></span> New Invoice
        </button>
      </div>
  </div>
  <form id="user_form" method="post">
  <div class="form-group row">
      <label class="col-md-3 col-form-label" for="customer">Customer Name</label>
      <div class="col-md-9">
        <select class="form-control" id="customer" name="customer_id">
            @if($customers->count() > 0)
              @foreach($customers as $customer)
              <option value="{{$customer->id}}">{{$customer->first_name}} {{$customer->last_name}}</option>
              @endforeach
            @endif
        </select>
      </div>
    </div>
  {{-- <div class="form-group row">
      <label class="col-md-3 col-form-label" for="invoice_no">Invoice#</label>
      <div class="col-md-9">
        <input type="text" id="invoice_no" name="invoice_no" value="INV-00000" class="form-control" placeholder="Text">
        <span class="help-block">Autogenerate Number Enabled</span>
      </div>
  </div> --}}
    <div class="row">
      <div class="col-sm-6">
          <fieldset class="form-group">
              <label>Invoice Date</label>
              <div class="input-group">
                <span class="input-group-prepend">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </span>
                <input name="invoice_date" class="form-control" type="text" invoice-date>
              </div>
            </fieldset>
      </div>
      <div class="col-sm-6">
          <fieldset class="form-group">
              <label>Due Date</label>
              <div class="input-group">
                <span class="input-group-prepend">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </span>
                <input name="due_date" class="form-control" type="text" invoice-date>
              </div>
            </fieldset>
      </div>
    </div>

      <div class="table-responsive-sm">
        <table class="table table-bordered " >
          <thead>
            <tr>
              <th>Item</th>
              <th>Description</th>
              <th>Quantity</th>
              <th>Unit Cost</th>
              {{-- <th>Tax Rate</th> --}}
              <th class="bg-light">Amount</th>
              <th class="bg-light">
                <button type="button" class="btn btn-danger btn-sm" remove-row><i class="fa fa-trash-o"></i></button>
              </th>
            </tr>
          </thead>
          <tbody item-list>

          </tbody>
        </table>
      </div>

      <button type="button" class="btn btn-default mb-2 mt-3" @if($items->count() == 0) disabled @endif new-line>Add New Line</button>
      <div class="row">

          <div class="col-lg-4 col-sm-5">
              <textarea id="description" name="description" rows="9" 
              class="form-control" placeholder="Invoice Comment"></textarea>
          </div>
          <!--/.col-->

          @include('dashboard.invoice-template-total')

        </div>
        <!--/.row-->
        <div class="form-group mt-3 text-center">
          <button type="submit" id="send" class="btn btn-success" @if($customers->count() == 0 || $items->count() == 0) disabled @endif>
              <i class="fa fa-send-o"></i> Send</button>
          {{-- <button type="button" class="btn btn-light">
            <i class="fa fa-bookmark"></i> Save as Draft</button> --}}
          <button type="button" onclick="window.history.go(-1)" class="btn btn-danger">
            <i class="fa fa-trash-o"></i> Discard</button>
        </div>
  </form>
    <script>
     $(document).ready( function() {
      // console.log(11)
      
  $('input[invoice-date]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  })
  // $( this ).slideUp(); this remove a particuler click element
  var count = 0;
  var total_sum = 0;
  var discount_sum = 0;
   var dis = 0;
  

  $("[new-line]").click(function() {
    count = count + 1;
    $('[item-list]').append(`
      <tr id="row_${count}" class="ola">
          <td>
                <select class="form-control item" id="item${count}" name="item[]"  select-item>
                  <option value="0">Select Item</option>
                    @if($items->count() > 0)
                      @foreach($items as $item)
                      <option value="{{$item->id}}">{{$item->name}}</option>
                      @endforeach
                    @endif
                </select>
              </td>
              <td>
              <textarea id="item_description" name="item_description[]" rows="4" 
              class="form-control" placeholder="Item Description" required></textarea>
              </td>
              <td>
                  
                  <input type="text" name="quantity[]" id="quantity${count}"
                  class="form-control quantity" value="1.00">
              </td>
              <td>
                  <input type="text" name="unit_cost[]" id="unit_cost${count}"
                  class="form-control cost" value="1.00"  readonly>
              </td>
              <td class="bg-light" total id="tot"></td>
              <td>
                  <input type="checkbox" name="record">

                </td>
            </tr>
        </tr>
    `)

    // get selected item unit cost
    $("[select-item]").change(function(event){
      var sol = $(this).parents("tr");
      var selected = null;
      var selected = sol.find("[select-item]").val();
      if(selected == 0){
        sol.find(".cost").val('0.00');
        sol.find("[total]").text('0.00');
      }else {
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        })
        $.ajax({
          url: "{{ route('getPrice', ['businessCode' => $businessInfo->business_code]) }}",
          method: "POST",
          data: {
            id: selected
          },
          success: function (data) {
            sol.find( ".cost" ).val(data['data']);
            var quantity = sol.find( ".quantity" ).val();
            var unit = sol.find( ".cost" ).val();
            var total = quantity * unit;
            sol.find( "[total]" ).text( total );
            calculateSum();
          },
          error: function(err) {
            console.log(err);
          }
        }) 
        }
    })

  $( ".quantity" )
      .keyup(function(e) {
        var sol = $(this).parents("tr");
        var quantity = sol.find( ".quantity" ).val();
        var unit = sol.find( ".cost" ).val();
        var total = quantity * unit;
        sol.find( "[total]" ).text( total );
        calculateSum();
      })
 
  });
  
function calculateSum() {
  var subtotal = 0;
  $("[item-list] tr").find('td:eq(4)').each(function(){
      subtotal += parseFloat($(this).text());
  }) 
  $("#total_sum_value").html("&#8358; " + subtotal.toLocaleString() );
  $("#real-total").html("&#8358; " + subtotal.toLocaleString() );
   total_sum = subtotal;
}

$("#discount").keyup(function(e) {
      var discount_value = $(this).val();
      calculateDiscountTotal(discount_value)
  })

  function calculateDiscountTotal(discount_value) {
    if(discount_value > 0) {
        var discount = (discount_value * total_sum) / 100;
        $("#discount-total").html("&#8358; " + discount.toLocaleString() );
        total_sum = total_sum - discount;
        $("#real-total").html("&#8358; " + total_sum.toLocaleString() );
      }else {
        $("#discount-total").html("&#8358; 0.0" );
        $("#real-total").html("&#8358; " + total_sum.toLocaleString() );
      }
  }

  $("#vat").keyup(function(e) {
      var vat_value = $(this).val();
      calculateVatTotal(vat_value);
  })

  function calculateVatTotal(vat_value) {
    var vat = (vat_value * total_sum) / 100;
      if(vat_value > 0) {
        var vat = (vat_value * total_sum) / 100;
        $("#vat-total").html("&#8358; " + vat.toLocaleString() );
        if(discount_sum == 0){
            vat_sum = total_sum + vat;
          }else {
            vat_sum = discount_sum + vat;
          }
        $("#real-total").html("&#8358; " + vat_sum.toLocaleString() );
      }
  }
  

  $("[remove-row]").click(function(){
          $("table tbody").find('input[name="record"]').each(function(){
            if($(this).is(":checked")){
                  $(this).parents("tr").remove();
              }
          });
      })


  
        $('#user_form').on('submit', function (event) {
                event.preventDefault();
                var count_data = 0;

                $('.item').each(function () {
                    count_data = count_data + 1;
                });
                // console.log(23);
                if (count_data > 0) {
                    var form_data = $(this).serialize();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    })
                    $.ajax({
                        url: "{{ route('invoice.store', ['businessCode' => $businessInfo->business_code]) }}",
                        method: "POST",
                        data: form_data,
                        success: function (data) {
                            var success_message = data[0];
                            //redirect to invoice.show url
                            window.location.replace(data[1]);
                            
                            $("#user_form").trigger("reset");
                            $('#user_data').find("tr:gt(0)").remove();
                            $("#discount-total").html("");
                            $("#total_sum_value").html("");
                            $("#real-total").html("");
                            $("[total]").html("");
                            $(function(){ 
                                toastr.success(success_message, 'Success' , {
                                closeButton: true,
                                progressBar: true,
                                });
                              })
                        },
                        error: function(err) {
                          console.log(err);
                        }
                    })
                }

                else {
                    $('#action_alert').html('<p>Please Add atleast one data</p>');
                    // $('#action_alert').dialog('open');
                }
            });
      
      
      })
      </script>

  </main>
@endsection

