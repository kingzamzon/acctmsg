@extends('dashboard.invoice.invoice_template')

@section('invoice-breadcrumb')
<li class="breadcrumb-item active">Customers</li>
@endsection

@section('page-title')
  Customers
@endsection

@section('view-style')
  <link href="{{ asset('vendors/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
  <style>
    .no-display{
      display: none;
    }
    @media only screen and (max-width: 600px) {
      .no-display {
        display: inline;
      }
    }
  </style>
@endsection

@section('invoice-inbox')
<main>
  <div class="toolbar">
      <div class="btn-group">
        <button type="button" class="btn btn-light">
          <b>
            <i class="fa fa-address-book"></i> Customers
          </b>
        </button>
      </div>
      <div class="btn-group">
          <button type="button" class="btn btn-light" data-toggle="modal" data-target="#addModal">
            <span class="fa fa-plus"></span> New Customer
          </button>
        </div>
      {{-- <div class="btn-group">
          <button type="button" class="btn btn-light" data-toggle="modal" data-target="#addModal">
            <span class="fa fa-plus"></span> Import Customers
          </button>
        </div> --}}
  </div>

{{-- Entries   search for customers  --}}
<div class="messages mt-4">
    <table class="table table-striped table-bordered dt-responsive nowrap datatable " 
          style="width:100%;border-collapse: collapse !important">
            <thead>
              <tr>
                <th>Actions</th>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Tel</th>
                <th>Email</th>
                <th>Address</th>
              </tr>
            </thead>
            <tbody>
                @if($customers->count() > 0)
                @foreach($customers as $customer)
              <tr>
                <td>
                  <button class="edit btn btn-dark text-white" onclick="showEditModal('{{ $customer->id }}')">
                    <i class="icon-pencil" ></i>
                  </button>
                  <form method="POST"  action="customers/{{$customer->id}}" style="display: inline-block;">
                    @csrf 
                    <input type="hidden" name="_method" value="DELETE">
                    <button class="btn btn-danger" type="submit">
                      <i class="fa fa-trash-o"></i>
                    </button>
                  </form>
                </td>
                <td><b> <i class="icon-list no-display"></i> {{$loop->iteration}} </b></td>
                <td>{{$customer->first_name}}</td>
                <td>{{$customer->last_name}}</td>
                <td>{{$customer->tel}}</td>
                <td>{{$customer->email}}</td>
                <td>{{$customer->address}}</td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
          
        </div>
        
      </main>

      @include('dashboard.modal')

      @if($customers->count() > 0)
        @include('dashboard.modals.customer_edit')
      @endif

@endsection

@section('view-scripts')
  <script src="{{ asset('vendors/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendors/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('js/views/datatables.js') }}"></script>
  <script src="{{ asset('js/views/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('js/views/responsive.bootstrap4.min.js') }}"></script>
@endsection
