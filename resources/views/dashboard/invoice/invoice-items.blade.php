@extends('dashboard.invoice.invoice_template')

@section('invoice-breadcrumb')
<li class="breadcrumb-item active">Items</li>
@endsection

@section('page-title')
  Items
@endsection

@section('view-style')
  <link href="{{ asset('vendors/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
  <style>
    .no-display{
      display: none;
    }
    @media only screen and (max-width: 600px) {
      .no-display {
        display: inline;
      }
    }
  </style>
@endsection

@section('invoice-inbox')
<main>

    <div class="toolbar">
        <div class="btn-group">
          <button type="button" class="btn btn-light">
            <b>
              <span class="fa fa-eercast"></span> Items
            </b>
          </button>
        </div>
        <div class="btn-group">
            <button type="button" class="btn btn-light" data-toggle="modal" data-target="#addModal">
              <span class="fa fa-plus"></span> New Item
            </button>
          </div>
      </div>

      <div class="messages mt-4">
          <table class="table table-striped table-bordered dt-responsive nowrap datatable " 
                  style="width:100%;border-collapse: collapse !important">
            <thead>
              <tr>
                <th>Actions</th>
                <th>#</th>
                <th>Name</th>
                <th>Type</th>
                <th>Selling Price</th>
                <th>Account</th>
                <th>Unit</th>
              </tr>
            </thead>
            <tbody>
                @if($items->count() > 0)
                @foreach($items as $item)
              <tr>
                <td>
                  <button class="edit btn btn-dark text-white" onclick="showEditModal('{{ $item->id }}')">
                    <i class="icon-pencil" ></i>
                  </button>
                    <form method="POST"  action="items/{{$item->id}}" style="display: inline-block;">
                      @csrf 
                    <input type="hidden" name="_method" value="DELETE">
                         <button class="btn btn-danger" type="submit">
                            <i class="fa fa-trash-o"></i>
                            </button>
                      </form>
                  </td>
                <td> <b> <i class="icon-list no-display"></i> {{$loop->iteration}} </b> </td>
                <td>{{$item->name}}</td>
                <td>{{$item->type}}</td>
                <td>{{$item->price}}</td>
                <td>{{$item->account_name}}</td>
                <td>{{$item->unit}}</td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>

          
      </div>

    </main>
@include('dashboard.modal')
@if($items->count() > 0)
  @include('dashboard.modals.item_edit')
@endif
@endsection 

@section('view-scripts')
  <script src="{{ asset('vendors/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendors/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('js/views/datatables.js') }}"></script>
  <script src="{{ asset('js/views/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('js/views/responsive.bootstrap4.min.js') }}"></script>
@endsection
