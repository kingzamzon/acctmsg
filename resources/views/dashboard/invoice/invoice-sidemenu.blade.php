<nav>
    <a href="{{ route('invoice.create', ['businessCode' => $businessInfo->business_code]) }}" class="btn btn-danger btn-block">New Invoice</a>
    <ul class="nav">
    <li class="nav-item">
        <a class="nav-link" href="{{ route('invoice.index', ['businessCode' => $businessInfo->business_code]) }}"><i class="fa fa-rocket"></i> Sent </a>
    </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('items.index', ['businessCode' => $businessInfo->business_code]) }}"><i class="fa fa-eercast"></i> Items </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fa fa-bookmark"></i> Draft </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('customers.index', ['businessCode' => $businessInfo->business_code]) }}"><i class="fa fa-address-book"></i> Customers </a>
      </li>
    </ul>
  </nav>