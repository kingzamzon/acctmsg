@extends('dashboard.invoice.invoice_template')

@section('invoice-breadcrumb')
<li class="breadcrumb-item active">#{{$invoice->invoice_no}}</li>
@endsection

@section('page-title')
  #{{$invoice->invoice_no}}
@endsection

@section('invoice-inbox')
<main>
  @if($invoice->count() > 0)
    <div class="card-body">
        <div class="row mb-4">

          <div class="col-sm-4">
            <h6 >From:</h6>
            <div>
              <strong>{{$businessInfo->name}}</strong>
            </div>
            <div>{{$businessInfo->address}}</div>
            <div>Email: {{$businessInfo->email}}</div>
            <div>Phone: {{$businessInfo->phone}}</div>
          </div>
          <!--/.col-->

          <div class="col-sm-4">
            <h6 >To:</h6>
            <div>
              <strong>{{$invoice->customer->business_name}}</strong>
            </div>
            <div>{{$invoice->customer->address}}</div>
            <div>Email: {{$invoice->customer->email}}</div>
            <div>Phone: {{$invoice->customer->tel}}</div>
          </div>
          <!--/.col-->

          <div class="col-sm-4">
            <h6 >Details:</h6>
            <div>
              <strong>#{{$invoice->invoice_no}}</strong>
            </div>
            <div>{{$invoice->invoice_date}}</div>
            <div>VAT: </div>
          </div>
          <!--/.col-->

        </div>
        <!--/.row-->

        <div class="table-responsive-sm">
          <table class="table bg-light">
            <thead>
              <tr>
                <th>#</th>
                <th>Item</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Unit Cost</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody item-list>
              @foreach($invoice->invoicetransaction as $transaction)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$transaction->item['name']}}</td>
                  <td>{{$transaction->description}}</td>
                  <td class="quantity">{{$transaction->quantity}}</td>
                  <td class="cost">{{$transaction->unit_cost}}</td>
                  <td class="bg-light" total id="tot">$999,00</td>
                </tr>
              @endforeach 
            </tbody>
          </table>
        </div>

        <div class="row">

          <div class="col-lg-4 col-sm-5">
              {{$invoice->description}}
          </div>
          <!--/.col-->

          @include('dashboard.invoice-template-total')

        </div>
        <!--/.row-->
        <div class="form-group mt-3 text-center">
          <button type="button" class="btn btn-light" onclick="javascript:window.print();">
            <i class="fa fa-print"></i> Print</button>
            <a href="{{ route('download-pdf', ['businessCode' => $businessInfo->business_code, 'id' => $invoice->id]) }}" class="btn btn-primary">
              <i class="icon-cloud-download"></i> Download
            </a>
        </div>
      </div>
<script>
  $(document).ready( function() {
  var total_sum = 0;
  var discount_sum = 0;
  var vat_sum = 0;

    $( ".cost, .quantity" ).each(function(e) {
        var sol = $(this).parents("tr");
        var quantity = sol.find( ".quantity" ).text();
        var unit = sol.find( ".cost" ).text();
        var total = quantity * unit;
        sol.find( "[total]" ).text( total);
        
      })

    function calculateSum() {
        var subtotal = 0;
        $("[item-list] tr").find('td:eq(5)').each(function(){
            subtotal += parseFloat($(this).text());
        }) 
        $("#total_sum_value").html("&#8358; " + subtotal.toLocaleString() );
        total_sum = subtotal;
      }

      calculateSum();

    function calculateDiscount() {
      var discount_value = $("#invoice-discount").text();
      if(discount_value > 0) {
        var discount = (discount_value * total_sum) / 100;
        $("#discount-total").html("&#8358; " + discount.toLocaleString() );
        total_sum = total_sum - discount;
        $("#real-total").html("&#8358; " + total_sum.toLocaleString() );
      }else {
        $("#discount-total").html("&#8358; 0.0" );
        $("#real-total").html("&#8358; " + total_sum.toLocaleString() );
      }      
    }
    calculateDiscount();

    function calculateVat() {
      var vat_value = $("#invoice-vat").text();
      if(vat_value > 0) {
        var vat = (vat_value * total_sum) / 100;
        $("#vat-total").html("&#8358; " + vat.toLocaleString() );
            vat_sum = total_sum + vat;
        $("#real-total").html("&#8358; " + vat_sum.toLocaleString() );
      }else {
        $("#vat-total").html("&#8358; 0.0" );
        $("#real-total").html("&#8358; " + total_sum.toLocaleString() );
      }      
    }
    calculateVat();


  })
</script>
    
  @endif
  </main>
@endsection

