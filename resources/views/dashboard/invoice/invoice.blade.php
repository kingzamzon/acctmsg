@extends('dashboard.invoice.invoice_template')

@section('page-title')
  Sent
@endsection

@section('invoice-inbox')
<main class="inbox">
  <div class="toolbar">
    <div class="btn-group">
      <button type="button" class="btn btn-light">
        <b>
          <span class="fa fa-rocket"></span> Sent
        </b>
      </button>
    </div>
 
    <div class="btn-group float-right">
      <button type="button" class="btn btn-light">
        <span class="fa fa-chevron-left"></span>
      </button>
      <button type="button" class="btn btn-light">
        <span class="fa fa-chevron-right"></span>
      </button>
    </div>
  </div>

  <ul class="messages">
    @if($invoices->count() > 0)
      @foreach($invoices as $invoice)
        @if($invoice->customer)
          <a  style="color:#000;">
            <li class="message unread">
                {{-- <div class="actions">
                  <span class="action"><i class="fa fa-square-o"></i></span>
                  <span class="action"><i class="fa fa-star-o"></i></span>
                </div> --}}
                <div class="header">
                <span class="from">{{$invoice->customer->first_name}} {{$invoice->customer->last_name}} </span>
                  <span class="date">
                  <span class="fa fa-paper-clip"></span> {{$invoice->created_at}} 
                </div>
                <div class="title">
                    {{$invoice->customer->business_name}}
                </div>
                <div class="description">
                    {{$invoice->description}}
                </div>
                <div class="description" id="actions">
                  <a class="btn btn-success text-white" href="invoice/{{$invoice->id}}">
                    <i class="fa fa-search-plus "></i>
                  </a>
              </div>
              </li>
            </a>
        @endif
        
      @endforeach
    @else 
      <li class="message unread">
          No Sent Invoice
      </li>
    @endif
  </ul>
</main>
@endsection 

