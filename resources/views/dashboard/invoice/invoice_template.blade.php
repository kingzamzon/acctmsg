@extends('dashboard.template')

@section('main')
<main class="main">

  <!-- Breadcrumb -->
  <ol class="breadcrumb no-print">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item"><a href="#">Admin</a></li>
    <li class="breadcrumb-item active">Invoice</li>
    @yield('invoice-breadcrumb')
  </ol>

  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="email-app mb-4">
        {{-- @include('dashboard.invoice-sidemenu') --}}

        @yield('invoice-inbox')
        
      </div>
    </div>

  </div>
  <!-- /.conainer-fluid -->
  
</main>
@endsection
