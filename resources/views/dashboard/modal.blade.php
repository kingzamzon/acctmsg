  <!--Add Modal -->
  <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="addModalLabel">
                @if(Request::route()->getName() == 'journal.index')
                  New Journal
                @elseif(Request::route()->getName() == 'users.index')
                  New User
                @elseif(Request::route()->getName() == 'business.index')
                  New Business
                @elseif(Request::route()->getName() == 'items.index')
                  New Item
                @elseif(Request::route()->getName() == 'customers.index')
                  New Customer
                @elseif(Request::route()->getName() == 'chartofaccounts.index')
                  New Chart Of Account
                @endif

              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              @if(Request::route()->getName() == 'journal.index')
                @include('dashboard.forms.add_journal')
              @elseif(Request::route()->getName() == 'users.index')
                @include('dashboard.forms.add_user')
              @elseif(Request::route()->getName() == 'business.index')
                @include('dashboard.forms.add_business')
              @elseif(Request::route()->getName() == 'items.index')
                @include('dashboard.forms.add_item')
              @elseif(Request::route()->getName() == 'customers.index')
                @include('dashboard.forms.add_customer')
              @elseif(Request::route()->getName() == 'chartofaccounts.index')
                @include('dashboard.forms.add_account')
              @endif
            </div>
          </div>
        </div>
      </div>

@if (count($errors) > 0)
      <script>
          $( document ).ready(function() {
              $('#addModal').modal('show');
          });
      </script>
  @endif
  

  


 

