  <!--Edit Modal -->
  <div class="modal fade" id="editBusinessModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addModalLabel">
              Edit Business
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            @include('dashboard.forms.edit_business')
        </div>
      </div>
    </div>
  </div>
@if (count($errors) > 0)
      <script>
          $( document ).ready(function() {
              $('#editBusinessModal').modal('show');
          });
      </script>
@endif

<script>
    function showEditModal(business_id){
        var url = '{{ route("business.show", ["business"=> ":business"] ) }}';
        url = url.replace(':business', business_id);

        $('#editBusinessModal').modal('show');

        $.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}	
			});
			$.ajax({
				url: url,
				method: "GET",
				success: function (data) {
          $('#editmodal').modal('show');
          $('#eid').val(data.id);
          $('#bname').val(data.name);
          $('#bphone').val(data.phone);
          $('#baddress').val(data.address);
          $('#bemail').val(data.email);
          $('#bwebsite').val(data.website);
				},
				error: function(err) {
					console.log(err);
				}
			})
    };
</script> 