  <!--Edit Modal -->
  <div class="modal fade" id="editCustomerModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addModalLabel">
              Edit Customer
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            @include('dashboard.forms.edit_customer')
        </div>
      </div>
    </div>
  </div>
@if (count($errors) > 0)
      <script>
          $( document ).ready(function() {
              $('#editCustomerModal').modal('show');
          });
      </script>
@endif

<script>
    function showEditModal(customer_id){
        var url = '{{ route("customers.show", ["businessCode" => $businessInfo->business_code, "customer"=> ":customer"] ) }}';
        url = url.replace(':customer', customer_id);

        $('#editCustomerModal').modal('show');

        $.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}	
			});
			$.ajax({
				url: url,
				method: "GET",
				success: function (data) {
                    $('#e_bname').val(data.business_name);
                    $('#e_fname').val(data.first_name);
                    $('#e_lname').val(data.last_name);
                    $('#e_tel').val(data.tel);
                    $('#e_city').val(data.city);
                    $('#e_state').val(data.state);
                    $('#e_email').val(data.email);
                    $('#e_address').val(data.address);
                    $('#e_id').val(data.id);
				},
				error: function(err) {
					console.log(err);
				}
			})
    };
</script> 