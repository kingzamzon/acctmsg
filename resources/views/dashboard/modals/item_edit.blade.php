  <!--Edit Modal -->
  <div class="modal fade" id="editItemModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addModalLabel">
              Edit Item
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          @include('dashboard.forms.edit_item')
        </div>
      </div>
    </div>
  </div>
@if (count($errors) > 0)
      <script>
          $( document ).ready(function() {
              $('#editItemModal').modal('show');
          });
      </script>
@endif

<script>
    function showEditModal(item_id){
        var url = '{{ route("items.show", ["businessCode" => $businessInfo->business_code, "item"=> ":item"] ) }}';
        url = url.replace(':item', item_id);

        $('#editItemModal').modal('show');

        $.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}	
			});
			$.ajax({
				url: url,
				method: "GET",
				success: function (data) {
          console.log(data)
          $('#e_id').val(data.id);
          $('#e_name').val(data.name);
          $('#e_type').val(data.type);
          $('#e_price').val(data.price);
          $('#e_account_name').val(data.account_name);
          $('#e_description').val(data.description);
          $('#e_unit').val(data.unit);
          if(data.type == 'Goods'){
            document.getElementById('e_goods').checked = true;
          }else {
            document.getElementById('e_service').checked = true;
          }
				},
				error: function(err) {
					console.log(err);
				}
			})
    };
</script> 