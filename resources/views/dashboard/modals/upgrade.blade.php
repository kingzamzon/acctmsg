<div class="modal fade" id="upgradeModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editModalLabel">Upgrade Plan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           @if(Session::get('plans')->count() > 0)
          <div class="row">
            <div class="col-sm-6">
              @foreach(Session::get('plans') as $plan)
               @if($plan->plan == 'PREMIUM') 
                  {{-- Enable monthly sub my commenting this form --}}
                  <dl>
                    <dt></dt>
                    <dt>Professional</dt>
                    <dd>- Accounting Journal</dd>
                    <dd>- Income Statement</dd>
                    <dd>- Balance Sheet</dd>
                    <dd>- Cash Flow Statement</dd>
                    <dd>- Invoice</dd>
                    <dd>- Downloading Enabled</dd>
                    <dd>- Printing Enabled</dd>
                </dl>
                <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                  <div class="row" style="margin-bottom:40px;">
                    <div class="col-md-8 col-md-offset-2">
                      @php
                        $value = $plan->amount * 100 ;
                      @endphp
                      <input type="hidden" name="email" value="{{auth()->user()->email}}"> 
                      <input type="hidden" name="orderID" value="{{$plan->id}}">
                      <input type="hidden" name="amount" value="{{$value}}" id="amount"> 
                      <input type="hidden" name="quantity" value="1">
                      @if(!Auth::guest())
                      <input type="hidden" name="metadata" value="{{ json_encode($array = ['user_id' => auth()->user()->id,'plan_id' => $plan->id, 'prev_url' => URL::current()]) }}" > 
                      @endif
                      <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> 
                      <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> 
                      {{ csrf_field() }} 
          
                      <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                      <p>
                        <button class="btn btn-info btn-sm" type="submit" value="Pay Now!">
                        <i class="fa fa-plus-circle fa-lg"></i> Pay Now!
                        </button>
                      </p>
                    </div>
                  </div>
                </form>
                {{-- End of montly sub form --}}
              @endif
              @endforeach

            
              </div>
            </div>
          @endif
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>