<!DOCTYPE html>
<html>
<head>
    
    <title>Hi</title>
     <!-- Main styles for this application -->
    <style>
        h3{
            font-weight: 700;
            text-align: center;
        }
        tr > th, tr > td {
            text-align: left; 
            padding:15px; 
            /* border-bottom: 1px solid #a4b7c1; */
            border-top: 1px solid #a4b7c1;
        }
        .thead-th {
            text-align: left; 
            padding:15px; 
            background:#4dbd74 ;
        }
        .table-success {
            text-align: left; 
            padding:15px; 
            background-color: #cdedd8;
        }
        .td-white {
            text-align: left; 
            padding:15px; 
            background:#fff;
        }
        .td-total {
            text-align: left; 
            padding: 0.75rem;
            padding-left: 3rem !important;
        }
        .text-white {
            color: #fff;
        }
        .text-black {
            color: #000;
        }
        .bg-light {
            text-align: left; 
            padding:15px; 
            background-color: #f0f3f5 !important;
        }
        .bg-primary {
            text-align: left; 
            padding:15px; 
            background-color: #20a8d8 !important;
        }
    </style>
</head>
<body>
    <h3>Balance Sheet</h3>
    <div class="col-sm-4">
        <div>{{$businessInfo->name}}</div>
        <div>Email: {{$businessInfo->email}}</div>
        <div>Phone: {{$businessInfo->phone}}</div>
    </div>
    <div style="margin-top: 30px;">
        <table style="width:100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th class="thead-th text-white">Assets</th>
                    <th class="thead-th text-white"> {{ $year }}</th>
                </tr>
                <tr>
                    <th class="table-success">Current Assests</th>
                    <th class="table-success"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($current_assets_data as $current_asset_data)
                <tr>
                    <td class="td-white">{{ $current_asset_data->account_name }} </td>
                    <td class="td-white">{{ number_format($current_asset_data->balance, 2)}}</td>
                </tr>
                @endforeach
                <tr>
                    <th class="td-total" style="width: 65%;">Total current Assets</th>
                    <th class="">{{ number_format($current_assets_sum, 2)}}</th>
                </tr>
            </tbody>

            <thead>
                <tr>
                    <th class="table-success">Fixed (Long-Term) Assets - Non Current</th>
                    <th class="table-success"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($fixed_assets_data as $fixed_asset_data)
                <tr>
                    <td class="td-white">{{ $fixed_asset_data->account_name }} </td>
                    <td class="td-white">{{ number_format($fixed_asset_data->balance, 2)}}</td>
                </tr>
                @endforeach
                <tr>
                    <th class="td-total" style="width: 65%;">Total fixed assets</th>
                    <th class="">{{ number_format($current_assets_sum, 2)}}</th>
                </tr>
                <tr>
                    <th class="bg-light" style="width: 65%;">Total Assets</th>
                    <th class="">{{ number_format($assets_total, 2)}}</th>
                </tr>
            </tbody>
        </table>
    </div>

    <div style="margin-top: 30px;">
        <table style="width:100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th class="thead-th text-white">Liabilities and Owner's Equity</th>
                    <th class="thead-th text-white"> {{ $year }}</th>
                </tr>
                <tr>
                    <th class="table-success">Current Liabilities</th>
                    <th class="table-success"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($current_liabilities_data as $current_liability_data)
                <tr>
                    <td class="td-white">{{ $current_liability_data->account_name }} </td>
                    <td class="td-white">{{ number_format($current_liability_data->balance, 2)}}</td>
                </tr>
                @endforeach
                <tr>
                    <th class="td-total" style="width: 65%;">Total current liabilities</th>
                    <th class="">{{ number_format($current_liabilities_sum, 2)}}</th>
                </tr>
            </tbody>

            <thead>
                <tr>
                    <th class="table-success">Long-Term Liabilities</th>
                    <th class="table-success"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($fixed_liabilities_data as $fixed_liability_sum)
                <tr>
                    <td class="td-white">{{ $fixed_liability_sum->account_name }} </td>
                    <td class="td-white">{{ number_format($fixed_liability_sum->balance, 2)}}</td>
                </tr>
                @endforeach
                <tr>
                    <th class="td-total" style="width: 65%;">Total long-term liabilities</th>
                    <th class="">{{ number_format($fixed_liabilities_sum, 2)}}</th>
                </tr>
            </tbody>
        </table>
    </div>

    <div style="margin-top: 30px;">
        <table style="width:100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th class="table-success">Owner's Equity</th>
                    <th class="table-success"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($equity_data as $equity)
                <tr>
                    <th class="table-success">{{ $equity->account_name }}</th>
                    <th class="table-success">{{ number_format($equity->balance, 2)}}</th>
                </tr>
                @endforeach
                <tr>
                    <th class="table-success">Retained earnings</th>
                    <th class="table-success">{{ number_format($retained_earning, 2)}}</th>
                </tr>
                <tr>
                    <th class="td-total" style="width: 65%;">Total owner's equity</th>
                    <th class="">{{ number_format($equity_total, 2)}}</th>
                </tr>
                <tr>
                    <th class="bg-light" style="width: 65%;">Total Liabilities and Owner's Equity</th>
                    <th class="">{{ number_format($liablities_equity_total, 2)}}</th>
                </tr>
            </tbody>
        </table>
    </div>

    <div style="margin-top: 30px;">
        <table style="width:100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th class="bg-primary text-white">Common Financial Ratios</th>
                    <th class="bg-primary text-white"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="bg-light" style="width: 65%;"><strong>Debt Ratio </strong>(Total Liabilities / Total Assets)</td>
                    <td class="bg-light">{{ number_format($debt_ratio, 2)}}</td>
                </tr>
                <tr>
                    <td class="bg-light" style="width: 65%;"><strong>Current Ratio </strong>(Current Assets / Current Liabilities)</td>
                    <td class="bg-light">{{ number_format($current_ratio, 2)}}</td>
                </tr>
                <tr>
                    <td class="bg-light" style="width: 65%;"><strong>Working Capital </strong>(Current Assets - Current Liabilities)</td>
                    <td class="bg-light">{{ number_format($working_capital, 2)}}</td>
                </tr>
                <tr>
                    <td class="bg-light" style="width: 65%;"><strong>Assets-to-Equity Ratio </strong>(Total Assets / Owner's Equity)</td>
                    <td class="bg-light">{{ number_format($assests_equity_ratio, 2)}}</td>
                </tr>
                <tr>
                    <td class="bg-light" style="width: 65%;"><strong>Debt-to-Equity Ratio </strong>(Total Liabilities / Owner's Equity)</td>
                    <td class="bg-light">{{ number_format($debt_equity_ratio, 2)}}</td>
                </tr>
            </tbody>
        </table>
    </div>

</body>
</html>