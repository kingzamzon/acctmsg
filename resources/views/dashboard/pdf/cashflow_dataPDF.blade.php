<!DOCTYPE html>
<html>
<head>
    
    <title>Hi</title>
     <!-- Main styles for this application -->
    <style>
        h3{
            font-weight: 700;
            text-align: center;
        }
        tr > th, tr > td {
            text-align: left; 
            padding:15px; 
            /* border-bottom: 1px solid #a4b7c1; */
            border-top: 1px solid #a4b7c1;
        }
        .thead-th {
            text-align: left; 
            padding:15px; 
            background:#4dbd74 ;
        }
        .table-success {
            text-align: left; 
            padding:15px; 
            background-color: #cdedd8;
        }
        .td-white {
            text-align: left; 
            padding:15px; 
            background:#fff;
            padding-left: 3rem !important;
        }
        .td-total {
            text-align: left; 
            padding: 0.75rem;
        }
        .text-white {
            color: #fff;
        }
        .text-black {
            color: #000;
        }
        .bg-light {
            text-align: left; 
            padding:15px; 
            background-color: #f0f3f5 !important;
        }
        .bg-primary {
            text-align: left; 
            padding:15px; 
            background-color: #20a8d8 !important;
        }
        .pl-3 {
            padding-left: 3rem !important;
        }
    </style>
</head>
<body>
    <h3>{{ title_case($data['businessInfo']->name) }}</h3>
    <h3>Cash Flow Statement</h3>
    <div class="col-sm-4">
        <div>For the year Ending: {{ $data['year'] }}</div>
        <div>Cash at Beginning of Year: sdsd</div>
    </div>

    <div style="margin-top: 30px;">
        <table style="width:100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th class="thead-th text-white">Operations</th>
                    <th class="thead-th text-white"></th>
                </tr>
                <tr>
                    <th class="table-success">Cash reciepts from</th>
                    <th class="table-success"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($data['revenue_data'] as $revenue)
                <tr>
                    <td class="td-white">{{ $revenue->account_name }}</td>
                    <td class="td-white">{{ number_format($revenue->balance, 2)}}</td>
                </tr>
                @endforeach
            </tbody>

            <thead>
                <tr>
                    <th class="table-success">Cash paid for</th>
                    <th class="table-success"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($data['expense_data'] as $expense)
                <tr>
                    <td class="td-white">{{ $expense->account_name }}</td>
                    <td class="td-white">{{ number_format($expense->balance, 2)}}</td>
                </tr>
                @endforeach
                <tr>
                    <th class="td-total" style="width: 65%;">Net Cash Flow From Operations</th>
                    <th class="pl-3">{{ number_format($data['cash_sum'], 2)}}</th>
                </tr>
            </tbody>
        </table>
    </div>

    {{-- Investing Activities --}}
    <div style="margin-top: 30px;">
        <table style="width:100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th class="thead-th text-white">Investing Activities</th>
                    <th class="thead-th text-white"></th>
                </tr>
                <tr>
                    <th class="table-success">Cash reciepts from</th>
                    <th class="table-success"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($data['investing_data'] as $investing)
                <tr>
                    <td class="td-white">{{ $investing->account_name }}</td>
                    <td class="td-white">{{ number_format($investing->balance, 2)}}</td>
                </tr>
                @endforeach
            </tbody>

            <thead>
                <tr>
                    <th class="table-success">Cash paid for</th>
                    <th class="table-success"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="td-total" style="width: 65%;">Net Cash Flow From Investing Activities</th>
                    <th class="pl-3">{{ number_format($data['investing_sum'], 2)}}</th>
                </tr>
            </tbody>
        </table>
    </div>

    <div style="margin-top: 30px;">
        <table style="width:100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th class="thead-th text-white">Financing Activities</th>
                    <th class="thead-th text-white"></th>
                </tr>
                <tr>
                    <th class="table-success">Cash reciepts from</th>
                    <th class="table-success"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($data['financing_data'] as $financing)
                <tr>
                    <td class="td-white">{{ $financing->account_name }}</td>
                    <td class="td-white">{{ number_format($financing->balance, 2)}}</td>
                </tr>
                @endforeach
            </tbody>

            <thead>
                <tr>
                    <th class="table-success">Cash paid for</th>
                    <th class="table-success"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="td-total" style="width: 65%;">Net Cash Flow From Financing Activities</th>
                    <th class="pl-3">{{ number_format($data['financing_sum'], 2)}}</th>
                </tr>
            </tbody>
        </table>
    </div>

    <div style="margin-top: 30px;">
        <table style="width:100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th class="thead-th text-white">Net Increase In Cash</th>
                    <th class="thead-th text-white pl-3">{{ number_format($data['cash_sum'], 2)}}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="td-white">Cash at End of Year</td>
                    <td class="td-white">{{ number_format($data['cash_sum'], 2)}}</td>
                </tr>
            </tbody>
        </table>
    </div>

</body>
</html>