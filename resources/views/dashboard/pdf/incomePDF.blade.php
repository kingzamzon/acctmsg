<!DOCTYPE html>
<html>
<head>
    
    <title>Hi</title>
     <!-- Main styles for this application -->
    <style>
        h3{
            font-weight: 700;
            text-align: center;
        }
        tr > th {
            text-align: left; 
            padding:15px; 
            border-top: 1px solid black; 
            border-bottom: 1px solid black;
        }
        .td-white {
            text-align: left; 
            padding:15px; 
            background:#fff;
        }
        .td-total {
            text-align: left; 
            padding:15px; 
            background:#a4b7c1;
        }
    </style>
</head>
<body>
    <h3>Income Statement</h3>
    <div class="col-sm-4">
        <div>{{$businessInfo->name}}</div>
        <div>Email: {{$businessInfo->email}}</div>
        <div>Phone: {{$businessInfo->phone}}</div>
    </div>
    <div style="margin-top: 30px;">
        <table style="width:100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>Revenue</th>
                    <th> {{ $year }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($revenue_data as $revenue)
                <tr>
                    <td class="td-white">{{ $revenue->account_name }} </td>
                    <td class="td-white">{{ number_format($revenue->balance, 2)}}</td>
                </tr>
                @endforeach
                <tr>
                    <th class="td-total" style="width: 65%;">Total Revenues</th>
                    <th class="td-total">{{ number_format($revenue_sum, 2)}}</th>
                </tr>
            </tbody>
        </table>
    </div>

    <div style="margin-top: 30px">
        <table style="width:100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>Expenses</th>
                    <th> {{ $year }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($expense_data as $expense)
                <tr>
                    <td class="td-white">{{ $expense->account_name }} </td>
                    <td class="td-white">{{ number_format($expense->balance, 2) }}</td>
                </tr>
                @endforeach
                <tr>
                    <th class="td-total" style="width: 65%;">Total Expenses</th>
                    <th class="td-total">{{ number_format($expense_sum, 2)}}</th>
                </tr>
            </tbody>
        </table>
    </div>

    <div style="margin-top: 30px">
        <table style="width:100%" cellpadding="0" cellspacing="0">
            
            <tbody>
                <tr>
                    <td class="td-white">Net Income Before Taxes</td>
                    <td class="td-white">{{ number_format($net_icome_before_taxes, 2)}}</td>
                </tr>
                <tr>
                    <td class="td-white">Income Tax Expense</td>
                    <td class="td-white">{{ number_format($tax_expense, 2)}}</td>
                </tr>
                <tr>
                    <td class="td-white" style="width: 65%;"><b>Net Income!</b></td>
                    <td class="td-white">{{ number_format($net_income, 2)}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>