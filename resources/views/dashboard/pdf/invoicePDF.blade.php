<!DOCTYPE html>
<html>
<head>
  <style>
    * {
        padding:0px;
        margin:0px;
    }
    #main {
        padding:50px;
        font-family: sans-serif;
    }
    #header {
        /* overflow:hidden;  */
        width:100%;
        height: 100px;
    }
    #container {
        float:left; 
        width: 33%; 
        line-height: 25px;
        padding-left: 30px;
    }
  </style>
</head>
    <body>
        <div class="main">
            <h1 style="text-align: center;">Invoice Statement</h1>
            <div id="header">
                <div id="container">
                    <p>From:</p>
                    <h3>{{$invoice->business->name}}</h3>
                    <p>{{$invoice->business->address}}</p>
                    <p>Email: {{$invoice->business->email}}</p>
                    <p>Phone: {{$invoice->business->phone}}</p>
                </div>
                <div id="container">
                    <p>To:</p>
                    <h3 >{{$invoice->customer->business_name}}</h3>
                    <p >{{$invoice->customer->business_name}}</p>
                    <p >Email: {{$invoice->customer->email}}</p>
                    <p >Phone: {{$invoice->customer->tel}}</p>
                </div>
                <div class="container">
                    <p>Details:</p>
                    <h3>#{{$invoice->invoice_no}}</h3>
                    <p>{{$invoice->invoice_date}}</p>
                    <p>VAT: </p>
                </div>
            </div>
            <div style="margin-top: 30px;">
                <table style="width:100%" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="text-align: left; padding:15px; border-top: 1px solid black; border-bottom: 1px solid black;">#</th>
                            <th style="text-align: left; padding:15px; border-top: 1px solid black; border-bottom: 1px solid black;">Item</th>
                            <th style="text-align: left; padding:15px; border-top: 1px solid black; border-bottom: 1px solid black;">Description</th>
                            <th style="text-align: center; padding:15px; border-top: 1px solid black; border-bottom: 1px solid black;">Quantity</th>
                            <th style="text-align: center; padding:15px; border-top: 1px solid black; border-bottom: 1px solid black;">Unit Cost</th>
                            <th style="text-align: center; padding:15px; border-top: 1px solid black; border-bottom: 1px solid black;">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invoice->invoicetransaction as $transaction)
                        <tr>
                            <td style="text-align: left; padding:15px; background:#eee;">{{$loop->iteration}}</td>
                            <td style="text-align: left; padding:15px; background:#eee;">{{$transaction->item['name']}}</td>
                            <td style="text-align: left; padding:15px; background:#eee;">{{$transaction->description}}</td>
                            <td style="text-align: center; padding:15px; background:#eee;">{{$transaction->quantity}}</td>
                            <td style="text-align: center; padding:15px; background:#eee;">{{$transaction->unit_cost}}</td>
                            <td style="text-align: center; padding:15px; background:#eee;">{{ number_format($transaction->quantity * $transaction->unit_cost, 2)}}</td>
                        </tr>
                        @endforeach 

                        <tr>
                            <td colspan="4"></td>
                            <td style="text-align: right; padding:15px; font-weight:bold;">Subtotal</td>
                            <td style="text-align: center; padding:15px;">{{ number_format($subtotal, 2)}}</td>
                        </tr>
                        <tr>
                            <td colspan="4"></td>
                        <td style="text-align: right; padding:15px; font-weight:bold;">Discount ({{$invoice->discount}}%)</td>
                            <td style="text-align: center; padding:15px;">{{ number_format($discount, 2)}}</td>
                        </tr>
                        <tr>
                            <td colspan="4"></td>
                            <td style="text-align: right; padding:15px; font-weight:bold;">VAT ({{$invoice->vat}}%)</td>
                            <td style="text-align: center; padding:15px;">{{ number_format($vat, 2)}}</td>
                        </tr>
                        <tr>
                            <td colspan="4"></td>
                            <td style="text-align: right; padding:15px; font-weight:bold; background:#eee;">Total</td>
                            @if($invoice->vat == 0)
                            <td style="text-align: center; padding:15px; background:#eee;">{{ number_format($subtotal - $discount, 2)}}</td>
                            @else
                            <td style="text-align: center; padding:15px; background:#eee;">{{number_format(($subtotal - $discount) + $vat, 2)}}</td>
                            @endif
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>