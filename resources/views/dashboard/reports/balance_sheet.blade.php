@extends('dashboard.template')

@section('page-title')
  Balance Sheet
@endsection

@section('view-style')
<link href="{{ asset('vendors/css/bootstrap-datepicker3.standalone.css') }}" rel="stylesheet">
@endsection

@section('main')
<main class="main">

  <!-- Breadcrumb -->
  <ol class="breadcrumb no-print">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item"><a href="#">Admin</a></li>
    <li class="breadcrumb-item active">Balance Sheet</li>
  </ol>


  <div class="container-fluid">
      <div class="card">
          <div class="card-header no-print">
            Balance Sheet
            <a href="#" class="btn btn-sm btn-info float-right" onclick="javascript:window.print();"><i class="fa fa-print"></i> Print</a>
          </div>
          <div class="row mt-3 m-3 no-print">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-8 form-inline">
                <div class="form-group mb-2">
                  <div class="input-daterange">
                    <input type="text" class="form-control" id="start-date" value="2012-04-05" data-date-format="yyyy-mm-dd">
                  </div>
                </div>
                <div class="form-group mx-sm-3 mb-2">
                  <div class="input-daterange">
                    <input type="text" class="form-control" id="end-date" value="2012-04-19" data-date-format="yyyy-mm-dd">
                  </div>
                </div>
                <button type="button" class="btn btn-secondary mb-2 mr-3 search-btn">
                  <i class="fa fa-search"></i>&nbsp; Search
                </button>
                <a href="" 
                    class="btn btn-primary mb-2 d-none download-btn" download-btn>
                <i class="icon-cloud-download"></i> Download
              </a>
            </div>
          </div>
             <!--/.col-->
          <div class="card-body" id="table2excel">
              <strong><h4 class="text-center">Balance Sheet</h4></strong>
              <div class="row mb-4">
                  <div class="col-sm-4">
                    <div>{{$businessInfo->name}}</div>
                    <div>Email: {{$businessInfo->email}}</div>
                    <div>Phone: {{$businessInfo->phone}}</div>
                  </div>
                <!--/.col-->
              </div>
            <div class="data-content d-none">
                <div class="row">
                    <div class="col-lg-6">
                          <table class="table table-responsive-sm">
                            <thead>
                              <tr class="bg-success">
                                  <th scope="col">Assests</th>
                                  <th scope="col" id="end-year">{{date('Y')}}</th>
                                </tr>
                                <tr class="table-success">
                                    <th scope="col">Current Assests</th>
                                    <th scope="col"></th>
                                  </tr>
                            </thead>
                            <tbody id="current-assets">
                          
                                
                            </tbody>
          
                            <thead>
                                <tr class="table-success">
                                    <th scope="col">Fixed (Long-Term) Assets - Non Current</th>
                                    <th scope="col"></th>
                                  </tr>
                            </thead>
                            <tbody id="fixed-assets">
          
                                
                            </tbody>
          
          
                          </table>
                    </div>
                    <!--/.col-->
          
                    <div class="col-lg-6">
          
                          <table class="table table-responsive-sm">
                            <thead>
                              <tr class="bg-success">
                                  <th scope="col">Liabilities and Owner's Equity</th>
                                  <th scope="col" id="end-year1">{{date('Y')}}</th>
                                </tr>
                                <tr class="table-success">
                                    <th scope="col">Current Liabilities</th>
                                    <th scope="col"></th>
                                  </tr>
                            </thead>
                            <tbody id="current-liabilities">
          
                            </tbody>
          
                            {{-- long-term liabilities --}}
                            <thead>
                                <tr class="table-success">
                                    <th scope="col">Long-Term Liabilities</th>
                                    <th scope="col"></th>
                                  </tr>
                            </thead>
                            <tbody id="fixed-liabilities">
          
                            </tbody>
                            
                          </table>
          
                          <table class="table table-responsive-sm">
                            <thead>
                                <tr class="table-success">
                                    <th scope="col">Owner's Equity</th>
                                    <th scope="col"></th>
                                  </tr>
                            </thead>
                            <tbody id="owner_equity">
          
                            </tbody>
                            
                          </table>
                    </div>
                    <!--/.col-->
          
                  </div>
                  <div class="row">
                      <div class="col-lg-6">
                          <table class="table table-responsive-sm">
                              <thead>
                                <tr class="bg-primary">
                                    <th scope="col">Common Financial Ratios</th>
                                    <th scope="col"></th>
                                  </tr>
                              </thead>
                              <tbody class="bg-light" id="common-financial-ratios">
           
                              </tbody>
                            </table>
                      </div>
                      <div class="col-lg-6">
                      </div>
                  </div>
                </div>
            </div>
      
          </div>
    </div>

  <!-- /.conainer-fluid -->
</main>
<script src="{{ asset('vendors/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('vendors/js/jquery.table2excel.min.js') }}"></script>
<script>
  $(document).ready( function() {
    // initialize bootstrap datepicker.js
    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
      });
      
    $('.search-btn').on('click', function() {
      var startdate = $('#start-date').val();
      var enddate = $('#end-date').val();

      var download_btn = $('[download-btn]');

      // rewrite button url 
      var url = '{{ route("balance-generate-pdf", ["businessCode" => ":business_code", "start_date" => ":start_date", "end_date" => ":end_date"]) }}';
      var businesscode = "{{ $businessInfo->business_code }}"
      url = url.replace(':business_code', businesscode);
      url = url.replace(':start_date', startdate);
      url = url.replace(':end_date', enddate);
      // replace href value jquery
      download_btn = download_btn.attr('href', url);
     
  $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          })
        
    $.ajax({
        url: "{{ route('getBS', ['businessCode' => $businessInfo->business_code]) }}",
        method: "post",
        data: {
          startdate: startdate,
          enddate: enddate,
        },
        success: function(data){
          $('.download-btn').removeClass('d-none');
          $('.data-content').removeClass('d-none');
              // initilized account type data 
              var current_assets_data = [];
              var fixed_assets_data = [];
              var current_liabilities_data = [];
              var fixed_liabilities_data = [];
              var equity_data = [];

              //initialised account balance
              var sum_ca_total = 0;
              var sum_fa_total = 0;
              var sum_cl_total = 0;
              var sum_fl_total = 0;
              var sum_oe_total = 0;

              //-- Revenue 
              var revenue_data = [];
              var expense_data = [];
  
              var sum_revenue_total = 0;
              var sum_expense_total = 0;
              //--end of revenue

              $.each(data, function(i, v) {
                // push account type into their array
              if(v.account_type == "Current Assets"){
                current_assets_data.push(v);
                }

              if(v.account_type == "Fixed Assets"){
                fixed_assets_data.push(v);
                }

                if(v.account_type == "Current Liabilities"){
                  current_liabilities_data.push(v);
                }

                if(v.account_type == "Fixed Liabilities"){
                  fixed_liabilities_data.push(v);
                }

                if(v.account_type == "Equity"){
                  equity_data.push(v);
                }

                 //-----Revenue Wahala 
                  //revenue
                  if(v.account_type == "Revenue"){
                  revenue_data.push(v);
                  }

                  //expense
                  if(v.account_type == "Expense"){
                    expense_data.push(v);
                  }


                //--- End of revenue wahala
                
              })
              //end of each
              
              //reload dom 
              $('#current-assets').html("");
              $('#fixed-assets').html("");
              $('#current-liabilities').html("");
              $('#fixed-liabilities').html("");
              $('#owner_equity').html("");
              $('#common-financial-ratios').html("");

                // looping through account type data 
                $.each(current_assets_data, function(i, v) {
                $('#current-assets').append(`
                  <tr>
                  <td>${v.account_name}</td>
                  <td>${v.balance.toLocaleString()}</td>
                  </tr>
                  `)
                });

                $.each(fixed_assets_data, function(i, v) {
                  $('#fixed-assets').append(`
                    <tr>
                    <td>${v.account_name}</td>
                    <td>${v.balance.toLocaleString()}</td>
                    </tr>
                    `)
                })

                $.each(current_liabilities_data, function(i, v) {
                  $('#current-liabilities').append(`
                    <tr>
                    <td>${v.account_name}</td>
                    <td>${v.balance.toLocaleString()}</td>
                    </tr>
                    `)
                })

                $.each(fixed_liabilities_data, function(i, v) {
                  $('#fixed-liabilities').append(`
                    <tr>
                    <td>${v.account_name}</td>
                    <td>${v.balance.toLocaleString()}</td>
                    </tr>
                    `)
                })
                
                $.each(equity_data, function(i, v) {
                  $('#owner_equity').append(`
                    <tr>
                    <td>${v.account_name}</td>
                    <td>${v.balance.toLocaleString()}</td>
                    </tr>
                    `)
                })

                //-----Revenue Wahala 
                  // get total balance of each account type
                  for(var i = 0; i < revenue_data.length; i++)
                  {
                    sum_revenue_total += parseInt(revenue_data[i].balance) ;
                  }

                 for(var i = 0; i < expense_data.length; i++)
                  {
                    sum_expense_total += parseInt(expense_data[i].balance) ;
                  }
                
                  var retained_earning = sum_revenue_total - sum_expense_total;
                  $('#owner_equity').append(`
                    <tr>
                    <td>Retained earnings</td>
                    <td>${retained_earning.toLocaleString()}</td>
                    </tr>
                    `)
                //--- End of revenue wahala


                // get total balance of each account type
                for(var i = 0; i < current_assets_data.length; i++)
                  {
                    sum_ca_total += parseInt(current_assets_data[i].balance) ;
                  }

                for(var i = 0; i < fixed_assets_data.length; i++)
                  {
                    sum_fa_total += parseInt(fixed_assets_data[i].balance) ;
                  }

                for(var i = 0; i < current_liabilities_data.length; i++)
                  {
                    sum_cl_total += parseInt(current_liabilities_data[i].balance) ;
                  }

                for(var i = 0; i < fixed_liabilities_data.length; i++)
                  {
                    sum_fl_total += parseInt(fixed_liabilities_data[i].balance) ;
                  }

                for(var i = 0; i < equity_data.length; i++)
                  {
                    sum_oe_total += parseInt(equity_data[i].balance) ;
                  }
           
              // Append total balance to view
                  sum_oe_total = sum_oe_total + retained_earning;
              $('#current-assets').append(`
                  <tr>
                      <th class="pl-5">Total current Assets</th>
                    <td> ${sum_ca_total.toLocaleString()}</td>
                  </tr>
              `)

              $('#fixed-assets').append(`
                  <tr>
                      <th class="pl-5">Total fixed assets</th>
                    <td> ${sum_fa_total.toLocaleString()}</td>
                  </tr>
              `)
              

              $('#current-liabilities').append(`
                  <tr>
                      <th class="pl-5">Total current liabilities</th>
                    <td> ${sum_cl_total.toLocaleString()}</td>
                  </tr>
              `)

              $('#fixed-liabilities').append(`
                  <tr>
                      <th class="pl-5">Total long-term liabilities</th>
                    <td> ${sum_fl_total.toLocaleString()}</td>
                  </tr>
              `)

              $('#owner_equity').append(`
                  <tr>
                      <th class="pl-5">Total owner's equity</th>
                    <td> ${sum_oe_total.toLocaleString()}</td>
                  </tr>
              `)
             
              // total sum of value
              var assets_total = sum_ca_total + sum_fa_total;
              var liablities_equity_total = sum_cl_total + sum_fl_total + sum_oe_total;
              var liablities_total = sum_cl_total + sum_fl_total;
              
              $('#fixed-assets').append(`
                  <tr>
                    <th class="bg-light">Total Assets</th>
                    <td> ${assets_total.toLocaleString()}</td>
                  </tr>
              `)

              $('#owner_equity').append(`
                  <tr>
                    <th class="bg-light">Total Liabilities and Owner's Equity</th>
                    <td> ${liablities_equity_total.toLocaleString()}</td>
                  </tr>
              `)
               
                var debt_ratio = liablities_total / assets_total;
                var current_ratio = sum_ca_total / sum_cl_total;
                var working_capital = sum_ca_total - sum_cl_total;
                var assests_equity_ratio = assets_total / sum_oe_total;
                var debt_equity_ratio = liablities_total / sum_oe_total;
                
                // var checkIsNaN  = isNaN(debt_ratio);
                if(isNaN(debt_ratio)){ debt_ratio = 0.0; }
                if(isNaN(current_ratio)) { current_ratio = 0.0; }
                if(isNaN(working_capital)) { working_capital = 0.0; }
                if(isNaN(assests_equity_ratio)) { assests_equity_ratio = 0.0; }
                if(isNaN(debt_equity_ratio)) { debt_equity_ratio = 0.0; }
                // console.log(checkIsNaN);
                // if debt_ratio
              $('#common-financial-ratios').html(`
              <tr>
                <td><strong>Debt Ratio </strong>(Total Liabilities / Total Assets)</td>
                <td class="bg-light"> ${debt_ratio.toFixed(2)}</td>
              </tr>
              <tr>
                <td><strong>Current Ratio </strong>(Current Assets / Current Liabilities)</td>
                <td class="bg-light">${current_ratio.toFixed(2)}</td>
              </tr>
              <tr>
                <td><strong>Working Capital </strong>(Current Assets - Current Liabilities)</td>
                <td class="bg-light">${working_capital.toFixed(2)}</td>
              </tr>
              <tr>
                <td><strong>Assets-to-Equity Ratio </strong>(Total Assets / Owner's Equity)</td>
                <td class="bg-light">${assests_equity_ratio.toFixed(2)}</td>
              </tr>
              <tr>
                <td><strong>Debt-to-Equity Ratio </strong>(Total Liabilities / Owner's Equity)</td>
                <td class="bg-light">${debt_equity_ratio.toFixed(2)}</td>
              </tr>

              `)
        }
    })


    });



  })
</script>
@endsection
