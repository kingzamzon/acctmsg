@extends('dashboard.template')

@section('page-title')
  CashFlow
@endsection

@section('view-style')
<link href="{{ asset('vendors/css/bootstrap-datepicker3.standalone.css') }}" rel="stylesheet">
@endsection

@section('main')
<main class="main">

  <!-- Breadcrumb -->
  <ol class="breadcrumb no-print">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item"><a href="#">Admin</a></li>
    <li class="breadcrumb-item active">Cash Flow</li>
  </ol>

  <div class="container-fluid">
    <div class="card">
      <div class="card-header no-print">
        Cash Flow Statement
        <a href="#" class="btn btn-sm btn-info float-right" onclick="javascript:window.print();"><i class="fa fa-print"></i> Print</a>
      </div>
         <!--/.col-->
         <div class="row mt-3 m-3 no-print">
          <div class="col-sm-4">
          </div>
          <div class="col-sm-8 form-inline">
              <div class="form-group mb-2">
                <div class="input-daterange">
                  <input type="text" class="form-control" id="start-date" value="2012-04-05" data-date-format="yyyy-mm-dd">
                </div>
              </div>
              <div class="form-group mx-sm-3 mb-2">
                <div class="input-daterange">
                  <input type="text" class="form-control" id="end-date" value="2012-04-19" data-date-format="yyyy-mm-dd">
                </div>
              </div>
              <button type="button" class="btn btn-secondary mb-2 mr-3 search-btn">
                <i class="fa fa-search"></i>&nbsp; Search
              </button>
              <a href="" 
                    class="btn btn-primary mb-2 d-none download-btn" download-btn>
                <i class="icon-cloud-download"></i> Download
              </a>
          </div>
        </div>
          <!--/.col-->

      <div class="card-body" id="table2excel">
        <div class="row">
          <div class="col-sm-12 text-center ">
            <strong><h4>{{ title_case($businessInfo->name) }}</h4></strong>
          </div>
          <div class="col-sm-12 text-center ">
            <h4 class="bold">Cash Flow Statement</h4>
        </div>
        </div>
        <!--/.row-->
          <section>
                <div class="row float-right">
                <div class="col-5-sm">For the year Ending:</div>
                <div class="col" id="end-year"></div>
                </div>
                <br>
                <div class="row float-right">
                <div class="col-5-sm">Cash at Beginning of Year:</div>
                <div class="col" id="begin-cash">  </div>
                </div>
          </section>

          <div class="data-content d-none">
            {{-- Operations --}}
            <table class="table table-responsive-sm ">
              <thead>
                <tr class="bg-success">
                    <th class="col-sm-8">Operations</th>
                    <th class="col" id="end-year3"></th>
                  </tr>
                  <tr class="table-success">
                      <th class="col-sm-8">Cash reciepts from</th>
                      <th class="col"></th>
                    </tr>
              </thead>
              <tbody id="operations-cash-reciepts-from">
 
              </tbody>
              <thead>
                  <tr class="table-success">
                      <th class="col-sm-8">Cash paid for</th>
                      <th class="col"></th>
                    </tr>
              </thead>
              <tbody id="operations-cash-paid-for">
                 
              </tbody>
            </table>

            {{-- Investing Acitivities --}}
            <table class="table table-responsive-sm ">
              <thead>
                <tr class="bg-success">
                    <th class="col-sm-8">Investing Activities</th>
                    <th class="col" id="end-year3"></th>
                  </tr>
                  <tr class="table-success">
                      <th class="col-sm-8">Cash reciepts from</th>
                      <th class="col"></th>
                    </tr>
              </thead>
              <tbody id="investing-cash-reciepts-from">
                  
              </tbody>
              <thead>
                  <tr class="table-success">
                      <th class="col-sm-8">Cash paid for</th>
                      <th class="col"></th>
                    </tr>
              </thead>
              <tbody id="investing-cash-paid-for">
                  
                  <tr>
                    <th class="col-sm-8">Net Cash Flow From Investing Activities</th>
                    <th class="bg-light pl-5">&#8358;4500</th>
                  </tr>
              </tbody>
            </table>

            {{-- Financing activities --}}
            <table class="table table-responsive-sm ">
              <thead>
                <tr class="bg-success">
                    <th class="col-sm-8">Financing Activities</th>
                    <th class="col" id="end-year3"></th>
                  </tr>
                  <tr class="table-success">
                      <th class="col-sm-8">Cash reciepts from</th>
                      <th class="col"></th>
                    </tr>
              </thead>
              <tbody id="financing-cash-reciepts-from">
                  
              </tbody>
              <thead>
                  <tr class="table-success">
                      <th class="col-sm-8">Cash paid for</th>
                      <th class="col"></th>
                    </tr>
              </thead>
              <tbody id="financing-cash-paid-for">
                 
              </tbody>
            </table>

            {{-- Net Increase in cash --}}
            <table class="table table-responsive-sm " id="net-cash">
              <thead>
                <tr class="bg-success" >
                  <th class="col-sm-8">Net Increase In Cash</th>
                  <th class="col pl-5">&#8358;4500</th>
                </tr>
              </thead>
              <tbody>
                  <tr>
                    <th class="col-sm-8 text-right">Cash at End of Year</th>
                    <th class="col bg-light pl-5">&#8358;4500</td>
                  </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!-- cardbody -->

      </div>
    </div>
  <!-- /.conainer-fluid -->
</main>
<script src="{{ asset('vendors/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('vendors/js/jquery.table2excel.min.js') }}"></script>
<script>
  $(document).ready( function() {
    // initialize bootstrap datepicker.js
    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
      });
      
    $('.search-btn').on('click', function() {
      var startdate = $('#start-date').val();
      var enddate = $('#end-date').val();
      $('#end-year').html(`${enddate}`);

      var download_btn = $('[download-btn]');

      // rewrite button url 
      var url = '{{ route("cashflow-generate-pdf", ["businessCode" => ":business_code", "start_date" => ":start_date", "end_date" => ":end_date"]) }}';
      var businesscode = "{{ $businessInfo->business_code }}"
      url = url.replace(':business_code', businesscode);
      url = url.replace(':start_date', startdate);
      url = url.replace(':end_date', enddate);
      // replace href value jquery
      download_btn = download_btn.attr('href', url);
     
      $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              })
            
        $.ajax({
            url: "{{ route('getBS', ['businessCode' => $businessInfo->business_code]) }}",
            method: "post",
            data: {
              startdate: startdate,
              enddate: enddate,
            },
            success: function(data){
              $('.download-btn').removeClass('d-none');
              $('.data-content').removeClass('d-none');

              //-- Cash reciepts from
              var revenue_data = [];
              //-- Cash paid for
              var expense_data = [];

              /**
               * Investing activities
              */
               //-- Cash reciepts from
              var investing_cash_reciepts_data = [];
              //-- Cash paid for
              var investing_cash_paid_data = [];

              /**
               * Financing Activities
              */
               //-- Cash reciepts from
              var financing_cash_reciepts_data = [];
              //-- Cash paid for
              var financing_cash_paid_data = [];
  
              var sum_revenue_total = 0;
              var sum_expense_total = 0;
              var investing_cash_reciepts_total = 0;
              var investing_cash_paid_total = 0;
              var financing_cash_reciepts_total = 0;
              var financing_cash_paid_total = 0;

              var cash_sum = 0;
              //--end of revenue
              console.log(data);
              $.each(data, function(i, v) {
                // push account type into their array

                /*Begin of Operational Data*/
                //-----Cash reciepts from
                if(v.account_type == "Revenue"){
                  revenue_data.push(v);
                }

                //Cash paid for
                if(v.account_type == "Expense"){
                  expense_data.push(v);
                }
                /*End of Operational Data*/

                /*Begin of Operational Data*/
                // Make use of where it was credited
                //-----Cash reciepts from
                if(v.account_type == "Non-Current Assets"){
                  investing_cash_reciepts_data.push(v);
                }

                // Make use of where it was debited
                //Cash paid for
                if(v.account_type == "Non-Current Assets"){
                  investing_cash_paid_data.push(v);
                }
                /*End of Operational Data*/

                /*Begin of Operational Data*/
                //-----Cash reciepts from
                if(v.account_type == "Non-Current Liabilities"){
                  financing_cash_reciepts_data.push(v);
                }

                //Cash paid for
                if(v.account_type == "Non-Current Liabilities"){
                  financing_cash_paid_data.push(v);
                }
                /*End of Operational Data*/
              }); /*End of each*/

              //reload dom 
              $('#operations-cash-reciepts-from').html("");
              $('#operations-cash-paid-for').html("");
              $('#investing-cash-reciepts-from').html("");
              $('#investing-cash-paid-for').html("");
              $('#financing-cash-reciepts-from').html("");
              $('#financing-cash-paid-for').html("");

              // looping through account type data 
              $.each(revenue_data, function(i, v) {
              $('#operations-cash-reciepts-from').append(`
                <tr>
                  <td class="col-sm-8 pl-5">${v.account_name}</td>
                  <td class="pl-5">&#8358; ${v.balance.toLocaleString()}</td>
                </tr>
                `)
              });

              $.each(expense_data, function(i, v) {
                $('#operations-cash-paid-for').append(`
                  <tr>
                    <td class="col-sm-8 pl-5">${v.account_name}</td>
                    <td class="pl-5">&#8358; ${v.balance.toLocaleString()}</td>
                  </tr>
                  `)
              })

              $.each(investing_cash_reciepts_data, function(i, v) {
                $('#investing-cash-reciepts-from').append(`
                  <tr>
                    <td class="col-sm-8 pl-5">${v.account_name}</td>
                    <td class="pl-5">&#8358; ${v.balance.toLocaleString()}</td>
                  </tr>
                  `)
              })

              $.each(financing_cash_reciepts_data, function(i, v) {
                $('#financing-cash-reciepts-from').append(`
                  <tr>
                    <td class="col-sm-8 pl-5">${v.account_name}</td>
                    <td class="pl-5">&#8358; ${v.balance.toLocaleString()}</td>
                  </tr>
                  `)
              })

              // get total balance of each account type
              for(var i = 0; i < revenue_data.length; i++)
              {
                sum_revenue_total += parseInt(revenue_data[i].balance) ;
              }

              for(var i = 0; i < expense_data.length; i++)
              {
                sum_expense_total += parseInt(expense_data[i].balance) ;
              }

              for(var i = 0; i < investing_cash_reciepts_data.length; i++)
              {
                investing_cash_reciepts_total += parseInt(investing_cash_reciepts_data[i].balance) ;
              }

              for(var i = 0; i < investing_cash_paid_data.length; i++)
              {
                investing_cash_paid_total += parseInt(investing_cash_paid_data[i].balance) ;
              }

              for(var i = 0; i < financing_cash_reciepts_data.length; i++)
              {
                financing_cash_reciepts_total += parseInt(financing_cash_reciepts_data[i].balance) ;
              }

              for(var i = 0; i < financing_cash_paid_data.length; i++)
              {
                financing_cash_paid_total += parseInt(financing_cash_paid_data[i].balance) ;
              }
            

              cash_sum = sum_revenue_total + sum_expense_total;
              investing_sum = investing_cash_reciepts_total + investing_cash_paid_total;
              financing_sum = financing_cash_reciepts_total + financing_cash_paid_total;

              $('#operations-cash-paid-for').append(`
                <tr>
                    <th class="col-sm-8">Net Cash Flow From Operations</th>
                    <th class="bg-light pl-5">&#8358; ${cash_sum.toLocaleString()}</th>
                </tr> `);
              
              $('#investing-cash-paid-for').append(`
              <tr>
                  <th class="col-sm-8">Net Cash Flow From Investing Activities</th>
                  <th class="bg-light pl-5">&#8358; ${investing_sum.toLocaleString()}</th>
              </tr> `);

              $('#financing-cash-paid-for').append(`
              <tr>
                  <th class="col-sm-8">Net Cash Flow From Financing Activities</th>
                  <th class="bg-light pl-5">&#8358; ${financing_sum.toLocaleString()}</th>
              </tr> `);

              $('#net-cash').html(`
              <thead>
                <tr class="bg-success">
                  <th class="col-sm-8">Net Increase In Cash</th>
                  <th class="col pl-5">&#8358;${cash_sum.toFixed(2)}</th>
                </tr>
              </thead>
              <tbody>
                  <tr>
                    <th class="col-sm-8 text-right">Cash at End of Year</th>
                    <th class="col bg-light pl-5">&#8358;${cash_sum.toFixed(2)}</td>
                  </tr>
              </tbody>`);
      
            } /*End of Success*/
          });
      }); /*End of .search-btn on click*/
  }) /*End of document.ready*/
</script>
@endsection
