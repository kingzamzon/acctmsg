@extends('dashboard.template')

@section('page-title')
  Income Statement
@endsection

@section('view-style')
<link href="{{ asset('vendors/css/bootstrap-datepicker3.standalone.css') }}" rel="stylesheet">
@endsection

@section('main')
<main class="main">

  <!-- Breadcrumb -->
  <ol class="breadcrumb no-print">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item"><a href="#">Admin</a></li>
    <li class="breadcrumb-item active">Income Statement</li>
  </ol>

  <div class="container-fluid">
    <div class="card">
      <div class="card-header no-print">
        Income Statement
        <a href="#" class="btn btn-sm btn-info float-right" onclick="javascript:window.print();"><i class="fa fa-print"></i> Print</a>

      </div>
         <!--/.col-->
         <div class="row mt-3 m-3 no-print">
          <div class="col-sm-4">
          </div>
          <div class="col-sm-8 form-inline">
              <div class="form-group mb-2">
                <div class="input-daterange">
                  <input type="text" class="form-control" id="start-date" value="2012-04-05" data-date-format="yyyy-mm-dd">
                </div>
              </div>
              <div class="form-group mx-sm-3 mb-2">
                <div class="input-daterange">
                  <input type="text" class="form-control" id="end-date" value="2012-04-19" data-date-format="yyyy-mm-dd">
                </div>
              </div>
              <button type="button" class="btn btn-secondary mb-2 mr-3 search-btn">
                <i class="fa fa-search"></i>&nbsp; Search
              </button>
              <a href="" 
                    class="btn btn-primary mb-2 d-none download-btn" download-btn>
                <i class="icon-cloud-download"></i> Download
              </a>
          </div>
        </div>
          <!--/.col-->
      <div class="card-body">
              <strong><h4 class="text-center">Income Statement</h4></strong>
        <div class="row mb-4">
            <div class="col-sm-4">
              <div>{{$businessInfo->name}}</div>
              <div>Email: {{$businessInfo->email}}</div>
              <div>Phone: {{$businessInfo->phone}}</div>
            </div>
          <!--/.col-->
        </div>
        <!--/.row-->
        <div class="data-content d-none" >
          <div class="row">
            <div class="col-sm-12">
              <table class="table table-responsive-sm" >
                <thead>
                  <tr>
                    <th class="col-sm-8">Revenue</th>
                    <th class="col" id="end-year">{{date('Y')}}</th>
                  </tr>
                </thead>
                <tbody id="revenue">
    
                </tbody>
              </table>
              
             
              <table class="table table-responsive-sm">
                  <thead>
                      <tr>
                        <th class="col-sm-8">Expenses</th>
                        <th class="col" id="end-year1">{{date('Y')}}</th>
                      </tr>
                    </thead>
                    <tbody id="expense">
                      <tr>
                        <td>Salaries </td>
                        <td >80</td>
                      </tr>
                    </tbody>
              </table>

            </div>
          </div>
          <table class="table table-clear table-borderless">             
                <tbody id="other_calculations">
                </tbody>
          </table>

        </div>
      </div>
    </div>

  </div>
  <!-- /.conainer-fluid -->
</main>
<script src="{{ asset('vendors/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('vendors/js/jquery.table2excel.min.js') }}"></script>
<script>
    $(document).ready( function() {

      // initialize bootstrap datepicker.js
      $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
      });

      $('.search-btn').on('click', function() {
        var startdate = $('#start-date').val();
        var enddate = $('#end-date').val();
        var download_btn = $('[download-btn]');

        // rewrite button url 
        var url = '{{ route("income-generate-pdf", ["businessCode" => ":business_code", "start_date" => ":start_date", "end_date" => ":end_date"]) }}';
        var businesscode = "{{ $businessInfo->business_code }}"
        url = url.replace(':business_code', businesscode);
        url = url.replace(':start_date', startdate);
        url = url.replace(':end_date', enddate);
        // replace href value jquery
        download_btn = download_btn.attr('href', url);

        
        $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          })
          $.ajax({
              url: "{{ route('getBS', ['businessCode' => $businessInfo->business_code]) }}",
              method: "post",
              data: {
                startdate: startdate,
                enddate: enddate,
              },
              success: function(data){
                $('.download-btn').removeClass('d-none');
                $('.data-content').removeClass('d-none');
                var revenue_data = [];
                var expense_data = [];

                function empty() {
                  revenue_data.length = 0;
                  expense_data.length = 0;
                }
                empty();
                 // initilized account type data 
                var sum_revenue_total = 0;
                var sum_expense_total = 0;
                
                var h = $.each(data, function(i, v) {
  
                  //revenue
                if(v.account_type == "Revenue"){
                  revenue_data.push(v);
                  }

                  //expense
                  if(v.account_type == "Expense"){
                    expense_data.push(v);
                  }
                  
                })
                //end of each

                //reload dom 
                $('#revenue').html("");
                $('#expense').html("");
                $('#other_calculations').html("");

                // looping through account type data 
                $.each(revenue_data, function(i, v) {
                  $('#revenue').append(`
                    <tr>
                    <td>${v.account_name}</td>
                    <td>${v.balance.toLocaleString()}</td>
                    </tr>
                    `)
                });

                $.each(expense_data, function(i, v) {
                  $('#expense').append(`
                    <tr>
                    <td>${v.account_name}</td>
                    <td>${v.balance.toLocaleString()}</td>
                    </tr>
                    `)
                });

                 // get total balance of each account type
                 for(var i = 0; i < revenue_data.length; i++)
                  {
                    sum_revenue_total += parseInt(revenue_data[i].balance) ;
                  }

                 for(var i = 0; i < expense_data.length; i++)
                  {
                    sum_expense_total += parseInt(expense_data[i].balance) ;
                  }
  
                $('#revenue').append(`
                    <tr class="bg-secondary">
                        <th>Total Revenues</th>
                      <td>&#8358; ${sum_revenue_total.toLocaleString()}</td>
                    </tr>
                `)
  
                $('#expense').append(`
                    <tr class="bg-secondary">
                        <th>Total Expenses</th>
                      <td>&#8358; ${sum_expense_total.toLocaleString()}</td>
                    </tr>
                `)

                var net_icome_before_taxes = sum_revenue_total - sum_expense_total;
                var tax_expense = 0;
                var net_income = net_icome_before_taxes - tax_expense;
                $('#other_calculations').append(`
                
                <tr>
                      <td class="col-sm-8">Net Income Before Taxes</td>
                      <td class="col">&#8358; 
                          ${net_icome_before_taxes.toLocaleString()}
                      </td>
                    </tr>
                    <tr >
                      <td class="col-sm-8">Income Tax Expense</td>
                      <td class="col">&#8358; 
                          ${tax_expense.toLocaleString()}
                      </td>
                    </tr>

                    <tr>
                        <td class="font-weight-bold col-sm-8">Net Income</td>
                        <td class="col">&#8358; 
                            ${net_income.toLocaleString()}
                        </td>
                    </tr>
  
                `)
              }
          })

      });
          
    })
  </script>
@endsection