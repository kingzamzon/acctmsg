<div class="sidebar">
    <nav class="sidebar-nav">
      @if(Request::route()->getName() != 'business.index')
      <ul class="nav">
        <li class="nav-item">

          <a class="nav-link" href="@if($businesses->count() > 0) {{ route('dashboard.index', ['businessCode' => $businessInfo->business_code]) }} @else # @endif">
            <i class="icon-speedometer"></i> Dashboard
           </a>
        </li>
        <li class="divider m-2"></li>
        @if($businesses->count() > 0)
        <li class="nav-title">
          management
        </li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-compass"></i> Invoice</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('invoice.create', ['businessCode' => $businessInfo->business_code]) }}"><i class="icon-note"></i> Create</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('invoice.index', ['businessCode' => $businessInfo->business_code]) }}"><i class="fa fa-rocket"></i> Sent</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('items.index', ['businessCode' => $businessInfo->business_code]) }}"><i class="fa fa-eercast"></i> Items</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('customers.index', ['businessCode' => $businessInfo->business_code]) }}"><i class="fa fa-address-book"></i> Customers</a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(Request::route()->getName() == 'users.index') active @endif" 
            href="{{ route('users.index', ['businessCode' => $businessInfo->business_code]) }}"><i class="fa fa-users"></i> Users
              </a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if(Request::route()->getName() == 'journal.index') active @endif" 
            href="{{ route('journal.index', ['businessCode' => $businessInfo->business_code]) }}"><i class="icon-map"></i> Accounting Journal
              </a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if(Request::route()->getName() == 'chartofaccounts.index') active @endif" 
            href="{{ route('chartofaccounts.index', ['businessCode' => $businessInfo->business_code]) }}"><i class="fa fa-line-chart"></i> Chart Of Accounts
              </a>
          </li>

        <li class="nav-title">
            Report
          </li>
          <li class="nav-item">
            <a class="nav-link @if(Request::route()->getName() == 'cashflow.index') active @endif" 
            href="{{ route('cashflow.index', ['businessCode' => $businessInfo->business_code]) }}"><i class="icon-pie-chart"></i> Cash Flow</a>
          </li>
        <li class="nav-item">
          <a class="nav-link @if(Request::route()->getName() == 'balance_sheet.index') active @endif" 
          href="{{ route('balance_sheet.index', ['businessCode' => $businessInfo->business_code]) }}"><i class="fa fa-pie-chart"></i> Balance Sheet
            </a>
        </li>
        <li class="nav-item">
          <a class="nav-link @if(Request::route()->getName() == 'income_statement.index') active @endif" 
            href="{{ route('income_statement.index', ['businessCode' => $businessInfo->business_code]) }}"><i class="fa fa-cubes"></i> Income Statement
            </a>
        </li>

        <li class="divider m-2"></li>


        @endif
      </ul>

    </nav>
    @endif
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
  </div>
