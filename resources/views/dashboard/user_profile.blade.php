@extends('dashboard.template')

@section('page-title')
    {{Auth::user()->name}}
@endsection

@section('main')
<main class="main">

  <!-- Breadcrumb -->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">Profile</li>
  </ol>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h3>{{Auth::user()->name}} </h3>
            <img src="{{asset('img/avatars/'.Auth::user()->profile_photo) }}" 
                class="img-avatar mb-3" alt="{{Auth::user()->name}}" style="width:140px;height:200px" >
                <div class="form-group">
        <form method="POST" action="{{ route('users.update', ['businessCode' => $businessInfo->business_code,'user' => Auth::user()->id]) }}" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="_method" value="PUT">
            <label for="profile_photo">Profile Photo:</label> <br>
            <input type="file" name="profile_photo" id="profile_photo">
            @error('profile_photo')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror 
            <div class="form-group mb-0 mt-4">
                <button type="submit" class="btn btn-primary btn-sm" onclick="disabled">
                    Change Photo
                </button>
            </div>
          </form>
        
                </div>
            <div class="row">
              <div class="col-sm-3">Fullname</div>
              <div class="col-sm-7">{{Auth::user()->name}}</div>
            </div>
            <div class="row">
              <div class="col-sm-3">Email</div>
              <div class="col-sm-7">{{Auth::user()->email}}</div>
            </div>
            <div class="row">
              <div class="col-sm-3">Joined Date</div>
              <div class="col-sm-7">{{Auth::user()->created_at}}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.conainer-fluid -->
</main>
@endsection
