@extends('dashboard.template')

@section('page-title')
  Users
@endsection

@section('main')
<main class="main">

  <!-- Breadcrumb -->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item"><a href="#">Admin</a></li>
    <li class="breadcrumb-item active">Users</li>
  </ol>

  <div class="container-fluid">
    <div class="card">
      <div class="card-header">
        Users
        <strong>#{{$businessusers->count()}}</strong>
        <a href="#" class="btn btn-sm btn-info float-right" onclick="javascript:window.print();"><i class="fa fa-print"></i> Print</a>
        <a href="#" class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="#addModal">
            <i class="fa fa-plus"></i> Add</a>
      </div>
      <div class="card-body">
          <strong><h4 class="text-center">Users</h4></strong>
          <div class="row mb-4">
            <!--/.col-->
          </div>
          <!--/.row-->

        <div class="table-responsive-sm">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Id</th>
                <th>Full name</th>
                <th>Role</th>
                {{-- <th></th> --}}
                <th></th>
              </tr>
            </thead>
            <tbody>
                @foreach($businessusers as $businessuser)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$businessuser->name}}</td>
                <td>@if($businessuser->role_id == 1)
                      Admin 
                    @else 
                      Staff
                    @endif</td>
                {{-- <td>
                  @if($userrole == 1)
                  <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#editModal">
                      <i class="icon-pencil" ></i> Edit</a>
                  @endif
                </td> --}}
                <td>
                  @if($userrole == 1 &&  $businessuser->id != auth()->user()->id)

                    <form method="POST" 
                      action="userbusiness/{{$businessuser->userBusinessId}}">
                        @csrf
                      <input type="hidden" name="_method" value="DELETE">
                      <button class="btn btn-danger" type="submit">
                        <i class="icon-trash"></i>Remove</button>
                    </form>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!--/.row-->
      </div>
    </div>
      
      <!-- Modal -->
      <div class="modal fade" id="addModal" tabindex="-1" role="dialog" 
      aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="card-header">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" 
                      role="tab" aria-controls="home" aria-selected="true">Search</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" 
                      aria-controls="profile" aria-selected="false">Register</a>
                    </li>
                    
                  </ul>
              </div>
            <div class="card-body">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        @include('dashboard.forms.search_user')
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        @include('dashboard.forms.add_user')
                      </div>
                  </div>
              </div>
            <div class="card-footer">
              
              <button type="button" class="btn btn-sm btn-secondary float-right" data-dismiss="modal">
                  <i class="fa fa-times"></i> Close</button>
            </div>
          </div>
        </div>
      </div>

  </div>
  <!-- /.conainer-fluid -->
</main>

@endsection
