@component('mail::message')
# Introduction
@component('mail::table')
| From:    |
| ------------- |
| {{$invoice->business->name}} | 
| {{$invoice->business->address}} | 
| {{$invoice->business->email}} |
| {{$invoice->business->phone}} |

@endcomponent


@component('mail::table')
| #       | Item         | Description  | Quantity | Unit Cost | Total |
| ------------- |:-------------:|:-------------:| :-------------:| :-------------:|  --------:| 
@foreach($invoice->invoicetransaction as $transaction)
| {{$loop->iteration}} | {{$transaction->item['name']}}    | {{$transaction->description}}  | {{$transaction->quantity}}  |{{$transaction->unit_cost}}  |{{$transaction->quantity * $transaction->unit_cost}}  |
@endforeach
@endcomponent

@component('mail::button', ['url' => $url])
View Invoice 
@endcomponent

Thanks,<br>
{{ config('app.name') }}

{{-- Subcopy --}}
@isset($url)
@component('mail::subcopy')
If you’re having trouble clicking the  button, copy and paste the URL below
into your web browser: [{{ $url }}]({{ $url }})
@endcomponent
@endisset
@endcomponent
