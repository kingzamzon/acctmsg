<link href="{{ asset('css/style.css') }}" rel="stylesheet">

<script src="{{ asset('vendors/js/jquery.min.js') }}"></script>
<title>
  {{$invoice->business->name}} | #{{$invoice->invoice_no}}
</title>

<style>
  body {
    background-color: #fff
  }
</style>

<main>
    @if($invoice->count() > 0)
      <div class="card-body">
          <div class="row mb-4">
  
            <div class="col-sm-4">
              <h6 class="mb-3">From:</h6>
              <div>
                <strong>{{$invoice->business->name}}</strong>
              </div>
              <div>{{$invoice->business->address}}</div>
              <div>Email: {{$invoice->business->email}}</div>
              <div>Phone: {{$invoice->business->phone}}</div>
            </div>
            <!--/.col-->
  
            <div class="col-sm-4">
              <h6 class="mb-3">To:</h6>
              <div>
                <strong>{{$invoice->customer->business_name}}</strong>
              </div>
              <div>{{$invoice->customer->business_name}}</div>
              <div>{{$invoice->customer->address}}</div>
              <div>Email: {{$invoice->customer->email}}</div>
              <div>Phone: {{$invoice->customer->tel}}</div>
            </div>
            <!--/.col-->
  
            <div class="col-sm-4">
              <h6 class="mb-3">Details:</h6>
              <div>
                <strong>#{{$invoice->invoice_no}}</strong>
              </div>
              <div>{{$invoice->invoice_date}}</div>
              <div>VAT: </div>
            </div>
            <!--/.col-->
  
          </div>
          <!--/.row-->
  
          <div class="table-responsive-sm">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Item</th>
                  <th>Description</th>
                  <th>Quantity</th>
                  <th>Unit Cost</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody item-list>
                @foreach($invoice->invoicetransaction as $transaction)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$transaction->item['name']}}</td>
                    <td>{{$transaction->description}}</td>
                    <td class="quantity">{{$transaction->quantity}}</td>
                    <td class="cost">{{$transaction->unit_cost}}</td>
                    <td class="bg-light" total id="tot">{{$transaction->quantity * $transaction->unit_cost}}</td>
                  </tr>
                @endforeach 
              </tbody>
            </table>
          </div>
  
          <div class="row">
  
            <div class="col-lg-4 col-sm-5">
                {{$invoice->description}}
            </div>
            <!--/.col-->
  
            @include('dashboard.invoice-template-total')
  
          </div>
          <!--/.row-->
        </div>
  <script>
    $(document).ready( function() {
    var total_sum = 0;
    var discount_sum = 0;
  
      $( ".cost, .quantity" ).each(function(e) {
          var sol = $(this).parents("tr");
          var quantity = sol.find( ".quantity" ).text();
          var unit = sol.find( ".cost" ).text();
          var total = quantity * unit;
          sol.find( "[total]" ).text( total);
          
        })
  
      function calculateSum() {
          var subtotal = 0;
          $("[item-list] tr").find('td:eq(5)').each(function(){
              subtotal += parseFloat($(this).text());
          }) 
          $("#total_sum_value").html("&#8358; " + subtotal.toLocaleString() );
          total_sum = subtotal;
        }
  
        calculateSum();
  
      function calculateDiscount() {
        var disc = $("#discount").val();
        if(disc > 0) {
          var discount_value = (disc * total_sum) / 100;
          $("#discount-total").html("&#8358; " + discount_value.toLocaleString() );
          discount_sum = total_sum = discount_value;
          $("#real-total").html("&#8358; " + discount_value.toLocaleString() );
        }else {
          $("#discount-total").html("&#8358; 0.0" );
          $("#real-total").html("&#8358; " + total_sum.toLocaleString() );
        }
      }
      calculateDiscount();
  
  
    })
  </script>
      
    @endif
    </main>
</body>
