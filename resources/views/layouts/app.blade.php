<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Easy Bookkeeping for Startups" />
        <meta name="keywords" content="Fintech, Bookkeeping, startup, clerk, startupclerk, invoice, accounting journal, income statement, balance sheet, cashflow, finance, finanicial records" />
        <meta name="author" content="Petrong" />
        <meta name="email" content="petrongprogrammers@gmail.com" />
        <meta name="website" content="http://www.petrongsoftware.com" />
        <meta name="Version" content="v2.5.1" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('page-title') | StartupClerk</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="{{ asset('img/logoi.png') }}">
        <!-- Bootstrap -->
        <link href="{{ asset('land/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Magnific -->
        <link href="{{ asset('land/css/magnific-popup.css') }}" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="{{ asset('land/css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v2.1.9/css/unicons.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
        <!-- Slider -->               
        <link rel="stylesheet" href="{{ asset('land/css/owl.carousel.min.css') }}"/> 
        <link rel="stylesheet" href="{{ asset('land/css/owl.theme.default.min.css') }}"/> 
        <!-- Main Css -->
        <link href="{{ asset('land/css/style.css') }}" rel="stylesheet" type="text/css" id="theme-opt" />
        <link href="{{ asset('land/css/colors/default.css') }}" rel="stylesheet" type="text/css" id="color-opt">
    </head>

    <body>
        <!-- Loader -->
        <!-- <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div> -->
        <!-- Loader -->
        
        

        @yield('content')

        @include('layouts.footer')

        <!-- Back to top -->
        <a href="#" class="btn btn-icon btn-soft-primary back-to-top"><i data-feather="arrow-up" class="icons"></i></a>
        <!-- Back to top -->

        <!-- javascript -->
        @include('layouts.scripts')
        @yield('scripts')
    	
        <script src="//code.tidio.co/n4vnbpii4y0funu0mjzxelph8kstakkf.js" async></script>
    </body>
</html>