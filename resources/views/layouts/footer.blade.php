        <!-- Footer Start -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-12 mb-0 mb-md-4 pb-0 pb-md-2">
                        <a href="{{ route('homepage') }}" class="logo-footer">
                            <img src="{{ asset('land/images/logo-light.png') }}" height="24" alt="">
                        </a>
                        <p class="mt-4">Easy Bookkeeping For Startups.</p>
                        <ul class="list-unstyled social-icon social mb-0 mt-4">
                            <li class="list-inline-item"><a href="https://facebook.com/startupclerk" class="rounded"><i data-feather="facebook" class="fab fa-facebook-f"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="instagram" class="fab fa-instagram fea-social"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="twitter" class="fab fa-twitter fea-social"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="linkedin" class="fab fa-linkedin-in fea-social"></i></a></li>
                        </ul><!--end icon-->
                    </div><!--end col-->
                    
                    <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                        
                    </div><!--end col-->
                    
                    <div class="col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                        <h4 class="text-light footer-head">Useful Links</h4>
                        <ul class="list-unstyled footer-list mt-4">
                            <li><a href="{{ route('homepage') }}" class="text-foot"><i class="fas fa-chevron-right mr-1"></i> Home</a></li>
                            <li><a href="{{ route('services') }}" class="text-foot"><i class="fas fa-chevron-right mr-1"></i> Services</a></li>
                            <li><a href="{{ route('contactus.index') }}" class="text-foot"><i class="fas fa-chevron-right mr-1"></i> Contact us</a></li>
                            <li><a href="{{ route('pricing') }}" class="text-foot"><i class="fas fa-chevron-right mr-1"></i> Pricing</a></li>
                            <li><a href="{{ route('login') }}" class="text-foot"><i class="fas fa-chevron-right mr-1"></i> Sign in</a></li>
                            <li><a href="{{ route('register') }}" class="text-foot"><i class="fas fa-chevron-right mr-1"></i> Register</a></li>
                        </ul>
                    </div><!--end col-->

                    <div class="col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">

                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </footer><!--end footer-->
        <footer class="footer footer-bar">
            <div class="container text-center">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="text-sm-left">
                            <p class="mb-0">&copy; Startupclerk.</p>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </footer><!--end footer-->
        <!-- Footer End -->