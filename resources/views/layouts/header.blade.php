<header id="topnav" class="defaultscroll sticky">
    <div class="container" style="margin-top: 5px;">
        <!-- Logo container-->
        <div>
            <a class="logo" href="index.html">
                @if(Request::route()->getName() == 'homepage')
                    <img src="{{ asset('land/images/logo-dark.png') }}" class="l-dark" height="50" alt="">
                    <img src="{{ asset('land/images/logo-light.png') }}" class="l-light" height="50" alt="">
                @else
                    <img src="{{ asset('land/images/logo-dark.png') }}" height="50" alt="">
                @endif

            </a>
        </div>                 
        <div class="buy-button d-none d-md-block">
            <a href="{{ route('register') }}">
                <div class="btn btn-primary login-btn-primary">Get Started</div>
                <div class="btn btn-light login-btn-light">Get Started</div>
            </a>
        </div><!--end login button-->
        <!-- End Logo container-->
        <div class="menu-extras">
            <div class="menu-item">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </div>
        </div>

        <div id="navigation">
            <!-- Navigation Menu-->   
            <ul class="navigation-menu @if(Request::route()->getName() == 'homepage') nav-light @endif">
                <li><a href="{{ route('homepage') }}">Home</a></li>
                <li><a href="{{ route('services') }}">Services</a></li>
                <li><a href="{{ route('pricing') }}">Pricing</a></li>
                <li><a href="{{ route('login') }}">Sign in</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>
            </ul><!--end navigation menu-->
            <div class="buy-menu-btn d-none">
                <a href="{{ route('register') }}" target="_blank" class="btn btn-primary">Get Started</a>
            </div><!--end login button-->
        </div><!--end navigation-->
    </div><!--end container-->
</header><!--end header-->
<!-- Navbar End -->