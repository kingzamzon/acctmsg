
<script src="{{ asset('land/js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('land/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('land/js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('land/js/scrollspy.min.js') }}"></script>
<!-- Icons -->
<script src="{{ asset('land/js/feather.min.js') }}"></script>
<script src="https://unicons.iconscout.com/release/v2.1.9/script/monochrome/bundle.js"></script>
<!-- Switcher -->
<script src="{{ asset('land/js/switcher.js') }}"></script>

<!-- Main Js -->
<script src="{{ asset('land/js/app.js') }}"></script>
