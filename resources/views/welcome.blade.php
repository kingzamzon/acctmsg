<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

{{-- <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=false"> --}}
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>{{ config('app.name', 'StartupClerk') }}</title>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
<link href="{{ asset('css/frontend/bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('css/frontend/stylesheet.css') }}" rel="stylesheet">
<link href="{{ asset('css/frontend/responsive.css') }}" rel="stylesheet">
<link rel="shortcut icon" href="{{ asset('img/logoi.png') }}">
<script src="{{ asset('js/frontend/html5shiv.js') }}"></script>
<script src="{{ asset('js/frontend/respond.min.js') }}"></script> 
<script src="{{ asset('js/frontend/jquery-1.10.2.js') }}"></script>
<script src="{{ asset('js/frontend/jquery-migrate.min.js') }}"></script>
<script src="{{ asset('js/frontend/tether.min.js') }}"></script>
<script src="{{ asset('js/frontend/bootstrap.js') }}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.7/js/tether.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script> --}}
<style>
  a:hover {
    cursor: pointer;
  }
</style>
</head>

<body>
  <header id="hpart">
    <div class="mb-nav-icon"><span class="nav-icon" id="trigger-overlay"></span></div>
    <div class="nav-part">
      <div class="logo-box pushmenu-push"><a href="/">
        <img src="images/logo.png" alt="" width="240vhm" type="image/png"/></a></div>
      <nav id="nav">
        <ul class="main-nav">
          <li><a href="#hpart" class="selected">HOME</a></li>
          <li><a href="#about-me">ABOUT</a></li>
          <li><a href="#our-services">Features</a></li>
          <li><a href="#press-mention">Customers</a></li>
          <li><a href="#latest-blog">Subscription</a></li>
          <li><a href="#get-in-touch">CONTACT</a></li>
          <li><a data-toggle="modal" data-target="#signinModal"
            >GET STARTED</a></li>
        </ul>
      </nav>
    </div>
    <div class="container-fluid pushmenu-push">
      <div class="row hpart-container">
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
          <div class="left-part">
            <div class="clearfix">
              <div class="banner-text"> 
                <span style="color:#ffc736">You <br>
                Don't Need<br>
                An Accountant <br>
                You Need <br></span>
                StartupClerk!<br>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
          <div class="right-part"><img src="images/banner-bg-1.png" class="img-responsive" alt="" /></div>
        </div>
      </div>
    </div>
    <canvas id="banner-canvas" class="pushmenu-push"></canvas>
  </header>
  <div class="pushmenu-push">
    <section id="about-me" >
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
            <div class="about-cpart">
              <h2 class="heading-2">About StartupClerk</h2>
              <div class="about-content">
                <div class="sub-title">Easy Book-keeping For Startups</div>
                <div class="desc text-justify" style="font-size: 15px;">
                  <p>
                    StartupClerk is an account management software designed to help growing startups  do all the
                    book-keeping that is necessary to enable their businesses evade financial crisis, gain better credibility to investors
                    and ensure excellent cashflow mangement, without the presence of an accountant; thus allowing them the space
                    to focus their attention on product developemt, marketing and other profitable business adventures.
                  </p>
                  <p>
                    You can use StartupClerk for free to do financial book-keeping for your business.
                    It comes with features such as Accounting Journal, 
                    Balance Sheet, Income Statement,
                    Cash Flow Statement and Invoice.
                  </p>
                  <p>
                    StartupClerk provides you with very interesting features and tools such as reporting, 
                    spooling your financial records to excel and printing.
                  </p>
                  {{-- <a href="#youtube">HOW TO USE STARTUPCLERK</a>            --}}
                <dl>
                  <dt>HOW TO USE STARTUPCLERK</dt>
                  <a href="#video">
                    <dd>Download Video Tutorial For Use On Mobile Browser</dd>
                  </a>
                  <a href="#video1">
                    <dd>Download Video Tutorial For Use On Desktop Browser</dd>
                  </a>
                  <a href="#video2">
                    <dd>Download PDF User's Guide</dd>
                  </a>
                </dl>
                <div class="text-center">
                  <a data-toggle="modal" data-target="#signinModal"
                  class="btn btn-warning btn-lg text-white " style="cursor: pointer;">GET STARTED</a>
              </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5">
            <div class="img-box"><img src="images/pri.jpg" class="img-responsive" alt="" style="filter: blur(2px);opacity: 0.3"/></div>
          </div>
        </div>
      </div>
    </section>
    <section id="our-services">
      <div class="container">
        <h2 class="text-center heading-1">Features</h2>
        <div class="service-cpart">
          <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-6">
              <div class="service-box">
                <div class="clearfix">
                  <div class="name"><a href="#">ACCOUNTING JOURNAL</a></div>
                  <div class="icon-box"><img src="images/journal1.png" 
                    class="social-icon" alt="" /></div>
                </div>
                <ul class="serviice-list">
                  <li>Record daily financial activities in your business</li>
                </ul>
              </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6">
              <div class="service-box">
                <div class="clearfix">
                  <div class="name"><a href="#">INCOME STATEMENT</a></div>
                  <div class="icon-box"><img src="images/income_statement.png" 
                    class="contant-icon" alt="" /></div>
                </div>
                <ul class="serviice-list">
                  <li>Reports all income and expenses in your business, helps to inform of profits or losses made in your business</li>
                </ul>
              </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6">
              <div class="service-box">
                <div class="clearfix">
                  <div class="name"><a href="#">BALANCE SHEET</a></div>
                  <div class="icon-box"><img src="images/balance_sheet.png" 
                    class="branding-icon" alt="" /></div>
                </div>
                <ul class="serviice-list">
                  <li>Provides a statement of your assets, liablities and capital of your business.</li>
                </ul>
              </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6">
              <div class="service-box">
                <div class="clearfix">
                  <div class="name"><a href="#">CASHFLOW STATEMENT</a></div>
                  <div class="icon-box"><img src="images/cash_flow.png" 
                    class="digital-icon" alt="" /></div>
                </div>
                <ul class="serviice-list">
                  <li>Reports how chances in balance sheet accounts and income affect cash and cash equivalent</li>
                </ul>
              </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6">
              <div class="service-box">
                <div class="clearfix">
                  <div class="name"><a href="#">INVOICE</a></div>
                  <div class="icon-box"><img src="images/invoice.png" 
                    class="guest-icon" alt="" /></div>
                </div>
                <ul class="serviice-list">
                  <li>Generate invoice for your business transactions with clients</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="text-center">
            <a data-toggle="modal" data-target="#signinModal"
            class="btn btn-warning btn-sm text-white pt-3" style="height:50px;background-color:#f04e4e;cursor: pointer;">SET UP YOUR ACCOUNTING JOURNAL</a>
        </div>
      </div>
      <canvas id="canvas-basic"></canvas>
     
    </section>
    <section id="press-mention">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <h2 class="text-center " style="font-size: 20vh">{{$businesses}}</h2>
            <h2 class="text-center ">Businesses</h2>
          </div>
          <div class="col-sm-6"> 
          <h2 class="text-center " style="font-size: 20vh">{{$users}}</h2>
          <h2 class="text-center ">Registered Users</h2>
          </div>
        </div>
      </div>

    </section>
    <section id="latest-blog">
      <h2 class="text-center heading-1">SUBSCRIPTION</h2>
      <div class="container">
        <div class="row justify-content-center">
          {{-- @foreach ($plans as $plan)
          <div class="col-md-4 col-sm-6 col-xs-6">
              <div class="latest-blog-box">
                <div class="img-box"><a href="#"><img src="images/blog-thumb-1.jpg" class="img-responsive" alt="" /></a></div>
                <div class="date" style="height: 250px">
                   
                    @if($plan->plan == 'FREMIUM') 
                    <dl>
                        <dt>{{$plan->plan}} 
                          <span class="orange text-white p-1">FREE</span></dt>
                        <dd>- 
                          {{$plan->days}}
                           Days Trial</dd>
                        <dd>- Invoice Feature</dd>
                    </dl> 
                    @else 
                      <dl>
                       
                          <dt>{{$plan->plan}} <span class="purple text-white p-1">&#8358;1,000</span></dt>
                          <dd>- Accounting Journal</dd>
                          <dd>- Income Statement</dd>
                          <dd>- Balance Sheet</dd>
                          <dd>- Cashflow Statement</dd>
                          <dd>- Invoice</dd>
                     
                          <dt>Monthly Subscription</dt>
                      </dl>
                    @endif
                </div>
                <span data-toggle="modal" data-target="#signinModal" class="category @if($plan->plan == 'FREMIUM') orange @else purple @endif">
                  GET STARTED
                </span> </div>
            </div>
          @endforeach --}}
          {{-- <div class="col-md-4 col-sm-6 col-xs-6">
              <div class="latest-blog-box">
                <div class="img-box"><a href="#"><img src="images/blog-thumb-3.jpg" class="img-responsive" alt=""></a></div>
                <div class="date" style="height: 250px">
                  <dt>OFFLINE PAYMENT <span class="green text-white p-1">&#8358;1,000</span></dt>
                  <dd>To pay offline you can do transfer to the below account details </dd>
                  <dd>Bank Name: GTB </dd>
                  <dd>Account name: Petrong Software Solutions </dd>
                  <dd>Account number: 0482761144</dd>
                </div>
                <span class="category green" data-toggle="modal" data-target="#signinModal">GET STARTED</span> </div>
            </div> --}}
          <div class="col-md-4 col-sm-6 col-xs-6">
              <div class="latest-blog-box">
                <div class="img-box"><a href="#"><img src="images/blog-thumb-3.jpg" class="img-responsive" alt=""></a></div>
                <div class="date" style="height: 250px">
                  <dt>FREEMIUM <span class="orange text-white p-1">FREE</span></dt>
                  <dd>- Accounting Journal</dd>
                  <dd>- Income Statement</dd>
                  <dd>- Balance Sheet</dd>
                  <dd>- Cashflow Statement</dd>
                  <dd>- Invoice</dd>
                </div>
                <span class="category orange" data-toggle="modal" data-target="#signinModal" style="height:50px;cursor: pointer;">GET STARTED</span> </div>
            </div>
        </div>
      </div>
    </section>
    <section id="get-in-touch">
      <div class="container clearfix">
        <div class="title-box clearfix text-center">
          <h2 class="heading-1">get in touch with us</h2>
          <div class="sub-title">We will get back to you in less than 24 hours</div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="social-box">
              <div class="heading-3">Get Socialize with us</div>
              <ul class="social-icons clearfix">
                <li><a href="https://facebook.com/startupclerk"><img src="images/blank-img.png" alt="" class="icon fb-icon" /></a></li>
                <li><a href="#"><img src="images/blank-img.png" alt="" class="icon twitter-icon" /></a></li>
                <li><a href="#"><img src="images/blank-img.png" alt="" class="icon instagram-icon" /></a></li>
                <li><a href="#"><img src="images/blank-img.png" alt="" class="icon youtube-icon" /></a></li>
                <li><a href="#"><img src="images/blank-img.png" alt="" class="icon skype-icon" /></a></li>
              </ul>
              <div class="contact-info row clearfix">
                <div class="col-md-5 col-sm-6 col-xs-6">
                  <div class="contact-info-text">Phone: (+234) 8063757632</div>
                  <div class="contact-info-text">Mobile: (+234) 8066071971</div>
                </div>
                <div class="col-md-7 col-sm-6 col-xs-6">
                  <div class="contact-info-text">Email: <a href="mailto:hello@startupclerk.com">hello@startupclerk.com</a></div>
                  <div class="contact-info-text">Web: <a href="http://petrongsoftware.com/">www.petrongsoftware.com</a></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="contact-form">
              @if(Session::get('data'))
              
              <script>
                $( document ).ready(function() {
                    $('#contactModal').modal('show');
                });
              </script>
              @endif
              
              <form method="POST" action="contactus">
                @csrf
                <div class="form-group">
                  <input type="text" class="required form-control" 
                    name="name" placeholder="NAME">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control required" placeholder="EMAIL" 
                    name="email">
                </div>
                <div class="form-group">
                  <input type="url" class="form-control"   name="url" placeholder="URL">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control required"  name="message" placeholder="MESSAGE">
                </div>
              
                <button type="submit" class="submit-btn btn btn-default"  >
                 SUBMIT</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="black-bg-strip"></div>
      <canvas id="pink-bg"></canvas>
    </section>
    <footer id="fpart">
      <div class="container">
        <div class="text-center copyright-text clearfix"> {{ config('app.name', 'StartupClerk') }} &copy; 
        <script>
          document.write(new Date().getFullYear())</script>  
          </div>
      </div>
    </footer>
    <div id="back-top" style="display: block;"><a href="javascript:void(0)" class="backtotop"><img src="images/top-arrow.png" alt="" title=""></a></div>
  </div>
  {{-- <script src="{{ asset('js/frontend/granim.min.js') }}"></script> --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js"></script>
  {{-- <script src="{{ asset('js/frontend/jquery.validate.js') }}"></script> --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
  {{-- <script src="{{ asset('js/frontend/jquery.waypoints.min.js') }}"></script> --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
  <script src="{{ asset('js/frontend/custom.js') }}"></script>
  
  <script>
  var $aboutMe = $('#about-me');
  $aboutMe.waypoint(function(){	
    $('.main-nav li').removeClass('active');
    $('.main-nav li:nth-child(2)').addClass('active');
  },{ offset: '50%'});
    
  var $ourServices = $('#our-services');
  $ourServices.waypoint(function(){	
    $('.main-nav li').removeClass('active');
    $('.main-nav li:nth-child(3)').addClass('active');
  },{ offset: '50%'});
  
  var $pressMention = $('#press-mention');
  $pressMention.waypoint(function(){
    $('.main-nav li').removeClass('active');	
    $('.main-nav li:nth-child(4)').addClass('active');
  },{ offset: '50%'});
  
  var $latestBlog = $('#latest-blog');
  $latestBlog.waypoint(function(){	
    $('.main-nav li').removeClass('active');
    $('.main-nav li:nth-child(5)').addClass('active');
  },{ offset: '50%'});
    
  var $getInTouch = $('#get-in-touch');
  $getInTouch.waypoint(function(){	
    $('.main-nav li').removeClass('active');
    $('.main-nav li:nth-child(6)').addClass('active');
  },{ offset: '50%'});
    
  </script>
  
  @if (count($errors) > 0)
      <script>
          $( document ).ready(function() {
              $('#signinModal').modal('show');
          });
      </script>
  @endif
    <!--login  Modal -->
    <div class="modal fade" id="signinModal" tabindex="-1" role="dialog" 
    aria-labelledby="signinModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          {{-- <div class="modal-header"> --}}
              <ul class="nav nav-tabs" id="myTab" role="tablist">
  
                  <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" 
                    role="tab" aria-controls="home" aria-selected="true">Login</a>
                  </li>
  
                  <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" 
                    aria-controls="profile" aria-selected="false">Register</a>
                  </li>
                </ul>
            
          {{-- </div> --}}
          <div class="modal-body">
                <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                      @include('auth.login')
                  </div>
                  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                      @include('auth.register')
                    </div>
                </div>
            
          </div>
        </div>
      </div>
    </div>
    
    <!--contact us   Modal -->
    <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" 
    aria-labelledby="contactModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Thank You</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Thank you for contacting us. We will get back to you shortly.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  
  
  
  </body>
</html>
