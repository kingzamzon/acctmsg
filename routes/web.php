<?php

use App\Account;
use App\Business;
use App\Invoice;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PagesController@index')->name('homepage');
Route::get('/old', 'PagesController@oldindex')->name('oldhomepage');
Route::get('/services', 'PagesController@services')->name('services');
Route::get('/pricing', 'PagesController@pricing')->name('pricing');
Route::resource('contactus', 'ContactusController');

// Errors ROute
Route::get('/404', function() {
	return view('dashboard.404_page');
})->name('404');


Auth::routes();

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/new_plan', 'SubscriptionPlanController@store')->name('plan');

Route::get('users_list', 'UsersController@UsersList');
Route::post('/new_accounts', 'AccountsController@store')->name('accounts');

Route::resource('subscription_plan', 'SubscriptionPlanController');
Route::resource('business', 'BusinessController');
Route::resource('subscribe', 'SubscribersController');
Route::resource('accounts', 'AccountsController');

Route::group(['prefix'=>'{businessCode}', 'middleware'=>['businessMiddleware']],function($businessCode){
	Route::resource('dashboard', 'DashboardController');
	// Route::resource('businessaccounts', 'BusinessAccountsController');
	Route::resource('chartofaccounts', 'ChartOfAccountsController');
	Route::resource('journal', 'JournalController')->middleware('CheckSubscription');
	Route::resource('users', 'UsersController')->middleware(['CheckSubscription', 'Subscription']);

	Route::resource('userbusiness', 'UserBusinessController');
	// Route::get('12mcashflow', 'CashFlowController@months12');
	Route::resource('invoice', 'InvoiceController');
	Route::resource('customers', 'CustomerController');
	Route::resource('items', 'ItemController');

	/**
	 * Reporting Routes
	 */
	Route::resource('income_statement', 'IncomeStatementController')->middleware(['CheckSubscription', 'Subscription']);
	Route::resource('cashflow', 'CashFlowController')->middleware(['CheckSubscription', 'Subscription']);
	Route::resource('balance_sheet', 'BalanceSheetController')->middleware(['CheckSubscription', 'Subscription']);
	/**
	 * Get reporting details route
	 */
	Route::post('getbalancesheet', 'BalanceSheetController@getBalancesheet')->name('getBS');
	Route::post('getcashflow', 'CashFlowController@getCashFlow')->name('getCF');
	Route::post('items.price', 'ItemController@showPrice')->name('getPrice');

	Route::get('income-generate-pdf/{start_date}/{end_date}','IncomeStatementController@generatePDF')->name('income-generate-pdf');
	Route::get('balance-generate-pdf/{start_date}/{end_date}','BalanceSheetController@generatePDF')->name('balance-generate-pdf');
	Route::get('cashflow-generate-pdf/{start_date}/{end_date}','CashFlowController@generatePDF')->name('cashflow-generate-pdf');
	Route::get('download-pdf/{id}','InvoiceController@downloadInvoiceasPDF')->name('download-pdf');

});




Route::get('/industries/{account_id}', function ($account_id) {
	$account = Account::where('id','=',$account_id)->first();

	return $account->accounttype->name;
	// $json = asset('static/industries.json');
 //    return $json;
});

//Paystack route
Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay'); 
Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');

Route::get('export','IncomeStatementController@export')->name('export');


Route::get('mailable/{businesscode}/{id}', function ($businessCode,$id) {
	$business = Business::where(['business_code'=>$businessCode])->first();
	$invoice = Invoice::where(['id'=>$id,'business_id'=>$business->id])->first();
	if($invoice){
		return new App\Mail\InvoiceMail($invoice);
	}else {
		return view('dashboard.404_page');
	}
})->name('mailable');

// Route::get('/play', function() {

// 	$j =  Invoice::generateInvoiceCode();
// 	return $j;
// });

Route::get('/bela', function() {
	return view('dashboard.pdf.cashflow_dataPDF');
});