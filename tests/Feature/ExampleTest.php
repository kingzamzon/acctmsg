<?php

namespace Tests\Feature;

use App\Business;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    // public function testBasicTest()
    // {
        // $response = $this->get(route('404'));
        // $response->assertSuccessful();

        // $response->assertStatus(200);
    // }

    // public function testBusinesscode()
    // {
    //     $business = Business::get()->random()

        // $response = $this->get(route('dashboard',['businessCode'=>$business->business_code]))
    // }
    public function testRedirectIfGuest()
    {
        $business = Business::get()->random();
        $reponse = $this->get(route('dashboard.index', ['businessCode'=>$business->business_code]));
        $reponse->assertRedirect(route('homepage'));
    }
}
